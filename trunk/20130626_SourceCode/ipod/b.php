﻿<?php
	$doc_root = $_SERVER['DOCUMENT_ROOT'];
	require_once($doc_root.'/includes/logger.php');
	require_once($doc_root.'/includes/mysql_tools.php');

	$a = $_GET['a'];
	$encode_a = urlencode($a);
	
	$b = $_GET['b'];
	$c = $_GET['c'];
	if(preg_match('/[0-9a-f]{32}/', $b, $matches) != 1 || preg_match('/[0-9a-f]{32}/', $c, $matches) != 1) {
		die();
	}
	
	$r = md5("The SID-Item $encode_a with $b Request Download Completed!!");
	if($c != $r) {
		die();
	}
	logWithPrefix('downloaded', $_GET['a']);
?>