<?php
	require_once(dirname(__FILE__).'/../includes/file_io.php');
	require_once(dirname(__FILE__).'/../includes/mysql_tools.php');
	
	$mime_filter = array('text/plain','text/html',
					'application/pdf','application/msword','application/rtf','application/vnd.ms-powerpoint',application/x-zip-compressed,
					'image/x-ms-bmp','image/jpeg','image/gif','image/png',
					'video/mp4',
					'audio/mp3');

	if($_FILES['file']['error'] == UPLOAD_ERR_OK){
		if(!in_array($_FILES['file']['type'], $mime_filter)) {
			die('!!badtype!!');
		}
		
		$username = $_SESSION['username'];
		$size = $_FILES['file']['size'];
		$mime = $_FILES['file']['type'];
		
		$file_name_without_ext = "";
		$file_ext = "";
		$filename = "";
		$path = "";
		$fullpath = "";
		do {
			$file_name_without_ext = urldecode(pathinfo(str_replace('%2F', '/', urlencode($_FILES['file']['name'])), PATHINFO_FILENAME));
			$file_ext = urldecode(pathinfo(str_replace('%2F', '/', urlencode($_FILES['file']['name'])), PATHINFO_EXTENSION));
			$filename = $file_name_without_ext.".".$file_ext;
			$shortPath = "f".md5(date("YmdHis").$_SERVER["REMOTE_ADDR"]);
			//$path = "f".md5(date("YmdHis").$_SERVER["REMOTE_ADDR"]).".".$file_ext;
			$path = $shortPath.".".$file_ext;
			$fullpath = '../file_storage/'.$path;
		} while (file_exists($fullpath));
		
				
		if(move_uploaded_file($_FILES['file']['tmp_name'], $fullpath)){
			// 以壓縮加密方式儲存檔案
    		ob_start();
    		system("zip -P pass ../file_storage/".$shortPath.".zip " . $fullpath);
			//system("unzip -P pass " . $file_name_without_ext . ".zip");
			unlink($fullpath);
			ob_end_clean();
			
			//$sql = "INSERT INTO `Files`(ID,ParentID,IsLeaf,Title,Description,Path,CreateDate,CreateBy,DueDate,Size,MIME,Ext) VALUES(uuid(), '', 1, '$file_name_without_ext', '$filename', '$path', now(), '$username', adddate(now(), 5), $size, '$mime', '$file_ext')";
			$sql = "INSERT INTO `Files`(ID,ParentID,IsLeaf,Title,Description,Path,CreateDate,CreateBy,DueDate,Size,MIME,Ext) VALUES(uuid(), '', 1, '$file_name_without_ext', '$filename', '$shortPath.zip', now(), '$username', adddate(now(), 5), $size, '$mime', '$file_ext')";
			mysql_Exec($sql);
			echo '!!success!!';
		}
		else {
			echo '!!failed!!';
		}
	}
?>