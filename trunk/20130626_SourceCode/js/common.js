var loadingEffect = 0;

function ImageOver(idName, imagedo) {
	document.getElementById(idName).src = imagedo
}

function showLoading() {
	loadingEffect = 1;
	$("loading_message").appear({
		duration : 0.2,
		afterFinish : function() {
			loadingEffect = 0
		}
	});
}

function hideLoading() {
	if (loadingEffect == 1) {
		hideLoading.delay(0.5);
		return;
	}
	$("loading_message").hide();
}

function guid() {
	function S4() {
		return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
	}

	return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

if (Overlay) {
	Overlay.defaults({
		modal : true,
		click_hide : false,
		auto_hide : false,
		opacity : 1,
		afterhide : function(overlay) {
			if (overlay.holder && overlay.holder.remove)
				overlay.holder.remove();
			if (overlay.background && overlay.background.remove)
				overlay.background.remove()
		}
	});
}

function loadPart(target, url) {
	showLoading();
	new Ajax.Updater(target, url, {
		onComplete : hideLoading,
		evalScripts : true
	});

	if (url.indexOf("summary") != -1 || url.indexOf("operators") != -1 || url.indexOf("students") != -1 || url.indexOf("groups") != -1 || url.indexOf("files") != -1) {

		document.getElementById("Image11").src = "../images/n_istudy/ssbtw_01.png";
		if (document.getElementById("Image17") != null) {
			document.getElementById("Image17").src = "../images/n_istudy/btw_04.png";
			document.getElementById("Image18").src = "../images/n_istudy/btw_05.png";
			Event.observe($("aSummary"), "mouseover", function() {
				ImageOver('Image11', '../images/n_istudy/ssbto_01.png');
			});
			Event.observe($("aTFiles"), "mouseover", function() {
				ImageOver('Image17', '../images/n_istudy/bto_04.png');
			});
			Event.observe($("aTFiles"), "mouseout", function() {
				ImageOver('Image17', '../images/n_istudy/btw_04.png');
			});
			Event.observe($("aSummary"), "mouseout", function() {
				ImageOver('Image11', '../images/n_istudy/ssbtw_01.png');
			});
			Event.observe($("aCourse2"), "mouseover", function() {
				ImageOver('Image18', '../images/n_istudy/bto_05.png');
			});
			Event.observe($("aCourse2"), "mouseout", function() {
				ImageOver('Image18', '../images/n_istudy/btw_05.png');
			});
		} else {
			document.getElementById("Image12").src = "../images/n_istudy/ssbtw_02.png";
			document.getElementById("Image13").src = "../images/n_istudy/ssbtw_03.png";
			document.getElementById("Image14").src = "../images/n_istudy/ssbtw_04.png";
			document.getElementById("Image15").src = "../images/n_istudy/ssbtw_05.png";

			Event.observe($("aSummary"), "mouseover", function() {
				ImageOver('Image11', '../images/n_istudy/ssbto_01.png');
			});
			Event.observe($("aOperators"), "mouseover", function() {
				ImageOver('Image12', '../images/n_istudy/ssbto_02.png');
			});
			Event.observe($("aStudents"), "mouseover", function() {
				ImageOver('Image13', '../images/n_istudy/ssbto_03.png');
			});
			Event.observe($("aGroups"), "mouseover", function() {
				ImageOver('Image14', '../images/n_istudy/ssbto_04.png');
			});
			Event.observe($("aFiles"), "mouseover", function() {
				ImageOver('Image15', '../images/n_istudy/ssbto_05.png');
			});

			Event.observe($("aSummary"), "mouseout", function() {
				ImageOver('Image11', '../images/n_istudy/ssbtw_01.png');
			});
			Event.observe($("aOperators"), "mouseout", function() {
				ImageOver('Image12', '../images/n_istudy/ssbtw_02.png');
			});
			Event.observe($("aStudents"), "mouseout", function() {
				ImageOver('Image13', '../images/n_istudy/ssbtw_03.png');
			});
			Event.observe($("aGroups"), "mouseout", function() {
				ImageOver('Image14', '../images/n_istudy/ssbtw_04.png');
			});
			Event.observe($("aFiles"), "mouseout", function() {
				ImageOver('Image15', '../images/n_istudy/ssbtw_05.png');
			});
		}
	} else if (url.indexOf("courses2") != -1) {
		document.getElementById("Image11").src = "../images/n_istudy/ssbtw_01.png";
		document.getElementById("Image17").src = "../images/n_istudy/btw_04.png";
		document.getElementById("Image18").src = "../images/n_istudy/btw_05.png";

		Event.observe($("aSummary"), "mouseover", function() {
			ImageOver('Image11', '../images/n_istudy/ssbto_01.png');
		});
		Event.observe($("aTFiles"), "mouseover", function() {
			ImageOver('Image17', '../images/n_istudy/bto_04.png');
		});
		Event.observe($("aCourse2"), "mouseover", function() {
			ImageOver('Image18', '../images/n_istudy/ssbto_09.png');
		});

		Event.observe($("aSummary"), "mouseout", function() {
			ImageOver('Image11', '../images/n_istudy/ssbtw_01.png');
		});
		Event.observe($("aTFiles"), "mouseout", function() {
			ImageOver('Image17', '../images/n_istudy/btw_04.png');
		});
		Event.observe($("aCourse2"), "mouseout", function() {
			ImageOver('Image18', '../images/n_istudy/btw_05.png');
		});
	}

	if (url.indexOf("summary") != -1) {
		document.getElementById("Image11").src = "../images/n_istudy/ssbtb_01.png";
		Event.observe($("aSummary"), "mouseover", function() {
			ImageOver('Image11', '../images/n_istudy/ssbtb_01.png');
		})
		Event.observe($("aSummary"), "mouseout", function() {
			ImageOver('Image11', '../images/n_istudy/ssbtb_01.png');
		})
	} else if (url.indexOf("operators") != -1) {
		document.getElementById("Image12").src = "../images/n_istudy/ssbtb_02.png";
		Event.observe($("aOperators"), "mouseover", function() {
			ImageOver('Image12', '../images/n_istudy/ssbtb_02.png');
		})
		Event.observe($("aOperators"), "mouseout", function() {
			ImageOver('Image12', '../images/n_istudy/ssbtb_02.png');
		})
	} else if (url.indexOf("students") != -1) {
		document.getElementById("Image13").src = "../images/n_istudy/ssbtb_03.png";
		Event.observe($("aStudents"), "mouseover", function() {
			ImageOver('Image13', '../images/n_istudy/ssbtb_03.png');
		})
		Event.observe($("aStudents"), "mouseout", function() {
			ImageOver('Image13', '../images/n_istudy/ssbtb_03.png');
		})
	} else if (url.indexOf("groups") != -1) {
		document.getElementById("Image14").src = "../images/n_istudy/ssbtb_04.png";
		Event.observe($("aGroups"), "mouseover", function() {
			ImageOver('Image14', '../images/n_istudy/ssbtb_04.png');
		})
		Event.observe($("aGroups"), "mouseout", function() {
			ImageOver('Image14', '../images/n_istudy/ssbtb_04.png');
		})
	} else if (url.indexOf("files") != -1) {
		if (document.getElementById("Image17") != null) {
			document.getElementById("Image17").src = "../images/n_istudy/btb_04.png";
			Event.observe($("aTFiles"), "mouseover", function() {
				ImageOver('Image17', '../images/n_istudy/btb_04.png');
			})
			Event.observe($("aTFiles"), "mouseout", function() {
				ImageOver('Image17', '../images/n_istudy/btb_04.png');
			})
		} else {
			document.getElementById("Image15").src = "../images/n_istudy/ssbtb_05.png";
			Event.observe($("aFiles"), "mouseover", function() {
				ImageOver('Image15', '../images/n_istudy/ssbtb_05.png');
			})
			Event.observe($("aFiles"), "mouseout", function() {
				ImageOver('Image15', '../images/n_istudy/ssbtb_05.png');
			})
		}
	} else if (url.indexOf("courses2") != -1) {
		document.getElementById("Image18").src = "../images/n_istudy/btb_05.png";
		Event.observe($("aCourse2"), "mouseover", function() {
			ImageOver('Image18', '../images/n_istudy/btb_05.png');
		})
		Event.observe($("aCourse2"), "mouseout", function() {
			ImageOver('Image18', '../images/n_istudy/btb_05.png');
		})
	}
}

function loadCss(url) {
	url += ((url.indexOf("?") == -1) ? "?" : "&") + Math.random();
	if (Prototype.Browser.IE) {
		document.createStyleSheet(url);
	} else {
		$$("head")[0].insert("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + url + "\"/>");
	}
}

function setMenuSelected(element) {
	element = $(element);
	if ($$(".menu a").indexOf(element) > -1) {
		$$(".menu a").invoke("removeClassName", "selected");
		element.addClassName("selected");
	}
	return element;
}

function menuClicked(element, partUrl) {
	loadPart($$(".main").first(), partUrl);
	setMenuSelected(element);
}

function logout() {
	new Ajax.Request("server/auth.php?act=logout");
	(function() {
		location.href = "login.html";
	}).delay(0.5);
}

//Modal Container Control
function modalContainer(element, callback) {
	if ($$(".modal").length > 0)
		return;
	element = $(element).addClassName("modal");
	element.setStyle("border:solid 5px #521D75;border-radius:5px;-webkit-border-radius:5px;-moz-border-radius:5px");
	/*	if(Prototype.Browser.IE)
	 curvyCorners({tl:{radius:5},tr:{radius:5},bl:{radius:5},br:{radius:5},antiAlias:true}, ".modal");
	 */
	element.draggable = new Draggable(element, {
		scroll : window
	});
	if (!!element.select(".title"))
		element.select(".title").invoke("setStyle", "cursor:move");
	var w = document.viewport.getWidth();
	var h = document.viewport.getHeight();
	var ex = (w - element.getWidth()) / 2;
	ex = ((ex >= 0) ? ex : 0);
	var ey = (h - element.getHeight()) / 2;
	ey = ((ey >= 0) ? ey : 0);

	var filterWidth = $(document.body).getWidth();
	var filterHeight = $(document.body).getHeight();
	if (w > filterWidth)
		filterWidth = w;
	if (h > filterHeight)
		filterHeight = h;
	$(document.body).insert(new Template("<div id=\"modalFilter\" style=\"width:#{width}px;height:#{height}px\"></div>").evaluate({
		"width" : filterWidth,
		"height" : filterHeight
	}));
	element.setStyle({
		"left" : ex + "px",
		"top" : ey + "px"
	}).appear({
		duration : 0.5
	});
	if (Object.isFunction(callback)) {
		callback.defer();
	}
}

function removeModalContainer() {
	$$(".modal")[0].removeClassName("modal").hide();
	$("modalFilter").remove();
}

function onResizeModalFilter() {
	if ($$(".modal").length == 0)
		return;
	var element = $$(".modal")[0];
	var w = document.viewport.getWidth();
	var h = document.viewport.getHeight();
	var ex = (w - element.getWidth()) / 2;
	ex = ((ex >= 0) ? ex : 0);
	var ey = (h - element.getHeight()) / 2;
	ey = ((ey >= 0) ? ey : 0); debugger
	var filterWidth = $(document.body).getWidth();
	var filterHeight = $(document.body).getHeight();
	if (w > filterWidth)
		filterWidth = w;
	if (h > filterHeight)
		filterHeight = h;
	$("modalFilter").setStyle({
		"width" : filterWidth,
		"height" : filterHeight
	});
	element.setStyle({
		"left" : ex + "px",
		"top" : ey + "px"
	});
	element = null;
}

Event.observe(window, "resize", onResizeModalFilter);
//End Modal Container Control

var Utility = {
	getSize : function(size) {
		size = parseInt(size, 10);
		if (size < 1024)
			return size + "B";
		var k = size / 1024;
		if (k < 1024)
			return (Math.round(k * 100) / 100) + "KB";
		var m = k / 1024;
		return (Math.round(m * 100) / 100) + "MB";
	},
	getFilename : function(path) {
		if (Prototype.Browser.IE)
			return path.split("\\").last();
		else
			return path;
	},
	getFilenameWithoutExt : function(path) {
		path = Utility.getFilename(path);
		var parts = path.split(".");
		if (parts.length > 1) {
			parts.length -= 1;
			return parts.join(".");
		} else {
			return path;
		}
	}
};
