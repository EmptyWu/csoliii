<?php
	if($_SESSION['username'] == "") die("<script>location.href='login.html';</script>");
	$doc_root = $_SERVER['DOCUMENT_ROOT'];
	require_once($doc_root.'/includes/file_io.php');
	require_once($doc_root.'/includes/mysql_tools.php');
	
	$_SESSION['selected'] = "course2";

	switch($_GET['act']) {
		case 'list':
			$conn = mysql_GetConnection();
			$title = mysql_real_escape_string($_POST['title'], $conn);
			$description = mysql_real_escape_string($_POST['description'], $conn);
			$username = $_SESSION['username'];
			$sql = "SELECT ID, CASE WHEN Title = '' THEN '(空)' ELSE Title END AS Title,
					CASE WHEN Description = '' THEN '(空)' ELSE Description END AS Description,
					(SELECT COUNT(1)
						FROM Files f INNER JOIN FileGroups fg ON f.ID = fg.FileID WHERE fg.GroupID = Files.ID) AS FileCount,
					(SELECT IFNULL(CONCAT('[', group_concat(f.title separator '], ['), ']'), '')
						FROM Files f INNER JOIN FileGroups fg ON f.ID = fg.FileID WHERE fg.GroupID = Files.ID) AS Filenames,
					Size, CreateDate
					FROM Files
					WHERE IsLeaf = 0 AND Title LIKE '$title%' AND Description LIKE '$description%' and CreateBy='$username'
					ORDER BY CreateDate DESC";
			$table = mysql_GetArrayRows($sql);
			print(json_encode($table));
			break;

		case 'listFiles':
			$username = $_SESSION['username'];
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sql = "SELECT ID, Title, (SELECT COUNT(1) FROM FileGroups WHERE FileID = Files.ID AND GroupID = '$id' and CreateBy='$username') AS Checked
					FROM Files
					WHERE IsLeaf = 1 and CreateBy='$username'
					ORDER BY CreateDate DESC";
					
			$table = mysql_GetArrayRows($sql);
			$sql = "select a.FileID as ID, b.Title as Title,
					(SELECT COUNT(1) FROM FileGroups WHERE FileID = a.FileID AND GroupID = '$id' and CreateBy='$username') AS Checked 
					from share_files a left join Files b on (a.FileID = b.ID)
					where a.ShareAcc='$username' order by a.CreateDate";
			
			$shareTable = mysql_GetArrayRows($sql);
			
			$tempArray = array();
			for ($i=0; $i<count($shareTable); $i++) {
				$exist = FALSE;
				for ($j=0; $j < count($table); $j++) { 
					if ($shareTable[$i][0]==$table[$j][0]) {
						$exist = TRUE;
						break;
					}					
				}	
				if (!$exist) {					
					array_push($tempArray, array($shareTable[$i][0], $shareTable[$i][1], $shareTable[$i][2]));
				}
			}
			
			if (count($tempArray) > 0) {
				for ($i=0; $i < count($tempArray); $i++) {
			//WriteLog($tempArray[$i][0] . " " . $tempArray[$i][1] . " " . $tempArray[$i][2]);
					array_push($table, array("ID" => $tempArray[$i][0], "Title" => $tempArray[$i][1], "Checked" => $tempArray[$i][2])); 
				}	
			}
			
			print(json_encode($table));
			break;
		
		case 'listGroupsAndStudents':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sql = "SELECT * FROM
					(SELECT 'All' AS Type, 'all' AS ID, 'all' AS Name, DATE(0) AS CreateDate,
					(SELECT COUNT(1) FROM FileRelations WHERE Type = 'All' AND FileID = '$id') AS Checked,
						IFNULL((SELECT DATE_FORMAT(BeginDate, GET_FORMAT(DATE, 'ISO')) FROM FileRelations WHERE Type = 'All' AND FileID = '$id'), '') AS BeginDate,
						IFNULL((SELECT DATE_FORMAT(EndDate, GET_FORMAT(DATE, 'ISO')) FROM FileRelations WHERE Type = 'All' AND FileID = '$id'), '') AS EndDate
					UNION
					SELECT 'Group' AS Type, ID, Name, CreateDate,
						(SELECT COUNT(1) FROM FileRelations WHERE Type = 'Group' AND TypeID = Groups.ID AND FileID = '$id') AS Checked,
						IFNULL((SELECT DATE_FORMAT(BeginDate, GET_FORMAT(DATE, 'ISO')) FROM FileRelations WHERE Type = 'Group' AND TypeID = Groups.ID AND FileID = '$id'), '') AS BeginDate,
						IFNULL((SELECT DATE_FORMAT(EndDate, GET_FORMAT(DATE, 'ISO')) FROM FileRelations WHERE Type = 'Group' AND TypeID = Groups.ID AND FileID = '$id'), '') AS EndDate
					FROM Groups
					UNION
					SELECT 'Student' AS Type, ID, Name, CreateDate,
						(SELECT COUNT(1) FROM FileRelations WHERE Type = 'Student' AND TypeID = Students.ID AND FileID = '$id') AS Checked,
						IFNULL((SELECT DATE_FORMAT(BeginDate, GET_FORMAT(DATE, 'ISO')) FROM FileRelations WHERE Type = 'Student' AND TypeID = Students.ID AND FileID = '$id'), '') AS BeginDate,
						IFNULL((SELECT DATE_FORMAT(EndDate, GET_FORMAT(DATE, 'ISO')) FROM FileRelations WHERE Type = 'Student' AND TypeID = Students.ID AND FileID = '$id'), '') AS EndDate
					FROM Students) fr
					ORDER BY Type ASC, CreateDate DESC";
//$log = new Logging();
//$log->lfile('mylog.txt'); 
//$log->lwrite($sql);			
//$log->lclose();			
			$table = mysql_GetArrayRows($sql);
			print(json_encode($table));
			break;
			
		case 'save':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$title = mysql_real_escape_string($_POST['title'], $conn);
			$description = mysql_real_escape_string($_POST['description'], $conn);
			$files = split(',', mysql_real_escape_string($_POST['files'], $conn));
			$sqls = array();
			
			$username = $_SESSION['username'];
			$sql = "SELECT IFNULL(SUM(Size), 0) AS Size FROM Files WHERE ID IN ('" . join("','", $files) . "')";
//$log = new Logging();
//$log->lfile('mylog.txt'); 
//$log->lwrite($sql);			
//$log->lclose();			
			$table = mysql_GetArrayRows($sql);
			
			$size = $table[0]["Size"];
			if(count(mysql_GetArrayRows("SELECT * FROM Files WHERE ID = '$id'")) == 0) {
				$table = mysql_GetArrayRows("SELECT uuid() AS UID");
				$uuid = $table[0]["UID"];
				
//$log->lwrite("INSERT INTO `Files` VALUES('$uuid', '', 0, '$title', '$description', '', now(), '$username', adddate(now(), 5), $size, '', '')");			
				array_push($sqls, "INSERT INTO `Files` VALUES('$uuid', '', 0, '$title', '$description', '', now(), '$username', adddate(now(), 5), $size, '', '')");
				for($i=0;$i<count($files);$i++) {
//$log->lwrite("INSERT INTO `FileGroups` VALUES('$files[$i]', '$uuid', now(), '$username')");			
					array_push($sqls, "INSERT INTO `FileGroups` VALUES('$files[$i]', '$uuid', now(), '$username')");
				}
			}
			else {
				array_push($sqls, "UPDATE `Files` SET Title = '$title', Description = '$description', Size = $size WHERE ID = '$id'");
				array_push($sqls, "DELETE FROM `FileGroups` WHERE GroupID = '$id'");
				for($i=0;$i<count($files);$i++) {
					array_push($sqls, "INSERT INTO `FileGroups` VALUES('$files[$i]', '$id', now(), '$username')");
				}
			}
			mysql_ExecTransaction($sqls);
//$log->lclose();			
			break;

		case 'assign':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$selections = json_decode(str_replace('\"', '"', $_POST['json']));
			$username = $_SESSION['username'];
			
			$sqls = array();
			array_push($sqls, "DELETE FROM FileRelations WHERE FileID = '$id'");
			for($i=0;$i<count($selections);$i++) {
				$type = $selections[$i]->Type;
				$type_id = $selections[$i]->ID;
				$begindate = $selections[$i]->BeginDate;
				$enddate = $selections[$i]->EndDate;
				array_push($sqls, "INSERT INTO FileRelations VALUES('$type', '$type_id', '$id', now(), '$username', '$begindate', '$enddate')");
			}
			mysql_ExecTransaction($sqls);
			break;
			
		case 'getItem':
			$username = $_SESSION['username'];
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sql = "SELECT ID, Title, Description FROM Files WHERE ID = '$id'";
			$table = mysql_GetArrayRows($sql);
			print(json_encode($table));
			break;
			
		case 'del':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sql = "SELECT ID, ParentID, IsLeaf, Path FROM Files WHERE ID = '$id'";
			$rows = mysql_GetArrayRows($sql);
			
			if(isset($rows[0])) {
				mysql_Exec("DELETE FROM Files WHERE ID = '$id'");
				mysql_Exec("DELETE FROM FileGroups WHERE GroupID = '$id'");
			}
			break;
		
		case 'modifyName':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$text = mysql_real_escape_string($_POST['text'], $conn);
			$sql = "UPDATE Files SET Title = '$text' WHERE ID = '$id'";
			mysql_Exec($sql);
			break;

		case 'modifyDesc':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$text = mysql_real_escape_string($_POST['text'], $conn);
			$sql = "UPDATE Files SET Description = '$text' WHERE ID = '$id'";
			mysql_Exec($sql);
			break;
	}
?>