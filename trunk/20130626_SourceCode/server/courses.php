﻿<?php
	if($_SESSION['username'] == "") die("<script>location.href='login.html';</script>");
	$doc_root = $_SERVER['DOCUMENT_ROOT'];
	require_once($doc_root.'/includes/file_io.php');
	require_once($doc_root.'/includes/mysql_tools.php');
	
	$upload_tmp_path = "$doc_root/upload_tmp/";
	$upload_storage_path = "$doc_root/file_storage/";

	$mime_filter = array('text/plain','text/html',
					'application/pdf','application/msword','application/rtf','application/vnd.ms-powerpoint',application/x-zip-compressed,
					'image/x-ms-bmp','image/jpeg','image/gif','image/png',
					'video/mp4',
					'audio/x-aiff','audio/mpeg','audio/mp3');

	switch($_GET['act']) {
		case 'add':
			$conn = mysql_GetConnection();
			$session = mysql_real_escape_string($_POST['session'], $conn);
			$name = mysql_real_escape_string($_POST['name'], $conn);
			$description = mysql_real_escape_string($_POST['description'], $conn);
			$files = $_POST['file'];
			$file_names = $_POST['file_name'];
			$file_descriptions = $_POST['file_description'];
			$username = $_SESSION['username'];
			
			$now = time();
			$due = $now + 2*24*60*60;
			$now_string = date('c', $now);
			$due_string = date('c', $due);

			$source_dir = $upload_tmp_path.$session.'/';
			$dest_dir = $upload_storage_path.$session.'/';
			if(!file_exists($dest_dir)) {
				mkdir($dest_dir);
			}
			
			$dir_size = 0;
			$sqls = array();
			
			$valid_count = 0;
			$invalid_count = 0;
			for($i=0;$i<count($files);$i++) {
				$file = mysql_real_escape_string($files[$i], $conn);
				$file_name = mysql_real_escape_string($file_names[$i], $conn);
				$file_description = mysql_real_escape_string($file_descriptions[$i], $conn);
				$file_ext = urldecode(pathinfo(str_replace('%2F', '/', urlencode($file_name)), PATHINFO_EXTENSION));
				$file_with_ext = "$file.$file_ext";
				$file_name_without_ext = urldecode(pathinfo(str_replace('%2F', '/', urlencode($file_name)), PATHINFO_FILENAME));
				
				$source_path = $source_dir.$file;
				$dest_path = $dest_dir.$file_with_ext;
				
				if(is_file($source_path)) {
					$size = filesize($source_path);
					$upload_info = json_decode(file_readall($source_path.'.info'));
					$mime = $upload_info->type;
					if(in_array($mime, $mime_filter)) {
						$dir_size += $size;
						copy($source_path, $dest_path);
						array_push($sqls, "INSERT INTO `Files` VALUES('$file_with_ext', '$session', 1, '$file_name_without_ext', '$file_description', '$now_string', '$username', '$due_string', $size, '$mime', '$file_ext')");
					}
				}
			}
			if(count($sqls) > 0) {
				array_push($sqls, "INSERT INTO `Files` VALUES('$session', '', 0, '$name', '$description', '$now_string', '$username', '$due_string', $dir_size, '', '')");
				mysql_ExecTransaction($sqls);
				
				dir_DeleteTree($upload_tmp_path.$session.'/');
				dir_CleanUploadTempFolder($upload_tmp_path);
				echo '!!success!!';
			}
			break;
			
		case 'list':
			$sql = 'SELECT a.ID AS CourseID, b.ID, b.IsLeaf, b.Title, b.Description, b.Size, b.CreateDate
					FROM Files a
					INNER JOIN Files b
					ON a.ID = b.ParentID
						OR a.Isleaf = 0 AND a.ID = b.ID
					ORDER BY a.CreateDate DESC, b.IsLeaf ASC';
			$table = mysql_GetArrayRows($sql);
			print(json_encode($table));
			break;
			
		case 'list_group_relation':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$dataset = array();
			$sql = "SELECT '$id' AS FileID, 'assign_all' AS ID, '' AS Name, 'checked' AS Assigned,
					BeginDate, EndDate
					FROM FileRelations
					WHERE Type = 0 AND FileID = '$id'";
			array_push($dataset, mysql_GetArrayRows($sql));

			$sql = "SELECT '$id' AS FileID, g.ID, g.Name, CASE WHEN FileID IS NULL THEN '' ELSE 'checked' END AS Assigned,
					fr.BeginDate, fr.EndDate
					FROM Groups g
					LEFT JOIN FileRelations fr
					ON fr.Type = 1 AND g.ID = fr.TypeID AND fr.FileID = '$id'
					ORDER BY g.CreateDate DESC, g.Name DESC";
			array_push($dataset, mysql_GetArrayRows($sql));
			print(json_encode($dataset));
			break;
			
		case 'assign':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$groups = $_POST['groups'];
			$begindate = mysql_real_escape_string($_POST['begindate'], $conn);
			$enddate = mysql_real_escape_string($_POST['enddate'], $conn);
			
			$username = $_SESSION['username'];
			$now = date('c');
			
			$sqls = array();
			array_push($sqls, "DELETE FROM FileRelations WHERE FileID = '$id'");
			if($_POST['assign_all'] == '1') {
				array_push($sqls, "INSERT INTO FileRelations VALUES('0', '', '$id', '$now', '$username', '$begindate', '$enddate')");
			}
			foreach($groups as $group) {
				$group = mysql_real_escape_string($group, $conn);
				array_push($sqls, "INSERT INTO FileRelations VALUES('1', '$group', '$id', '$now', '$username', '$begindate', '$enddate')");
			}
			mysql_ExecTransaction($sqls);
			break;
		
		case 'del':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sql = "SELECT ID, ParentID, IsLeaf FROM Files WHERE ID = '$id'";
			$rows = mysql_GetArrayRows($sql);
			
			if($rows[0]['IsLeaf'] == 1) {
				$parentid = $rows[0]['ParentID'];
				$sql = "SELECT COUNT(1) AS `Count` FROM Files WHERE ParentID = '$parentid'";
				$count = mysql_GetArrayRows($sql);
				if($count[0][0] > 1) {
					$folder = $rows[0]['ParentID'];
					$file = $rows[0]['ID'];
					$path = "$doc_root/file_storage/$folder/$file";
					mysql_Exec("DELETE FROM Files WHERE ID = '$id'");
					dir_DeleteTree($path);
				}
				else {
					$folder = $rows[0]['ParentID'];
					$path = "$doc_root/file_storage/$folder";
					mysql_Exec("DELETE FROM Files WHERE ID = '$parentid' OR ParentID = '$parentid'");
					mysql_Exec("DELETE FROM FileRelations WHERE FileID = '$parentid'");
					dir_DeleteTree($path);
				}
			}
			else {
				$folder = $rows[0]['ID'];
				$path = "$doc_root/file_storage/$folder";
				mysql_Exec("DELETE FROM Files WHERE ID = '$id' OR ParentID = '$id'");
				mysql_Exec("DELETE FROM FileRelations WHERE FileID = '$id'");
				dir_DeleteTree($path);
			}
			break;
	}
?>