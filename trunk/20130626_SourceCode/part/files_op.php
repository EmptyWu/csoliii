﻿<div class="file-wrapper">
	<div class="title"></div>
	<div align="left" id="files_controller" class="controller">
		<!--
		<input id="files_add_button" type="button" value="新增檔案"/>
		<input id="files_filter_button" type="button" value="篩選檔案"/>
		-->
		<img alt="新增檔案" id="files_add_button" src="../images/n_istudy/tbt01_New.png" />
		<img alt="篩選檔案" id="files_filter_button" src="../images/n_istudy/tbt05_Filter.png" />
		<!--			
			<?php if($_SESSION['usertype'] == 'root') {		?>
		<input id="files_share_button" type="button" value="分享檔案"/>						
		<img alt="分享檔案" id="files_share_button" src="../images/n_istudy/tbt12_Share.png" onclick="CurrentController.sharelists_OnClick()" />
			<?php }?>
		-->
		<img alt="移除檔案" id="files_remove_button" onclick="CurrentController.remove_OnClick()" src="../images/n_istudy/tbt14_Del_icon.png" />
		<span id="files_message" class="highlight" style="margin:0 1em"></span>
	</div>
	<div id="files_view" class="gridview"></div>
</div>
<div class="subforms">
	<div class="subform" id="files_add_view" style="display:none">
		<div class="title" style="padding: .2em 1em; background-color:#33a;color:#fff;font-size:18px;border-bottom:solid 1px #333">新增檔案</div>
		<div class="controller" style="padding: 1em"></div>
		<div class="toolbar">
			<img alt="完成" src="../images/n_istudy/tbt08_Finish.png" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.getViewLayout()"  />
			<!--<input type="button" value="完成" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.getViewLayout()"/>-->
		</div>
	</div>
	<div class="subform" id="files_filter_view" style="display:none">
		<div class="title" style="padding: .2em 1em; background-color:#33a;color:#fff;font-size:18px;border-bottom:solid 1px #333">篩選檔案</div>
		<div class="controller" style="padding: 1em">
			<div>檔案名稱：<input type="text" name="title"/></div>
			<div>檔案描述：<input type="text" name="description"/></div>
		</div>
		<div class="toolbar">
			<img alt="完成" src="../images/n_istudy/tbt08_Finish.png" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.setFilter()" />
			<img alt="取消 "src="../images/n_istudy/tbt09_Cancel.png" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.clearFilter()" />
			<!--
			<input type="button" value="完成" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.setFilter()"/>
			<input type="button" value="清除" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.clearFilter()"/>
			-->
		</div>
	</div>
		
	<div class="subform" id="courses_assign_view" style="display:none">
		<div class="title" style="padding: .2em 1em; background-color:#33a;color:#fff;font-size:18px;border-bottom:solid 1px #333">分享清單</div>
		<div class="controller" style="padding: 1em">
			<input type="hidden" name="id"/>
			<div>檔案名稱：<span name="title" style="color:gray"/></span>
			<hr/>
			<div class="all">
				<div class="item">
					<input type="hidden" class="type" value="All"/>
					<label for="courses_assign_all" style="display:inline"><input type="checkbox" class="check" id="courses_share_all" value="all"/>分享給所有人</label>
				</div>
			</div>
			<hr/>
			<div>
				<div>分享給教師：</div>
				<div>篩選：<input type="text" class="filter" name="filter_groups"/><input type="button" value="▼"/></div>
				<div class="operators" style="border:dashed 1px #93f"><a href="#" class="switch" onclick="return false">[展開]</a><div class="list" style="display:none"></div></div>
			</div>
		</div>
		<!--<div class="toolbar"><input type="button" value="完成" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.saveAssign()"/><input type="button" value="取消" onclick="Overlay.hide(this.up('.subform').hide())"/></div>-->
		<div class="toolbar">
			<img alt="完成" src="../images/n_istudy/tbt08_Finish.png" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.saveAssign()" />
			<img alt="取消" src="../images/n_istudy/tbt09_Cancel.png" onclick="Overlay.hide(this.up('.subform').hide())" />
		</div>
	</div>

</div>
<div class="upload_template" style="display:none">
	<div class="upload">
		<div class="view view_ie">
			<div class="action control">
				<form action="upload/uploadFile.php" method="post" enctype="multipart/form-data" name="upload" target="#{id}">
					<input type="hidden" name="APC_UPLOAD_PROGRESS" value="" />
					<table><tr><td><input type="file" name="file" style="width:55px;height:20px"/></td><td><span class="filename"></span></td><td><input type="submit" value="上傳" class="submit" style="height:20px"/></td></tr></table>
				</form>
				<iframe id="#{id}" name="#{id}"></iframe>
			</div>
			<div class="message" style="display:none"><span class="progressbar"><span class="progress"></span></span><span class="filename"></span>&nbsp;<span class="status">上傳中，請稍候...</span></div>
		</div>
		<div class="view view_else">
			<div class="action"><a href="#" class="button browse">瀏覽檔案...</a></div>
			<div class="message" style="display:none"><span class="progressbar"><span class="progress"></span></span><span class="filename"></span>&nbsp;<span class="status">上傳中，請稍候...</span></div>
			<div class="control">
				<form action="upload/uploadFile.php" method="post" enctype="multipart/form-data" name="upload" target="#{id}">
					<input type="hidden" name="APC_UPLOAD_PROGRESS" value="" /><input type="file" name="file"/><input type="submit" value="上傳"/>
				</form>
				<iframe id="#{id}" name="#{id}"></iframe>
			</div>
	<div class="share_format" align="left"><img src="../images/n_istudy/share_Format.png" /></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var FileController = Class.create({
		initialize: function(view, addView, messageView, addButton, filterView, filterButton, assignView) {
			this.view = $(view);
			this.addView = $(addView);
			this.addButton = $(addButton);
			this.messageView = $(messageView);
			this.filterView = $(filterView);
			this.filterButton = $(filterButton);
			this.assignView = $(assignView);
			
			this.filter = {title: "", description: ""};
			this.ajaxCount = 0;
			this.retry = 0;
			this.addButton.observe("click", this.add_OnClick);
			this.filterButton.observe("click", this.filter_OnClick);
			this.getViewLayout(this.filter);
		},
		getViewLayout: function(filter) {
			filter = filter || CurrentController.filter;
			showLoading();
			new Ajax.Request("server/files.php?act=list", {
				parameters: filter,
				onSuccess: function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					CurrentController.retry = 0;
					var table = transport.responseText.evalJSON();
					<?php //if($_SESSION['usertype'] == 'root') {		
					?>					
					//CurrentController.organizeViewLayoutRoot(table);
					<?php 
					//}
					?>
					<?php //if($_SESSION['usertype'] == 'operator') {		
					?>					
					CurrentController.organizeViewLayoutOperator(table);
					<?php 
					//}
					?>
					//CurrentController.view.previous(".title").update("管理檔案(" + table.length + ")");
					hideLoading();
				},
				onFailure: function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					if($(CurrentController.view.identify()) && CurrentController.retry < 3) {
						CurrentController.showMessage("更新資料失敗，等待重試中...");
						(function() {CurrentController.getViewLayout()}).delay(5);
						CurrentController.retry++;
					}
					if(CurrentController.retry >= 3) {
						CurrentController.showMessage("更新資料失敗，請稍候再試...");
					}
				}
			});
		},
		/*
		organizeViewLayoutRoot: function(table) {
			var html = "<table>";
			var btnShare = document.getElementById("files_share_button");
			if (table.length == 0)
			{
				btnShare.disabled = "disabled";
				html += "<tr><th>檔案名稱</th><th>檔案描述</th><th>建立時間</th><th>檔案大小</th><th>動作</th></tr>";
				html += "<tr><td colspan=\"5\">目前還沒建立課程...</td></tr>";			
			}
			else
			{
				btnShare.disabled = "";
				html += "<tr><th><input id=\"files_checkAll\" type=\"checkbox\" /></th><th>檔案名稱</th><th>檔案描述</th><th>建立時間</th><th>檔案大小</th><th>動作</th></tr>";				
			}
			for(var i=0;i<table.length;i++) {
				table[i]["SizeView"] = Utility.getSize(table[i]['Size']);
				var tmp = "<tr>";
				tmp += "<td><input class=\"files_check\" type=\"checkbox\" /><input class=\"del_fileid\" type=\"hidden\" value=\"#{ID}\"/></td>";
				tmp += "<td><a alt=\"點一下可修改檔名...\" title=\"點一下可修改檔名...\" onclick=\"CurrentController.showModifyName(this);return false\" href=\"#\">#{Title}</a><input type=\"hidden\" value=\"#{ID}\"/><input type=\"text\" style=\"display:none;width:90%\" onblur=\"CurrentController.modifyName(this)\"/></td>";
				tmp += "<td><a alt=\"點一下可修改描述...\" title=\"點一下可修改描述...\" onclick=\"CurrentController.showModifyDesc(this);return false\" href=\"#\">#{Description}</a><input type=\"hidden\" value=\"#{ID}\"/><input type=\"text\" style=\"display:none;width:90%\" onblur=\"CurrentController.modifyDesc(this)\"/></td>";
				tmp += "<td>#{CreateDate}</td>";
				tmp += "<td>#{SizeView}</td>";
				//tmp += "<td><img alt=\"分享清單\" class=\"files_share_list\" src=\"../images/n_istudy/tbt15_ShareInventory.png\" />&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"hidden\" value=\"#{ID}\"/><img alt=\"移除檔案\" class=\"files_remove\" src=\"../images/n_istudy/tbt14_Del_icon.png\" /><input type=\"hidden\" value=\"#{ID}\"/></td>";
				tmp += "<td><img alt=\"分享清單\" class=\"files_share_list\" src=\"../images/n_istudy/tbt15_ShareInventory.png\" />&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"hidden\" value=\"#{ID}\"/>&nbsp;&nbsp;&nbsp;&nbsp;<img alt=\"下載檔案\" class=\"files_download\" src=\"../images/n_istudy/tbt06_Down.png\" /><input type=\"hidden\" value=\"#{ID}\"/></td>";
				tmp += "</tr>";
				html += new Template(tmp).evaluate(table[i]);
			}
			html += "</table>";
			this.view.update(html);
			//this.view.select(".files_remove").invoke("observe", "click", this.remove_OnClick);
			this.view.select(".files_download").invoke("observe", "click", this.download_OnClick);
			this.view.select(".files_check").invoke("observe", "click", this.check_OnClick);
			this.view.select(".files_share_list").invoke("observe", "click", this.shareSingle_OnClick);
			this.view.select("#files_checkAll").invoke("observe", "click", this.checkAll_OnClick);
		},	
		*/	
		organizeViewLayoutOperator: function(table) {
			var html = "<table>";
			if(table.length == 0) {
				//btnShare.disabled = "disabled";
				html += "<tr><th>檔案名稱</th><th>檔案描述</th><th>檔案建立者</th><th>建立時間</th><th>檔案大小</th><th>動作</th></tr>";
				html += "<tr><td colspan=\"6\">目前還沒建立課程...</td></tr>";
			}
			else {
				//btnShare.disabled = "";
				html += "<tr><th><input id=\"files_checkAll\" type=\"checkbox\" /></th><th>檔案名稱</th><th>檔案描述</th><th>檔案建立者</th><th>建立時間</th><th>檔案大小</th><th>動作</th></tr>";								
			}
			 
			for(var i=0;i<table.length;i++) {
				table[i]["SizeView"] = Utility.getSize(table[i]['Size']);
				var down = table[i]['Download'];
				var tmp = "<tr>";
                if (down == 'Y') {
					tmp += "<td><input class=\"files_check\" type=\"checkbox\" /><input class=\"del_fileid\" type=\"hidden\" value=\"#{ID}\"/></td>";
				}	
				else {
					tmp += "<td>&nbsp;</td>";					
				}			
				tmp += "<td><a alt=\"點一下可修改檔名...\" title=\"點一下可修改檔名...\" onclick=\"CurrentController.showModifyName(this);return false\" href=\"#\">#{Title}</a><input type=\"hidden\" value=\"#{ID}\"/><input type=\"text\" style=\"display:none;width:90%\" onblur=\"CurrentController.modifyName(this)\"/></td>";
				tmp += "<td><a alt=\"點一下可修改描述...\" title=\"點一下可修改描述...\" onclick=\"CurrentController.showModifyDesc(this);return false\" href=\"#\">#{Description}</a><input type=\"hidden\" value=\"#{ID}\"/><input type=\"text\" style=\"display:none;width:90%\" onblur=\"CurrentController.modifyDesc(this)\"/></td>";
				tmp += "<td>#{CreateBy}</td>"
				tmp += "<td>#{CreateDate}</td>";
				tmp += "<td>#{SizeView}</td>";
				//tmp += "<td><input class=\"files_remove\" type=\"button\" value=\"移除檔案\"/><input type=\"hidden\" value=\"#{ID}\"/><input class=\"files_download\" type=\"button\" value=\"下載檔案\"/><input type=\"hidden\" value=\"#{ID}\"/></td>";
                if (down == 'Y') {
 					tmp += "<td><img alt=\"下載檔案\" class=\"files_download\" src=\"../images/n_istudy/tbt06_Down.png\" /><input type=\"hidden\" value=\"#{ID}\"/></td>";              	
                }
                else {
 					tmp += "<td><img alt=\"下載檔案\" class=\"files_not_download\" src=\"../images/n_istudy/tbt17_n_Down.png\" onclick=\"alert('檔案僅有擁有者才可下載，您只有打包與發派課程的權限。')\" /></td>";              	                	
                }
                
				tmp += "</tr>";
				html += new Template(tmp).evaluate(table[i]);
			}
			html += "</table>";
			this.view.update(html);
			//this.view.select(".files_remove").invoke("observe", "click", this.remove_OnClick);
			this.view.select(".files_download").invoke("observe", "click", this.download_OnClick);
			this.view.select(".files_check").invoke("observe", "click", this.check_OnClick);
			this.view.select("#files_checkAll").invoke("observe", "click", this.checkAll_OnClick);
		},
		getAddFileLayout: function() {
			this.appendAddFileControl();
			if(!this.addView.visible()) new Overlay().show(this.addView.show());
		},
		saveAssign: function() {
			var id = CurrentController.assignView.down("input[name='id']").value;
			var shareAll = CurrentController.assignView.down("input[id='courses_share_all']");
			var params = [];
			if (shareAll.checked) {
				$$('.share_check').each(function(item) {
					//if (item.checked) {
					params.push({
						"ACC":item.value,
					});
					//}
				});									
			}
			else {
				$$('.share_check').each(function(item) {
					if (item.checked) {
						params.push({
							"ACC":item.value,
						});
					}
				});					
			}
			
			
			new Ajax.Request("server/files.php?act=assignSingle", {
				parameters: {"id":id,"json":params.toJSON()},
				onSuccess: function(transport) {
					if(transport.responseText.indexOf("<script>") > -1)
						transport.responseText.evalScripts();
				}
			});
			CurrentController.getViewLayout();
		},
				
		/*
		sharelists_OnClick: function() {
			
		},
		shareSingle_OnClick: function() {
			this.disabled="fasle";
			var id = this.next("input[type='hidden']").value;
			CurrentController.ajaxCount = 0;
			new Ajax.Request("server/files.php?act=getItem", {
				parameters: {"id":id},
				onSuccess: function(transport) {
					CurrentController.ajaxCount --;
					if(transport.responseText.indexOf("<script>") > -1)
						transport.responseText.evalScripts();
					var table = transport.responseText.stripScripts().evalJSON();
					if(table.length != 1) return;
					CurrentController.assignView.down("input[name='id']").value = table[0]["ID"];
					CurrentController.assignView.down("span[name='title']").innerText = table[0]["FileName"];
				}

			});
			CurrentController.ajaxCount ++;
			
			new Ajax.Request("server/files.php?act=listOperator", {
				parameters: {"id":id},
				onSuccess: function(transport) {
					CurrentController.ajaxCount --;
					if(transport.responseText.indexOf("<script>") > -1)
						transport.responseText.evalScripts();
					var table = transport.responseText.stripScripts().evalJSON();
					var htmlOperators = "";
					for(var i=0;i<table.length;i++) {
						var tmp = "<div class=\"item\">";
						tmp += "<input type=\"hidden\" class=\"type\" value=\"Operator\"/>";
						tmp += "<label for=\"courses_assign_operators_#{Acc}\" style=\"display:inline\"><input type=\"checkbox\" class=\"share_check\" id=\"courses_assign_operators_#{Acc}\" value=\"#{Acc}\" " + table[i][4] + " />#{Acc}</label>";
						tmp += "</div>";
						htmlOperators += new Template(tmp).evaluate(table[i]);
						
					}
					CurrentController.switch_OnClick(null, CurrentController.assignView.down(".operators .list").update(htmlOperators).previous("a"), true);
					CurrentController.assignView.select(".switch").invoke("observe", "click", CurrentController.switch_OnClick);
					CurrentController.assignView.select(".filter").invoke("observe", "blur", CurrentController.filterAssign_OnBlur);
				}
			});
			CurrentController.ajaxCount ++;
			
			(function() {
				if(CurrentController.ajaxCount <= 0)
					new Overlay().show($$("#courses_assign_view").first().show());
					//modalContainer(CurrentController.assignView);
				else
					this.delay(1);
			}).delay(1);
		},
		filterAssign_OnBlur: function(event) {
			var filter = this.value;
			var css;
			if(this.name == "filter_groups") {
				css = ".operators .list div";
			}
			//else if(this.name == "filter_students") {
			//	css = ".students .list div";
			//}
			else return;
			CurrentController.assignView.select(css).each(function(element) {
				if(element.innerHTML.stripTags().indexOf(filter) > -1)
					element.show();
				else
					element.hide();
			});
		},
		*/
		appendAddFileControl: function() {
			if(!$$("#files_add_view .upload .filename").any(function(element) {return element.innerHTML == ""})) {
				var id = guid();
				var html = $$(".upload_template").first().innerHTML.interpolate({"id": id});
				var control = new Element("div").update(html);

				var fail_count = 0;
				var total_count = 0;
				function getProgress() {
					new Ajax.Request("upload/getprogress.php?id=" + id, {
						onSuccess: function(transport) {
							try {
								total_count ++;
								var result = transport.responseText;
								if(parseInt(result, 10))
									control.down(".progress").setStyle("width:" + result + "%");
								else
									fail_count ++;
								if(control.down(".status").innerHTML.indexOf("完成") == -1 && fail_count < 10 && total_count < 50) getProgress.delay(0.5);
							} catch(e) { 
								if(typeof console == "object") console.log(e) 
							}
						}
					});
				}

				if(Prototype.Browser.IE) {
					control.select(".view").without(control.down(".view_ie")).invoke("remove");
					control.down("input[type='file']").observe("change", (function(event) {
						if(Event.element(event).value == "") {
							control.select(".view .filename").invoke("update");
						}
						else {
							control.select(".view .filename").invoke("update", Event.element(event).value.split(/\\/g).last());
							control.down(".submit").show();
							control.down("form").observe("submit", (function(event) {
								Event.element(event).up(".upload").down(".action").hide();
								Event.element(event).up(".upload").down(".message").show();
								getProgress.delay(0.5);
								this.appendAddFileControl();
							}).bind(this));
						}
					}).bind(this));
					control.down(".control").toggleClassName("control");
				}
				else {
					control.select(".view").without(control.down(".view_else")).invoke("remove");
					control.down(".browse").observe("click", function(event) { Event.element(event).up(".upload").down("input[type='file']").click() });
					control.down("input[type='file']").observe("change", (function(event) {
						if(Event.element(event).value == "") {
							control.down(".view .share_format").show();
							control.down(".view .action").show();
							control.down(".view .filename").update();
							control.down(".view .message").hide();
						}
						else {
							control.down(".view .action").hide();
							control.down(".view .filename").update(Event.element(event).value.split(/\\/g).last());
							control.down(".view .message").show();
							control.down("form").submit();
							control.down(".view .share_format").hide();
							getProgress.delay(0.5);
							this.appendAddFileControl();
						}
					}).bind(this));
				}
				
				control.down("iframe").observe("load", function(event) {
					try {
						var content = Event.element(event).contentWindow.document.body.innerHTML;
						if(content.indexOf("!!success!!") > -1) {
							control.down(".status").update("上傳完成!!");
							control.down(".progress").setStyle("width:100%");
						}
						else if(content.indexOf("!!badtype!!") > -1) {
							control.down(".status").update("格式不正確!!");
							control.down(".progress").setStyle("width:100%");
						}
						else if(content.indexOf("!!failed!!") > -1) {
							control.down(".status").update("上傳失敗，請重新上傳!!");
							control.down(".progress").setStyle("width:100%");
						}
						this.appendAddFileControl();
					} catch(e) { 
						if(typeof console == "object") console.log(e) 
					}
				}.bind(this));

				control.down("form").writeAttribute("action", control.down("form").readAttribute("action") + "?id=" + id);
				control.down("input[name='APC_UPLOAD_PROGRESS']").writeAttribute("value", id);
				this.addView.down(".controller").insert(control);
				this.addView.select(".button").invoke("observe", "click", function(event) { Event.stop(event) });
			}
			
			/*
			if(Prototype.Browser.Gecko) {
				var html = "<div class=\"upload_control\" style=\"margin-bottom:.5em\" id=\"upload_control_#{id}\">";
				html += "<form action=\"upload/uploadFile.php\" method=\"post\" enctype=\"multipart/form-data\" name=\"upload\" target=\"files_iframe_#{id}\">";
				html += "<input type=\"hidden\" name=\"APC_UPLOAD_PROGRESS\" value=\"#{id}\" />";
				html += "<a href=\"#\" style=\"color: black\" onclick=\"return false\" style=\"display:block\">瀏覽檔案...</a>";
				html += "<input type=\"file\" style=\"position:absolute;opacity:0\" class=\"real_file\" name=\"file\" ";
				html += "onmouseover=\"this.previous().setStyle('text-decoration:underline;color:#66f')\" ";
				html += "onmouseout=\"this.previous().setStyle('text-decoration:none;color:#666')\" />";
				html += "</form>";
				html += "<iframe style=\"display: none\" id=\"files_iframe_#{id}\" name=\"files_iframe_#{id}\"></iframe>";
				html += "<div class=\"progressmsg\"></div><div class=\"progressbar\" style=\"display:none;width:90%;height:20px;border:solid 1px black\" id=\"progressbar_#{id}\"><div class=\"progress\" style=\"width:0;height:24px;background-color:#33f\" id=\"progress_#{id}\"></div></div>";
				html += "</div>";
				html = new Template(html).evaluate({"id": id});
				this.addView.select(".controller")[0].insert(html);
				
				(function() {
					var upload_control = $("upload_control_" + id);
					var real_file = upload_control.select(".real_file")[0];
					var anchor = upload_control.select("a")[0];
					var width = real_file.getWidth() + "px";
					var height = real_file.getHeight() + "px";
					anchor.setStyle({"width": width, "height": height});
					real_file.clonePosition(anchor);
					real_file.observe("change", CurrentController.uploadFile_OnChange);
					anchor = null;
					real_file = null;
					upload_control = null;
				}).delay(1);
			}
			else {
				var html = "<div class=\"upload_control\" style=\"margin-bottom:.5em\" id=\"upload_control_#{id}\">";
				html += "<form action=\"upload/uploadFile.php\" method=\"post\" enctype=\"multipart/form-data\" name=\"upload\" target=\"files_iframe_#{id}\">";
				html += "<input type=\"hidden\" name=\"APC_UPLOAD_PROGRESS\" value=\"#{id}\" />";
				html += "<a href=\"#\" style=\"color: black\" onclick=\"$(this).next().click();return false\">瀏覽檔案...</a>";
				html += "<input type=\"file\" style=\"position:absolute;left:-9999px;filter:alpha(opacity=0);opacity:0\" class=\"real_file\" name=\"file\" />";
				html += "</form>";
				html += "<iframe style=\"display: none\" id=\"files_iframe_#{id}\" name=\"files_iframe_#{id}\"></iframe>";
				html += "<div class=\"progressmsg\"></div><div class=\"progressbar\" style=\"display:none;width:90%;height:24px;border:solid 1px black\" id=\"progressbar_#{id}\"><div class=\"progress\" style=\"width:0;height:24px;background-color:#33f\" id=\"progress_#{id}\"></div></div>";
				html += "</div>";
				html = new Template(html).evaluate({"id": id});
				this.addView.select(".controller")[0].insert(html);
				$("upload_control_" + id).select(".real_file")[0].observe("change", this.uploadFile_OnChange);
			}
			*/
		},

/*		
		appendAddFileControl: function() {
			if(!$$("#files_add_view .upload .filename").any(function(element) {return element.innerHTML == ""})) {

				var id = guid();
				var html = $$(".upload_template").first().innerHTML.interpolate({"id": id});
				var control = new Element("div").update(html);

				var fail_count = 0;
				var total_count = 0;
				function getProgress() {
alert("step1");
					new Ajax.Request("upload/getprogress.php?id=" + id, {
alert("step2");
						onSuccess: function(transport) {
							try {
								total_count ++;
								var result = transport.responseText;
								if(parseInt(result, 10))
									control.down(".progress").setStyle("width:" + result + "%");
								else
									fail_count ++;
								if(control.down(".status").innerHTML.indexOf("完成") == -1 && fail_count < 10 && total_count < 50) getProgress.delay(0.5);
							} catch(e) { if(typeof console == "object") console.log(e) }
						}
					});
				}

				if(Prototype.Browser.IE) {

					control.select(".view").without(control.down(".view_ie")).invoke("remove");
					control.down("input[type='file']").observe("change", (function(event) {
						if(Event.element(event).value == "") {
							control.select(".view .filename").invoke("update");
						}
						else {
							control.select(".view .filename").invoke("update", Event.element(event).value.split(/\\/g).last());
							control.down(".submit").show();
							control.down("form").observe("submit", (function(event) {
								Event.element(event).up(".upload").down(".action").hide();
								Event.element(event).up(".upload").down(".message").show();
								getProgress.delay(0.5);
								this.appendAddFileControl();
							}).bind(this));
						}
					}).bind(this));
					control.down(".control").toggleClassName("control");
				}
				else {

					control.select(".view").without(control.down(".view_else")).invoke("remove");
					control.down(".browse").observe("click", function(event) { Event.element(event).up(".upload").down("input[type='file']").click() });
					control.down("input[type='file']").observe("change", (function(event) {
						if(Event.element(event).value == "") {
							control.down(".view .action").show();
							control.down(".view .filename").update();
							control.down(".view .message").hide();
						}
						else {
							
							control.down(".view .action").hide();
							control.down(".view .filename").update(Event.element(event).value.split(/\\/g).last());
							control.down(".view .message").show();
							control.down("form").submit();
							getProgress.delay(0.5);
							this.appendAddFileControl();
						}
					}).bind(this));

				}
				
				control.down("iframe").observe("load", function(event) {
					try {
						var content = Event.element(event).contentWindow.document.body.innerHTML;
						if(content.indexOf("!!success!!") > -1) {
							control.down(".status").update("上傳完成!!");
							control.down(".progress").setStyle("width:100%");
						}
						else if(content.indexOf("!!badtype!!") > -1) {
							control.down(".status").update("格式不正確!!");
							control.down(".progress").setStyle("width:100%");
						}
						else if(content.indexOf("!!failed!!") > -1) {
							control.down(".status").update("上傳失敗，請重新上傳!!");
							control.down(".progress").setStyle("width:100%");
						}
						this.appendAddFileControl();
					} catch(e) { if(typeof console == "object") console.log(e) }
				}.bind(this));
				control.down("form").writeAttribute("action", control.down("form").readAttribute("action") + "?id=" + id);
				control.down("input[name='APC_UPLOAD_PROGRESS']").writeAttribute("value", id);
				this.addView.down(".controller").insert(control);
				this.addView.select(".button").invoke("observe", "click", function(event) { Event.stop(event) });

			}
			/*
			if(Prototype.Browser.Gecko) {
				var html = "<div class=\"upload_control\" style=\"margin-bottom:.5em\" id=\"upload_control_#{id}\">";
				html += "<form action=\"upload/uploadFile.php\" method=\"post\" enctype=\"multipart/form-data\" name=\"upload\" target=\"files_iframe_#{id}\">";
				html += "<input type=\"hidden\" name=\"APC_UPLOAD_PROGRESS\" value=\"#{id}\" />";
				html += "<a href=\"#\" style=\"color: black\" onclick=\"return false\" style=\"display:block\">瀏覽檔案...</a>";
				html += "<input type=\"file\" style=\"position:absolute;opacity:0\" class=\"real_file\" name=\"file\" ";
				html += "onmouseover=\"this.previous().setStyle('text-decoration:underline;color:#66f')\" ";
				html += "onmouseout=\"this.previous().setStyle('text-decoration:none;color:#666')\" />";
				html += "</form>";
				html += "<iframe style=\"display: none\" id=\"files_iframe_#{id}\" name=\"files_iframe_#{id}\"></iframe>";
				html += "<div class=\"progressmsg\"></div><div class=\"progressbar\" style=\"display:none;width:90%;height:20px;border:solid 1px black\" id=\"progressbar_#{id}\"><div class=\"progress\" style=\"width:0;height:24px;background-color:#33f\" id=\"progress_#{id}\"></div></div>";
				html += "</div>";
				html = new Template(html).evaluate({"id": id});
				this.addView.select(".controller")[0].insert(html);
				
				(function() {
					var upload_control = $("upload_control_" + id);
					var real_file = upload_control.select(".real_file")[0];
					var anchor = upload_control.select("a")[0];
					var width = real_file.getWidth() + "px";
					var height = real_file.getHeight() + "px";
					anchor.setStyle({"width": width, "height": height});
					real_file.clonePosition(anchor);
					real_file.observe("change", CurrentController.uploadFile_OnChange);
					anchor = null;
					real_file = null;
					upload_control = null;
				}).delay(1);
			}
			else {
				var html = "<div class=\"upload_control\" style=\"margin-bottom:.5em\" id=\"upload_control_#{id}\">";
				html += "<form action=\"upload/uploadFile.php\" method=\"post\" enctype=\"multipart/form-data\" name=\"upload\" target=\"files_iframe_#{id}\">";
				html += "<input type=\"hidden\" name=\"APC_UPLOAD_PROGRESS\" value=\"#{id}\" />";
				html += "<a href=\"#\" style=\"color: black\" onclick=\"$(this).next().click();return false\">瀏覽檔案...</a>";
				html += "<input type=\"file\" style=\"position:absolute;left:-9999px;filter:alpha(opacity=0);opacity:0\" class=\"real_file\" name=\"file\" />";
				html += "</form>";
				html += "<iframe style=\"display: none\" id=\"files_iframe_#{id}\" name=\"files_iframe_#{id}\"></iframe>";
				html += "<div class=\"progressmsg\"></div><div class=\"progressbar\" style=\"display:none;width:90%;height:24px;border:solid 1px black\" id=\"progressbar_#{id}\"><div class=\"progress\" style=\"width:0;height:24px;background-color:#33f\" id=\"progress_#{id}\"></div></div>";
				html += "</div>";
				html = new Template(html).evaluate({"id": id});
				this.addView.select(".controller")[0].insert(html);
				$("upload_control_" + id).select(".real_file")[0].observe("change", this.uploadFile_OnChange);
			}
			
		},
*/
		switch_OnClick: function(event, element, hide) {
			element = element || this;
			var list = element.next(".list");
			if(list.visible() || hide) {
				list.hide();
				element.update("[展開]");
			}
			else {
				list.show();
				element.update("[收合]");
			}
			element = null;
		},
		showMessage: function(message) {
			CurrentController.messageView.update(message);
			(function() {CurrentController.messageView.update();view=null}).delay(3);
		},
		add_OnClick: function() {
			CurrentController.getAddFileLayout();
		},
		filter_OnClick: function() {
			new Overlay().show($$("#files_filter_view").first().show());
		},
		remove_OnClick: function() {
			if(confirm('確定要刪除檔案!?')) {
				var params = [];
				$$('.files_check').each(function(item) {
					if (item.checked) {
						params.push({
							"ID":item.next().value,
						});
					}
				});				
				new Ajax.Request("server/files.php?act=del", {
					//parameters: "id=" + this.next().value,
					parameters: {"json":params.toJSON()},
					onSuccess:function(transport) {
						if(transport.responseText.indexOf("<script>") > -1) {
							transport.responseText.evalScripts();
						}
						CurrentController.showMessage("刪除完成...");
						CurrentController.getViewLayout();
					}
				});
			}
		},
		download_OnClick: function() {
			if(confirm('確定要下載檔案!?')) {
				window.open("server/files.php?act=download&id=" + this.next().value);
				//new Ajax.Request("server/files.php?act=download", {
				//	parameters: "id=" + this.next().value,
				//	onSuccess:function(transport) {
				//		if(transport.responseText.indexOf("<script>") > -1) {
				//			transport.responseText.evalScripts();
				//		}
				//		CurrentController.showMessage("下載完成...");
				//		CurrentController.getViewLayout();
				//	}
				//});
			}
		},
		checkAll_OnClick: function () {
			if ($('files_checkAll').checked) {
				$$('.files_check').each(function(item) {
					item.checked = true; 
				});
			}
			else {
				$$('.files_check').each(function(item) {
					item.checked = false;
				});				
			}
		},
		check_OnClick: function () {
			var allCheck = true;
			$$('.files_check').each(function(item) {
				if (!item.checked)
				{
					allCheck = false;
				}
			});	
			if (allCheck) {
				$('files_checkAll').checked = true;
			}	
			else {
				$('files_checkAll').checked = false;
			}					
		},
		showModifyName: function(element) {
			var text = element.innerHTML;
			element.hide().next("input[type='text']").show().activate().value = text;
			element = null;
		},
		showModifyDesc: function(element) {
			var text = element.innerHTML;
			element.hide().next("input[type='text']").show().activate().value = text;
			element = null;
		},
		modifyName: function(element) {
			var id = element.previous().value;
			var text = element.value;
			new Ajax.Request("server/files.php?act=modifyName", {
				parameters:{"id":id, "text":text},
				onComplete: function() {
					CurrentController.getViewLayout();
				}
			});
		},
		modifyDesc: function(element) {
			var id = element.previous().value;
			var text = element.value;
			new Ajax.Request("server/files.php?act=modifyDesc", {
				parameters:{"id":id, "text":text},
				onComplete: function() {
					CurrentController.getViewLayout();
				}
			});
		},
		setFilter: function() {
			CurrentController.filter.title = CurrentController.filterView.down("input[name='title']").value;
			CurrentController.filter.description = CurrentController.filterView.down("input[name='description']").value;
			if(CurrentController.filter.title == "" && CurrentController.filter.description == "")
				CurrentController.filterButton.setStyle("color:black");
			else
				CurrentController.filterButton.setStyle("color:blue");				
			CurrentController.getViewLayout();
		},
		clearFilter: function() {
			CurrentController.filterView.down("input[name='title']").value = "";
			CurrentController.filterView.down("input[name='description']").value = "";
			CurrentController.setFilter();
		},
		uploadFile_OnChange: function() {
			var form = this.up("form");
			var upload_control = this.up(".upload_control");
			var progressmsg = upload_control.down(".progressmsg");
			var progressbar = upload_control.down(".progressbar");

			if(Prototype.Browser.IE) form.click();
			else form.submit();
			form.hide();
			progressmsg.update("正在上傳 " + Utility.getFilename(this.value));
			progressbar.show();
			
			CurrentController.monitorUploadProgress(this.previous("input[name='APC_UPLOAD_PROGRESS']").value);
			CurrentController.appendAddFileControl();
			
			form = null;
			upload_control = null;
			progressmsg = null;
			progressbar = null;
		},
		monitorUploadProgress: function(id) {
			new Ajax.Request("upload/getprogress.php?uid=" + id, {
				method:"get",
				onSuccess:function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					var percent = parseFloat(transport.responseText);
					$("progress_"+id).setStyle({width:percent.toString()+"%"});
					if(percent < 100)
						CurrentController.monitorUploadProgress.delay(0.5, id);
					else {
						try {
							var upload_response = $("files_iframe_" + id).contentWindow.document.body.innerHTML;
							if(upload_response.indexOf('!!badtype!!') > -1) alert("您上傳的檔案格式不正確...");
							if(upload_response.indexOf('!!failed!!') > -1) alert("上傳失敗，請再試一次...");
							if(upload_response.indexOf('!!badtype!!') > -1 || upload_response.indexOf('!!failed!!') > -1) {
								$("upload_control_"+id).remove();
								return;
							}
							var filename = Utility.getFilenameWithoutExt($("progressbar_"+id).up().down("input[type='file']").value);
							$("progressbar_"+id).hide().previous().update("檔案<span class=\"highlight\">" + filename + "</span>新增完成...");
						}catch(e){CurrentController.monitorUploadProgress.delay(0.5, id)}
					}
				}
			})
		}
	});
	
	window.CurrentController = new FileController("files_view", "files_add_view", "files_message", "files_add_button", "files_filter_view", "files_filter_button", "courses_assign_view");
	//window.CurrentController = new FileController("files_view", "files_add_view", "files_message", "files_add_button", "files_filter_view", "files_filter_button");
</script>
