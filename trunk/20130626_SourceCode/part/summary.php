﻿<div class="summary-wrapper">
	<div class="summary">
		<div class="title"><img src="../images/n_istudy/System-Overview_title.png" width="519" height="41" /></div>
		<div class="description">
<table align="center">
<tr>
<td id="system_text">
			<p class="sub_title">
			<span id="summary_title">0</span>,歡迎使用iStudy學習帶著走學習知識管理系統，您可使用的管理功能統計數據如下：
				<br/>目前共有
			</p>
			<?php if($_SESSION['usertype'] == 'root') {		?>
			<!--<p>&nbsp;&nbsp;-&nbsp;&nbsp;<span id="summary_operators">0</span>&nbsp;個操作人員</p>-->
			<center>
			<p>-&nbsp;&nbsp;<span id="summary_root_teacher">0</span>&nbsp;個老師</p>
			<p>-&nbsp;&nbsp;<span id="summary_root_students">0</span>&nbsp;個學生</p>
			<p>-&nbsp;&nbsp;<span id="summary_root_groups">0</span>&nbsp;個班級(分類)</p>
			<p>-&nbsp;&nbsp;<span id="summary_root_files">0</span>&nbsp;個檔案(系統上傳統計)</p>
			<p>-&nbsp;&nbsp;<span id="summary_root_own_files">0</span>&nbsp;個檔案(個人上傳統計)</p>
			</center>
			<?php }?>
			<?php if($_SESSION['usertype'] == 'operator') {	?>
			<!--<p>-&nbsp;&nbsp;<span id="summary_groups">0</span>&nbsp;個班級(分類)</p>
			<p>-&nbsp;&nbsp;<span id="summary_students">0</span>&nbsp;個學生</p>
			<p>-&nbsp;&nbsp;<span id="summary_files">0</span>&nbsp;個檔案</p>
			<p>-&nbsp;&nbsp;<span id="summary_courses">0</span>&nbsp;個課程</p>-->
			<center>
			<p>-&nbsp;&nbsp;<span id="summary_courses">0</span>&nbsp;個課程</p>
			<p>-&nbsp;&nbsp;<span id="summary_files">0</span>&nbsp;個檔案</p>
			</center>
			<?php }?>
</td>
</tr>
</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	(function loadSummary() {
		new Ajax.Request("server/summary.php?act=list", {
			onSuccess:function(transport) {
				if(transport.responseText.indexOf("<script>") > -1) {
					transport.responseText.evalScripts();
				}
				var table = transport.responseText.evalJSON();
				<?php if($_SESSION['usertype'] == 'root') {		?>
				//$("summary_operators").update(table[0][0]);				
				$("summary_title").update(table[5][0] + "管理者您好");
				$("summary_root_teacher").update(table[0][0]);
				$("summary_root_students").update(table[1][0]);
				$("summary_root_groups").update(table[2][0]);
				$("summary_root_files").update(table[3][0]);
				$("summary_root_own_files").update(table[4][0]);
				<?php }?>
				<?php if($_SESSION['usertype'] == 'operator') {	?>
				//$("summary_groups").update(table[0][0]);
				//$("summary_students").update(table[1][0]);
				//$("summary_files").update(table[2][0]);
				//$("summary_courses").update(table[3][0]);
				$("summary_title").update(table[2][0] + "老師您好");
				$("summary_courses").update(table[0][0]);
				$("summary_files").update(table[1][0]);
				<?php }?>
			}
		});
	}) ();
</script>