<?php
	$doc_root = $_SERVER['DOCUMENT_ROOT'];

	require_once($doc_root.'/includes/logger.php');
	require_once($doc_root.'/includes/mysql_config.php');
	
	function mysql_LogError($conn) {
		if($conn) {
			$error_no = mysql_errno($conn);
			$error = mysql_error($conn);
			log_mysql_error("($error_no)$error");
		}
	}
	function mysql_LogSqlCommandError($conn, $sql) {
		$error_no = mysql_errno($conn);
		$error = mysql_error($conn);
		log_mysql_error("[sql: $sql]($error_no)$error");
	}
	
	function mysql_GetConnection() {
		global $mysql_config;
		$conn = mysql_connect($mysql_config['server'], $mysql_config['username'], $mysql_config['password']);
		if(!$conn) {
			mysql_LogError($conn);
			die();
		}
		mysql_query("SET NAMES 'utf8'");
		$db_selected = mysql_select_db($mysql_config['default_database'], $conn);
		if(!$db_selected) {
			mysql_LogError($conn);
			die();
		}
		return $conn;
	}
	
	function mysql_GetOjbectRows($sql) {
		$conn = mysql_GetConnection();
		$result = mysql_query($sql);
		if(!$result) {
			mysql_LogSqlCommandError($conn, $sql);
			die();			
		}
		$arr = array();
		while($row = mysql_fetch_object($result)) {
			array_push($arr, $row);
		}
		mysql_free_result($result);
		mysql_close($conn);
		return $arr;
	}
	
	function mysql_GetArrayRows($sql) {
		$conn = mysql_GetConnection();
		$result = mysql_query($sql);
		if(!$result) {
			mysql_LogSqlCommandError($conn, $sql);
			die();			
		}
		$arr = array();
		while($row = mysql_fetch_array($result)) {
			array_push($arr, $row);
		}
		mysql_free_result($result);
		mysql_close($conn);
		return $arr;
	}
	
	function mysql_GetIndexedArrayRows($sql) {
		$conn = mysql_GetConnection();
		$result = mysql_query($sql);
		if(!$result) {
			mysql_LogSqlCommandError($conn, $sql);
			die();			
		}
		$arr = array();
		while($row = mysql_fetch_assoc($result)) {
			array_push($arr, $row);
		}
		mysql_free_result($result);
		mysql_close($conn);
		return $arr;
	}
	
	function mysql_Exec($sql) {
		$conn = mysql_GetConnection();
		$result = mysql_query($sql);
		if(!$result) {
			mysql_LogSqlCommandError($conn, $sql);
			die();			
		}
		return mysql_affected_rows();
		mysql_close($conn);
	}
	
	function mysql_ExecTransaction($sqls) {
		$conn = mysql_GetConnection();
		$success = true;
		mysql_query("START TRANSACTION", $conn);
		foreach($sqls as $sql) {
			mysql_query($sql);
			$pieces = preg_split("/ /",$sql);
			if(mysql_affected_rows() == 0 && $pieces[0] != 'DELETE') {
				mysql_LogSqlCommandError($conn, $sql);
				$success = false;
			}
		}

		if($success) {
			mysql_query("COMMIT", $conn);
		}
		else {
			mysql_query("ROLLBACK", $conn);
		}
		mysql_close($conn);
		return $success;
	}
?>