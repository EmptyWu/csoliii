﻿<p class="title">管理操作人員</p>
<div id="operators_view">
	<div id="operators_add" class="controller">
		<form id="operators_add_form" method="post" action="server/operators.php?act=add" enctype="application/x-www-form-urlencoded" onsubmit="return false">
			<table>
				<tr>
					<td><label for="operators_add_acc">帳號</label></td><td><input type="text" id="operators_add_acc" name="acc" class="textfield" /></td>
					<td style="padding-left:2em"><label for="operators_add_name">姓名</label></td><td><input type="text" id="operators_add_name" name="name" class="textfield" /></td>
					<td rowspan="2"><input type="button" value="新增" style="margin:auto .2em;height:4em" id="operators_add_button"/></td><td rowspan="2"><span id="operators_message" class="message"></span></td>
				</tr>
				<tr>
					<td><label for="operators_add_password">密碼</label></td><td><input type="password" id="operators_add_password" name="password" class="textfield" /></td>
					<td style="padding-left:2em"><label for="operators_add_password_retype">再一次密碼</label></td><td><input type="password" id="operators_add_password_retype" name="password_retype" class="textfield" /></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="operators_grid" class="gridview"></div>
	<div style="display:none">
		<form id="operators_list_form" method="post" action="server/operators.php?act=list" enctype="application/x-www-form-urlencoded" onsubmit="return false"></form>
		<form id="operators_delete_form" method="post" action="server/operators.php?act=delete" enctype="application/x-www-form-urlencoded" onsubmit="return false"><input type="hidden" name="acc"/></form>
	</div>
</div>
<script type="text/javascript">
	$("operators_list_form").async = function() {
		$("operators_list_form").request({
			onSuccess:function(transport) {
				if(transport.responseText.indexOf("<script>") > -1) {
					transport.responseText.evalScripts();
				}
				var table = transport.responseText.evalJSON();
				var html = "<table>";
				html += "<tr><th>帳號</th><th>姓名</th><th>建立時間</th><th>最後登入</th><th>IP</th><th>動作</th></tr>";
				if(table.length == 0) html += "<tr><td colspan=\"6\">目前還沒建立操作人員資料...</td></tr>";
				for(var i=0;i<table.length;i++) {
					var row = table[i];
					html += "<tr>";
					html += "<td>" + row["Acc"] + "</td>";
					html += "<td>" + row["Name"] + "</td>";
					html += "<td>" + row["CreateDate"] + "</td>";
					html += "<td>" + row["LastLogin"] + "</td>";
					html += "<td>" + row["LastIP"] + "</td>";
					html += "<td><input type=\"button\" value=\"刪除\" onclick=\"if(confirm('確定要刪除操作者!?')) $$('#operators_delete_form input[name=acc]')[0].value='" + row["Acc"] + "';$('operators_delete_form').async()\"/></td>";
					html += "</tr>";
				}
				html += "</table>";
				$("operators_grid").update(html);
				$("operators_list_form").reset();
			}
		});
	};
	
	$("operators_delete_form").async = function() {
		$("operators_delete_form").request({
			onSuccess:function(transport) {
				if(transport.responseText.indexOf("<script>") > -1) {
					transport.responseText.evalScripts();
				}
				$("operators_delete_form").reset();
				$("operators_list_form").async();
			}
		});
	};
	
	$("operators_add_button").observe("click", function() {
		var valid = true;
		$("operators_add_form").getInputs().each(function(element) {
			if(element.value.strip() == "") {
				element.activate();
				$("operators_message").update("請輸入" + $$("label[for='" + element.id + "']")[0].innerHTML).show();
				(function() {$("operators_message").fade({duration:0.5});}).delay(3);
				valid = false;
				throw $break;
			}
			if(element.id == "operators_add_password") {
				if($("operators_add_password").value != $("operators_add_password_retype").value) {
					$("operators_add_password").value = "";
					$("operators_add_password_retype").value = "";
					element.activate();
					$("operators_message").update("兩次密碼不一樣，請再輸入一次...").show();
					(function() {$("operators_message").fade({duration:0.5});}).delay(3);
					valid = false;
					throw $break;
				}
			}
		});
		if(!valid) {
			return;
		}
		$("operators_add_form").request({
			onSuccess: function(transport) {
				if(transport.responseText.indexOf("<script>") > -1) {
					transport.responseText.evalScripts();
				}
				if(transport.responseText.indexOf("dup_acc") > -1) {
					$("operators_message").update("帳號重覆了...").show();
					(function() {$("operators_message").fade({duration:0.5});}).delay(3);
				}
				else if(transport.responseText.indexOf("dup_acc") > -1) {
					$("operators_message").update("兩次密碼不一樣，請再輸入一次...").show();
					(function() {$("operators_message").fade({duration:0.5});}).delay(3);
				}
				else {
					$("operators_list_form").async();
					$("operators_add_form").reset();
				}
			}
		});
	});

	$("operators_add_form").getInputs().invoke("observe", "keydown", function(event) {
		event = event || window.event;
		if(event.keyCode == Event.KEY_RETURN) {
			$("operators_add_button").click();
		}
	});

	loadCss("css/operators.css");
	$("operators_list_form").async();
	$("operators_add_form").focusFirstElement();
</script>