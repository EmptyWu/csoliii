﻿<div class="summary-wrapper">
	<div class="summary">
		<p class="title">歡迎使用學習帶著走 - 學習知識管理系統</p>
		<div class="description">
			<p class="sub_title">目前共有</p>
			<?php if($_SESSION['usertype'] == 'root') {		?>
			<p>&nbsp;&nbsp;-&nbsp;&nbsp;<span id="summary_operators">0</span>&nbsp;個操作人員</p>
			<?php }?>
			<?php if($_SESSION['usertype'] == 'operator') {	?>
			<p>&nbsp;&nbsp;-&nbsp;&nbsp;<span id="summary_groups">0</span>&nbsp;個班級(分類)</p>
			<p>&nbsp;&nbsp;-&nbsp;&nbsp;<span id="summary_students">0</span>&nbsp;個學生</p>
			<p>&nbsp;&nbsp;-&nbsp;&nbsp;<span id="summary_files">0</span>&nbsp;個檔案</p>
			<p>&nbsp;&nbsp;-&nbsp;&nbsp;<span id="summary_courses">0</span>&nbsp;個課程</p>
			<?php }?>
		</div>
	</div>
</div>
<script type="text/javascript">
	(function loadSummary() {
		new Ajax.Request("server/summary.php?act=list", {
			onSuccess:function(transport) {
				if(transport.responseText.indexOf("<script>") > -1) {
					transport.responseText.evalScripts();
				}
				var table = transport.responseText.evalJSON();
				<?php if($_SESSION['usertype'] == 'root') {		?>
				$("summary_operators").update(table[0][0]);
				<?php }?>
				<?php if($_SESSION['usertype'] == 'operator') {	?>
				$("summary_groups").update(table[0][0]);
				$("summary_students").update(table[1][0]);
				$("summary_files").update(table[2][0]);
				$("summary_courses").update(table[3][0]);
				<?php }?>
			}
		});
	}) ();
</script>