﻿<div class="course-wrapper">
	<p class="title"></p>
	<div id="courses_controller" class="controller"><input id="courses_add_button" type="button" value="新增課程"/><input id="courses_filter_button" type="button" value="篩選課程"/><span id="courses_message" class="highlight" style="margin:0 1em"></span></div>
	<div id="courses_view" class="gridview"></div>
</div>
<div class="subforms">
	<div class="subform" id="courses_add_view" style="display:none">
		<div class="title" style="padding: .2em 1em; background-color:#33a;color:#fff;font-size:18px;border-bottom:solid 1px #333">維護課程</div>
		<div class="controller" style="padding: 1em">
			<input type="hidden" name="id"/>
			<div>課程名稱：<input type="text" name="title"/></div>
			<div>課程描述：<input type="text" name="description"/></div>
			<div>所屬檔案：<input type="text" name="filter"/><input type="button" name="doFilter" value="篩選"/></div>
			<div class="files" style="border:dashed 1px #93f"></div>
		</div>
		<div class="toolbar"><input type="button" value="完成" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.saveCourse()"/><input type="button" value="取消" onclick="Overlay.hide(this.up('.subform').hide())"/></div>
	</div>
	<div class="subform" id="courses_assign_view" style="display:none">
		<div class="title" style="padding: .2em 1em; background-color:#33a;color:#fff;font-size:18px;border-bottom:solid 1px #333">指派課程</div>
		<div class="controller" style="padding: 1em">
			<input type="hidden" name="id"/>
			<div>課程名稱：<input type="text" name="title" readonly="readonly" style="color:gray"/></div>
			<div>課程描述：<input type="text" name="description" readonly="readonly" style="color:gray"/></div>
			<hr/>
			<div class="all">
				<div class="item">
					<input type="hidden" class="type" value="All"/>
					<label for="courses_assign_all" style="display:inline"><input type="checkbox" class="check" id="courses_assign_all" value="all"/>指派給所有人</label>
					<label for="courses_assign_all_begindate" style="display:inline">從：<input type="text" id="courses_assign_all_begindate" class="begindate" name="begindate" style="width: 6em" readonly="readonly"/></label>
					<label for="courses_assign_all_enddate" style="display:inline">到：<input type="text" id="courses_assign_all_enddate" class="enddate" name="enddate" style="width: 6em" readonly="readonly"/></label>
				</div>
			</div>
			<hr/>
			<div>
				<div>指派給班級(分類)：</div>
				<div>篩選：<input type="text" class="filter" name="filter_groups"/><input type="button" value="▼"/></div>
				<div class="groups" style="border:dashed 1px #93f"><a href="#" class="switch" onclick="return false">[展開]</a><div class="list" style="display:none"></div></div>
			</div>
			<hr/>
			<div>
				<div>指派給學生：</div>
				<div>篩選：<input type="text" class="filter" name="filter_students"/><input type="button" value="▼"/></div>
				<div class="students" style="border:dashed 1px #93f"><a href="#" class="switch" onclick="return false">[展開]</a><div class="list" style="display:none"></div></div>
			</div>
		</div>
		<div class="toolbar"><input type="button" value="完成" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.saveAssign()"/><input type="button" value="取消" onclick="Overlay.hide(this.up('.subform').hide())"/></div>
	</div>
	<div class="subform" id="courses_filter_view" style="display:none">
		<div class="title" style="padding: .2em 1em; background-color:#33a;color:#fff;font-size:18px;border-bottom:solid 1px #333">篩選課程</div>
		<div class="controller" style="padding: 1em">
			<div>課程名稱：<input type="text" name="title"/></div>
			<div>課程描述：<input type="text" name="description"/></div>
		</div>
		<div class="toolbar"><input type="button" value="完成" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.setFilter()"/><input type="button" value="清除" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.clearFilter()"/></div>
	</div>
</div>
<script type="text/javascript">
	var FileController = Class.create({
		initialize: function(view, addView, messageView, addButton, filterView, filterButton, assignView) {
			this.view = $(view);
			this.addView = $(addView);
			this.addButton = $(addButton);
			this.messageView = $(messageView);
			this.filterView = $(filterView);
			this.filterButton = $(filterButton);
			this.assignView = $(assignView);
			
			this.filter = {title: "", description: ""};
			this.ajaxCount = 0;
			this.retry = 0;
			this.addButton.observe("click", this.add_OnClick);
			this.filterButton.observe("click", this.filter_OnClick);
			this.getViewLayout(this.filter);
		},
		getViewLayout: function(filter) {
			filter = filter || CurrentController.filter;
			showLoading();
			new Ajax.Request("server/courses2.php?act=list", {
				parameters: filter,
				onSuccess: function(transport) {
					if(transport.responseText.indexOf("<script>") > -1)
						transport.responseText.evalScripts();
					CurrentController.retry = 0;
					var table = transport.responseText.evalJSON();
					CurrentController.organizeViewLayout(table);
					//CurrentController.view.previous(".title").update("管理課程(" + table.length + ")");
					hideLoading();
				},
				onFailure: function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					if($(CurrentController.view.identify()) && CurrentController.retry < 3) {
						CurrentController.showMessage("更新資料失敗，等待重試中...");
						(function() {CurrentController.getViewLayout()}).delay(5);
						CurrentController.retry++;
					}
					if(CurrentController.retry >= 3) {
						CurrentController.showMessage("更新資料失敗，請稍候再試...");
					}
				}
			});
		},
		organizeViewLayout: function(table) {
			var html = "<table>";
			html += "<tr><th>課程名稱</th><th>課程描述</th><th>包含檔案</th><th>建立時間</th><th>課程大小</th><th>動作</th></tr>";
			if(table.length == 0) html += "<tr><td colspan=\"5\">目前還沒建立課程...</td></tr>";
			for(var i=0;i<table.length;i++) {
				table[i]["SizeView"] = Utility.getSize(table[i]['Size']);
				table[i]["ShortFilenames"] = table[i]["Filenames"].truncate(20, "...");
				table[i]["TitleFilenames"] = table[i]["Filenames"].replace(/, /g, "&#13;&#10;");
				var tmp = "<tr>";
				tmp += "<td><a alt=\"點一下可修改課程名稱...\" title=\"點一下可修改課程名稱...\" onclick=\"CurrentController.showModifyName(this);return false\" href=\"#\">#{Title}</a><input type=\"hidden\" value=\"#{ID}\"/><input type=\"text\" style=\"display:none;width:90%\" onblur=\"CurrentController.modifyName(this)\"/></td>";
				tmp += "<td><a alt=\"點一下可修改課程描述...\" title=\"點一下可修改課程描述...\" onclick=\"CurrentController.showModifyDesc(this);return false\" href=\"#\">#{Description}</a><input type=\"hidden\" value=\"#{ID}\"/><input type=\"text\" style=\"display:none;width:90%\" onblur=\"CurrentController.modifyDesc(this)\"/></td>";
				tmp += "<td title=\"#{TitleFilenames}\"><a href=\"#\" onclick=\"this.up('tr').down('.courses_modify').click();return false\">(#{FileCount})#{ShortFilenames}</a></td>";
				tmp += "<td>#{CreateDate}</td>";
				tmp += "<td>#{SizeView}</td>";
				tmp += "<td><input class=\"courses_assign\" type=\"button\" value=\"指派課程\"/><input class=\"courses_modify\" type=\"button\" value=\"修改課程\"/><input class=\"courses_remove\" type=\"button\" value=\"移除課程\"/><input type=\"hidden\" value=\"#{ID}\"/></td>";
				tmp += "</tr>";
				html += new Template(tmp).evaluate(table[i]);
			}
			html += "</table>";
			this.view.update(html);
			this.view.select(".courses_assign").invoke("observe", "click", this.assign_OnClick);
			this.view.select(".courses_modify").invoke("observe", "click", this.modify_OnClick);
			this.view.select(".courses_remove").invoke("observe", "click", this.remove_OnClick);
		},
		getAddCourseLayout: function(id) {
			id = id || "";
			showLoading();
			CurrentController.ajaxCount = 0;
			new Ajax.Request("server/courses2.php?act=getItem", {
				parameters: {"id":id},
				onSuccess: function(transport) {
					CurrentController.ajaxCount --;
					if(transport.responseText.indexOf("<script>") > -1)
						transport.responseText.evalScripts();
					var table = transport.responseText.stripScripts().evalJSON();
					if(table.length != 1) return;
					CurrentController.addView.down("input[name='id']").value = table[0]["ID"];
					CurrentController.addView.down("input[name='title']").value = table[0]["Title"];
					CurrentController.addView.down("input[name='description']").value = table[0]["Description"];
				}
			});
			CurrentController.ajaxCount ++;

			new Ajax.Request("server/courses2.php?act=listFiles", {
				parameters: {"id":id},
				onSuccess: function(transport) {
					CurrentController.ajaxCount --;
					if(transport.responseText.indexOf("<script>") > -1)
						transport.responseText.evalScripts();
					var table = transport.responseText.stripScripts().evalJSON();
					var html = "";
					for(var i=0;i<table.length;i++) {
						table[i]["Checked"] = (table[i]['Checked'] == 1) ? " checked=\"checked\"" : "";
						html += new Template("<div class=\"CourseFiles\" style=\"float:left;border:solid 1px silver;margin:.1em;color:#330000\"><label for=\"files_#{ID}\"><input id=\"files_#{ID}\" type=\"checkbox\" value=\"#{ID}\"#{Checked}/>#{Title}</label></div>").evaluate(table[i]);
					}
					html += "<div style=\"clear:both\"></div>";
					CurrentController.addView.down(".files").update(html);
					CurrentController.addView.down("input[name='doFilter']").observe("click", CurrentController.filterAddCourseFiles);
					hideLoading();
				}
			});
			CurrentController.ajaxCount ++;

			(function() {
				if(CurrentController.ajaxCount <= 0)
					new Overlay().show($$("#courses_add_view").first().show(), {aftershow: function(overlay) {overlay.holder.down("input[name='title']").activate()}});
				else
					this.delay(1);
			}).delay(1);
		},
		filterAddCourseFiles: function() {
			var filter = CurrentController.addView.down("input[name='filter']").value;
			CurrentController.addView.select(".CourseFiles").each(function(element) {
				var text = element.innerHTML.stripTags();
				if(text.indexOf(filter) > -1)
					element.show();
				else
					element.hide();
				element = null;
			});
		},
		saveCourse: function() {
			var id = CurrentController.addView.down("input[name='id']").value;
			var title = CurrentController.addView.down("input[name='title']").value.strip();
			var description = CurrentController.addView.down("input[name='description']").value.strip();
			if(title == "") {
				alert("請輸入課程名稱...");
				CurrentController.addView.down("input[name='title']").activate();
				return;
			}
			var files = [];
			CurrentController.addView.select(".CourseFiles").each(function(element) {
				if(!element.visible()) return;
				var checkbox = element.down("input[type='checkbox']");
				if(checkbox.checked) {
					files.push(checkbox.value);
				}
				checkbox = null;
				element = null;
			});
			
			new Ajax.Request("server/courses2.php?act=save", {
				parameters: {"id":id, "title":title, "description":description, "files":files.join(",")},
				onSuccess: function(transport) {
					if(transport.responseText.indexOf("<script>") > -1)
						transport.responseText.evalScripts();
					CurrentController.getViewLayout();
				}
			});
			CurrentController.addView.down("input[name='id']").value = "";
			CurrentController.addView.down("input[name='title']").value = "";
			CurrentController.addView.down("input[name='description']").value = "";
			//removeModalContainer();
		},
		cancelCourseEdit: function() {
			CurrentController.addView.down("input[name='id']").value = "";
			CurrentController.addView.down("input[name='title']").value = "";
			CurrentController.addView.down("input[name='description']").value = "";
			//removeModalContainer();
			CurrentController.getViewLayout();
		},
		saveAssign: function() {
			var id = CurrentController.assignView.down("input[name='id']").value;
			var params = [];
			CurrentController.assignView.select(".item").each(function(element) {
				if(element.down(".check").checked) {
					params.push({
						"Type":element.down(".type").value,
						"ID":element.down(".check").value,
						"BeginDate":element.down(".begindate").value,
						"EndDate":element.down(".enddate").value
					});
				}
			});
			
			new Ajax.Request("server/courses2.php?act=assign", {
				parameters: {"id":id,"json":params.toJSON()},
				onSuccess: function(transport) {
					if(transport.responseText.indexOf("<script>") > -1)
						transport.responseText.evalScripts();
				}
			});
			
			CurrentController.assignView.select(".filter").invoke("stopObserving");
			CurrentController.assignView.select(".switch").invoke("stopObserving");
			CurrentController.assignView.select(".begindate").each(function(element) {element.value=""});
			CurrentController.assignView.select(".enddate").each(function(element) {element.value=""});
			//removeModalContainer();
			CurrentController.getViewLayout();
		},
		cancelAssign: function() {
			CurrentController.assignView.select(".filter").invoke("stopObserving");
			CurrentController.assignView.select(".switch").invoke("stopObserving");
			CurrentController.assignView.select(".begindate").each(function(element) {element.value=""});
			CurrentController.assignView.select(".enddate").each(function(element) {element.value=""});
			//removeModalContainer();
			CurrentController.getViewLayout();
		},
		showMessage: function(message) {
			CurrentController.messageView.update(message);
			(function() {CurrentController.messageView.update();view=null}).delay(3);
		},
		add_OnClick: function() {
			CurrentController.getAddCourseLayout();
		},
		filter_OnClick: function() {
			new Overlay().show($$("#courses_filter_view").first().show());
			//modalContainer(CurrentController.filterView);
		},
		assign_OnClick: function() {
			var id = this.next("input[type='hidden']").value;
			CurrentController.ajaxCount = 0;
			new Ajax.Request("server/courses2.php?act=getItem", {
				parameters: {"id":id},
				onSuccess: function(transport) {
					CurrentController.ajaxCount --;
					if(transport.responseText.indexOf("<script>") > -1)
						transport.responseText.evalScripts();
					var table = transport.responseText.stripScripts().evalJSON();
					if(table.length != 1) return;
					CurrentController.assignView.down("input[name='id']").value = table[0]["ID"];
					CurrentController.assignView.down("input[name='title']").value = table[0]["Title"];
					CurrentController.assignView.down("input[name='description']").value = table[0]["Description"];
				}
			});
			CurrentController.ajaxCount ++;
			
			new Ajax.Request("server/courses2.php?act=listGroupsAndStudents", {
				parameters: {"id":id},
				onSuccess: function(transport) {
					CurrentController.ajaxCount --;
					if(transport.responseText.indexOf("<script>") > -1)
						transport.responseText.evalScripts();
					var table = transport.responseText.stripScripts().evalJSON();

					var htmlGroups = "";
					var htmlStudents = "";
					for(var i=0;i<table.length;i++) {
						table[i]["CheckAttr"] = (table[i]["Checked"] == 1) ? " checked=\"checked\"" : "";
						if(table[i]["Type"] == "Group") {
							var tmp = "<div class=\"item\">";
							tmp += "<input type=\"hidden\" class=\"type\" value=\"Group\"/>";
							tmp += "<label for=\"courses_assign_groups_#{ID}\" style=\"display:inline\"><input type=\"checkbox\" class=\"check\" id=\"courses_assign_groups_#{ID}\" value=\"#{ID}\"#{CheckAttr}/>#{Name}</label>";
							tmp += "<label for=\"courses_assign_groups_#{ID}_begindate\" style=\"display:inline\">從：<input type=\"text\" id=\"courses_assign_groups_#{ID}_begindate\" class=\"begindate\" name=\"begindate\" style=\"width: 6em\" readonly=\"readonly\" value=\"#{BeginDate}\"/></label>";
							tmp += "<label for=\"courses_assign_groups_#{ID}_enddate\" style=\"display:inline\">到：<input type=\"text\" id=\"courses_assign_groups_#{ID}_enddate\" class=\"enddate\" name=\"enddate\" style=\"width: 6em\" readonly=\"readonly\" value=\"#{EndDate}\"/></label>";
							tmp += "</div>";
							htmlGroups += new Template(tmp).evaluate(table[i]);
						}
						if(table[i]["Type"] == "Student") {
							var tmp = "<div class=\"item\">";
							tmp += "<input type=\"hidden\" class=\"type\" value=\"Student\"/>";
							tmp += "<label for=\"courses_assign_students_#{ID}\" style=\"display:inline\"><input type=\"checkbox\" class=\"check\" id=\"courses_assign_students_#{ID}\" value=\"#{ID}\"#{CheckAttr}/>#{Name}</label>";
							tmp += "<label for=\"courses_assign_students_#{ID}_begindate\" style=\"display:inline\">從：<input type=\"text\" id=\"courses_assign_students_#{ID}_begindate\" class=\"begindate\" name=\"begindate\" style=\"width: 6em\" readonly=\"readonly\" value=\"#{BeginDate}\"/></label>";
							tmp += "<label for=\"courses_assign_students_#{ID}_enddate\" style=\"display:inline\">到：<input type=\"text\" id=\"courses_assign_students_#{ID}_enddate\" class=\"enddate\" name=\"enddate\" style=\"width: 6em\" readonly=\"readonly\" value=\"#{EndDate}\"/></label>";
							tmp += "</div>";
							htmlStudents += new Template(tmp).evaluate(table[i]);
						}
						if(table[i]["Type"] == "All") {
							if(table[i]["Checked"] == 1) {
								CurrentController.assignView.down(".all .check").checked = true;
								CurrentController.assignView.down(".all .begindate").value = table[i]["BeginDate"];
								CurrentController.assignView.down(".all .enddate").value = table[i]["EndDate"];
							}
						}
					}
					CurrentController.switch_OnClick(null, CurrentController.assignView.down(".groups .list").update(htmlGroups).previous("a"), true);
					CurrentController.switch_OnClick(null, CurrentController.assignView.down(".students .list").update(htmlStudents).previous("a"), true);
					CurrentController.assignView.select(".switch").invoke("observe", "click", CurrentController.switch_OnClick);

					var defaultBegindate = (new Date()).strftime("%Y-%m-%d");
					var defaultEnddate = (new Date()).add(5).strftime("%Y-%m-%d");
					CurrentController.assignView.select("input[name='begindate']").each(function(element) {
						Calendar.setup({"dateField": element, "triggerElement": element});
						if(element.value == "") element.value = defaultBegindate;
						element = null;
					});
					CurrentController.assignView.select("input[name='enddate']").each(function(element) {
						Calendar.setup({"dateField": element, "triggerElement": element});
						if(element.value == "") element.value = defaultEnddate;
						element = null;
					});
					CurrentController.assignView.select(".filter").invoke("observe", "blur", CurrentController.filterAssign_OnBlur);
				}
			});
			CurrentController.ajaxCount ++;
			
			(function() {
				if(CurrentController.ajaxCount <= 0)
					new Overlay().show($$("#courses_assign_view").first().show());
					//modalContainer(CurrentController.assignView);
				else
					this.delay(1);
			}).delay(1);
		},
		switch_OnClick: function(event, element, hide) {
			element = element || this;
			var list = element.next(".list");
			if(list.visible() || hide) {
				list.hide();
				element.update("[展開]");
			}
			else {
				list.show();
				element.update("[收合]");
			}
			element = null;
		},
		filterAssign_OnBlur: function(event) {
			var filter = this.value;
			var css;
			if(this.name == "filter_groups") {
				css = ".groups .list div";
			}
			else if(this.name == "filter_students") {
				css = ".students .list div";
			}
			else return;
			CurrentController.assignView.select(css).each(function(element) {
				if(element.innerHTML.stripTags().indexOf(filter) > -1)
					element.show();
				else
					element.hide();
			});
		},
		modify_OnClick: function(event) {
			var id = this.next("input[type='hidden']").value;
			CurrentController.getAddCourseLayout(id);
		},
		remove_OnClick: function(event) {
			if(confirm('確定要刪除課程!?')) {
				new Ajax.Request("server/courses2.php?act=del", {
					parameters: "id=" + this.next().value,
					onSuccess:function(transport) {
						if(transport.responseText.indexOf("<script>") > -1) {
							transport.responseText.evalScripts();
						}
						CurrentController.showMessage("刪除完成...");
						CurrentController.getViewLayout();
					}
				});
			}
		},
		showModifyName: function(element) {
			var text = element.innerHTML;
			element.hide().next("input[type='text']").show().activate().value = text;
			element = null;
		},
		showModifyDesc: function(element) {
			var text = element.innerHTML;
			element.hide().next("input[type='text']").show().activate().value = text;
			element = null;
		},
		modifyName: function(element) {
			var id = element.previous().value;
			var text = element.value;
			new Ajax.Request("server/courses2.php?act=modifyName", {
				parameters:{"id":id, "text":text},
				onComplete: function() {
					CurrentController.getViewLayout();
				}
			});
		},
		modifyDesc: function(element) {
			var id = element.previous().value;
			var text = element.value;
			new Ajax.Request("server/courses2.php?act=modifyDesc", {
				parameters:{"id":id, "text":text},
				onComplete: function() {
					CurrentController.getViewLayout();
				}
			});
		},
		setFilter: function() {
			CurrentController.filter.title = CurrentController.filterView.down("input[name='title']").value;
			CurrentController.filter.description = CurrentController.filterView.down("input[name='description']").value;
			if(CurrentController.filter.title == "" && CurrentController.filter.description == "")
				CurrentController.filterButton.setStyle("color:black");
			else
				CurrentController.filterButton.setStyle("color:blue");				
			CurrentController.getViewLayout();
		},
		clearFilter: function() {
			CurrentController.filterView.down("input[name='title']").value = "";
			CurrentController.filterView.down("input[name='description']").value = "";
			CurrentController.setFilter();
		}
	});
	
	window.CurrentController = new FileController("courses_view", "courses_add_view", "courses_message", "courses_add_button", "courses_filter_view", "courses_filter_button", "courses_assign_view");
</script>