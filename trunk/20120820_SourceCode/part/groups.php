﻿<div class="group-wrapper">
	<p class="title"></p>
	<div id="groups_view">
		<div id="groups_add" class="controller">
			<form id="groups_add_form" method="post" action="server/groups.php?act=add" enctype="application/x-www-form-urlencoded" onsubmit="return false">
				<table><tr><td><label for="groups_add_name">班級(分類)：</label></td><td><input type="text" id="groups_add_name" name="name" class="textfield" /></td><td><input id="groups_add_button" type="button" value="新增" /></td><td><span id="groups_message" class="message"></span></td></tr></table>
			</form>
		</div>
		<div id="groups_grid" class="gridview"></div>
		<div style="display:none">
			<form id="groups_list_form" method="post" action="server/groups.php?act=list" enctype="application/x-www-form-urlencoded" onsubmit="return false"></form>
			<form id="groups_delete_form" method="post" action="server/groups.php?act=delete" enctype="application/x-www-form-urlencoded" onsubmit="return false"><input type="hidden" name="id"/></form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$("groups_list_form").async = function() {
		$("groups_list_form").request({
			onSuccess:function(transport) {
				if(transport.responseText.indexOf("<script>") > -1) {
					transport.responseText.evalScripts();
				}
				var table = transport.responseText.evalJSON();
				var html = "<table>";
				html += "<tr><th>班級(分類)</th><th>建立時間</th><th>建立人員</th><th>動作</th></tr>";
				if(table.length == 0) html += "<tr><td colspan=\"4\">目前還沒建立班級(分類)...</td></tr>";
				for(var i=0;i<table.length;i++) {
					var row = table[i];
					html += "<tr>";
					html += "<td>" + row["Name"] + "</td>";
					html += "<td>" + row["CreateDate"] + "</td>";
					html += "<td>" + row["CreateBy"] + "</td>";
					html += "<td><input type=\"button\" value=\"刪除\" onclick=\"if(confirm('確定要刪除班級(分類)!?')) $$('#groups_delete_form input[name=id]')[0].value='" + row["ID"] + "';$('groups_delete_form').async()\"/></td>";
					html += "</tr>";
				}
				html += "</table>";
				$("groups_grid").update(html);
				$("groups_list_form").reset();
			}
		});
	};

	$("groups_delete_form").async = function() {
		$("groups_delete_form").request({
			onSuccess:function(transport) {
				if(transport.responseText.indexOf("<script>") > -1) {
					transport.responseText.evalScripts();
				}
				$("groups_delete_form").reset();
				$("groups_list_form").async();
			}
		});
	};
	
	$("groups_add_button").observe("click", function() {
		if($("groups_add_name").value.strip() == "") {
			$("groups_add_name").activate();
		}
		else {
			$("groups_add_form").request({
				onSuccess: function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					$("groups_list_form").async();
					$("groups_add_form").reset();
				}
			});
		}
	});

	$("groups_add_form").getInputs().invoke("observe", "keydown", function(event) {
		event = event || window.event;
		if(event.keyCode == Event.KEY_RETURN) {
			$("groups_add_button").click();
		}
	});
	
	loadCss("css/groups.css");
	$("groups_list_form").async();
	$("groups_add_form").focusFirstElement();
</script>