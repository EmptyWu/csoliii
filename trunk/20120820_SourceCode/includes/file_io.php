<?php
	function file_append($filename, $text) {
		if(is_file($filename)) {
			file_put_contents($filename, $text, FILE_APPEND);
		}
		else {
			file_write($filename, $text);
		}
	}

	function file_appendline($filename, $text) {
		file_append($filename, "{$text}\n");
	}
	
	function file_write($filename, $text) {
		file_put_contents($filename, $text);
	}

	function file_readall($filename) {
		$content = file_get_contents($filename);
		return $content;
	}
	
	function file_readline($filename) {
		return preg_split('/\r?\n/', file_readall($filename));
	}
	
	function dir_DeleteTree($dir) {
        if (!file_exists($dir)) return true;
        if (!is_dir($dir)) return unlink($dir);
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') continue;
            if (!dir_DeleteTree($dir.DIRECTORY_SEPARATOR.$item)) return false;
        }
        return rmdir($dir);
    }
	
	function dir_CleanUploadTempFolder($path) {
		if(!is_dir($path)) return;
		$check_hours = 24;
		$upload_session_list = glob("$path*");
		$now = time();
		foreach($upload_session_list as $upload_session) {
			if(is_file($upload_session)) {
				unlink($upload_session);
			}
			else {
				$upload_file_list = glob("$upload_session/*");
				$should_remove = false;
				foreach($upload_file_list as $upload_file) {
					if(is_dir($upload_file)) continue;
					$mtime = filemtime($upload_file);
					$hours_from_now = floor(($now - $mtime)/(60*60));
					if($hours_from_now >= $check_hours) {
						$should_remove = true;
						break;
					}
				}
				if($should_remove) dir_DeleteTree($dir);
			}
		}
	}
?>