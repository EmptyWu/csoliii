<?php
	function utility_StrToHex($string)
	{
	    $hex='';
	    for ($i=0; $i < strlen($string); $i++)
	    {
			if (ord($string[$i])<16)
			$hex .= "0";
	        $hex .= dechex(ord($string[$i]));
	    }
	    return $hex;
	}
	function utility_HexToStr($hex)
	{
	    $string='';
	    for ($i=0; $i < strlen($hex)-1; $i+=2)
	    {
	        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
	    }
	    return $string;
	}
?>