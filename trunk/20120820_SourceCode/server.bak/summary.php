﻿<?php
	if($_SESSION['username'] == "") die("<script>location.href='login.html';</script>");
	$doc_root = $_SERVER['DOCUMENT_ROOT'];
	require_once($doc_root.'/includes/file_io.php');
	require_once($doc_root.'/includes/mysql_tools.php');
	
	switch($_GET['act']) {
		case 'list':
			$sql = '';
			if($_SESSION['usertype'] == 'root') {
				$sql = 'SELECT COUNT(1) AS `Count` FROM Operators';
			}
			else {
				$sql = 'SELECT COUNT(1) AS `Count` FROM Groups
						UNION ALL
						SELECT COUNT(1)	FROM Students
						UNION ALL
						SELECT COUNT(1) FROM Files WHERE IsLeaf = 1
						UNION ALL
						SELECT COUNT(1) FROM Files WHERE IsLeaf = 0';
			}
			$table = mysql_GetArrayRows($sql);
			print(json_encode($table));
			break;
	}
?>