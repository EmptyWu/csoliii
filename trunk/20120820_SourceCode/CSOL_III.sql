-- MySQL dump 10.13  Distrib 5.1.41, for debian-linux-gnu (i486)
--
-- Host: localhost    Database: CSOL_III
-- ------------------------------------------------------
-- Server version	5.1.41-3ubuntu12.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `FileRelations`
--

DROP TABLE IF EXISTS `FileRelations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FileRelations` (
  `Type` tinyint(4) NOT NULL,
  `TypeID` varchar(100) NOT NULL,
  `FileID` varchar(100) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `CreateBy` varchar(100) NOT NULL,
  `BeginDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  PRIMARY KEY (`Type`,`TypeID`,`FileID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FileRelations`
--

LOCK TABLES `FileRelations` WRITE;
/*!40000 ALTER TABLE `FileRelations` DISABLE KEYS */;
INSERT INTO `FileRelations` VALUES (0,'','course.9021288109943271','2010-08-06 10:14:32','newtech','2010-08-06 00:00:00','2010-08-11 00:00:00'),(0,'','course.1416119383648038','2010-08-05 14:08:34','newtech','2010-08-05 00:00:00','2010-08-19 00:00:00');
/*!40000 ALTER TABLE `FileRelations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Files`
--

DROP TABLE IF EXISTS `Files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Files` (
  `ID` varchar(100) NOT NULL,
  `ParentID` varchar(100) NOT NULL,
  `IsLeaf` tinyint(4) NOT NULL,
  `Title` varchar(100) NOT NULL,
  `Description` varchar(100) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `CreateBy` varchar(100) NOT NULL,
  `DueDate` datetime NOT NULL,
  `Size` bigint(20) unsigned NOT NULL,
  `MIME` varchar(100) NOT NULL,
  `Ext` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Files`
--

LOCK TABLES `Files` WRITE;
/*!40000 ALTER TABLE `Files` DISABLE KEYS */;
INSERT INTO `Files` VALUES ('course.9021288109943271','',0,'測試1','測試1','2010-08-04 18:15:22','newtech','2010-08-06 18:15:22',101448249,'',''),('attach.2192677902057767.jpg','course.1416119383648038',1,'attach.7705153040587902','attach.7705153040587902','2010-08-05 14:05:01','newtech','2010-08-07 14:05:01',1026372,'image/jpeg','jpg'),('attach.5638145236298442.pdf','course.1416119383648038',1,'attach.572873658194703','attach.572873658194703','2010-08-05 14:05:01','newtech','2010-08-07 14:05:01',1013868,'application/pdf','pdf'),('attach.44193093478679657.mp4','course.9021288109943271',1,'attach.8296816917136312','attach.8296816917136312','2010-08-04 18:15:22','newtech','2010-08-06 18:15:22',101448249,'video/mp4','mp4'),('course.1416119383648038','',0,'測試2','測試...','2010-08-05 14:05:01','newtech','2010-08-07 14:05:01',2040240,'',''),('attach.4989582346752286.mp3','course.40623111091554165',1,'01. 超人氣 (主演偶像劇《舞動台中》主題曲)','01. 超人氣 (主演偶像劇《舞動台中》主題曲)','2010-08-05 14:10:02','newtech','2010-08-07 14:10:02',5261598,'audio/mp3','mp3'),('course.40623111091554165','',0,'音樂1','音樂1','2010-08-05 14:10:02','newtech','2010-08-07 14:10:02',5261598,'','');
/*!40000 ALTER TABLE `Files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups`
--

DROP TABLE IF EXISTS `Groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups` (
  `ID` varchar(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `CreateBy` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups`
--

LOCK TABLES `Groups` WRITE;
/*!40000 ALTER TABLE `Groups` DISABLE KEYS */;
INSERT INTO `Groups` VALUES ('0c19db3a94d0bc6df62a95b21469fb90','99學測社','2010-08-05 09:59:04','newtech'),('2510d2f682360e2c4ef509b0a99ca679','99學測自','2010-08-05 09:59:11','newtech');
/*!40000 ALTER TABLE `Groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Operators`
--

DROP TABLE IF EXISTS `Operators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Operators` (
  `Acc` varchar(50) NOT NULL,
  `Pwd` varchar(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `LastLogin` datetime NOT NULL,
  `LastIP` varchar(100) NOT NULL,
  PRIMARY KEY (`Acc`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Operators`
--

LOCK TABLES `Operators` WRITE;
/*!40000 ALTER TABLE `Operators` DISABLE KEYS */;
INSERT INTO `Operators` VALUES ('newtech','81dc9bdb52d04dc20036dbd8313ed055','newtech','2010-08-03 13:42:23','2010-08-03 13:42:23','192.168.251.7');
/*!40000 ALTER TABLE `Operators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StudentGroups`
--

DROP TABLE IF EXISTS `StudentGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StudentGroups` (
  `StudentID` varchar(100) NOT NULL,
  `GroupID` varchar(100) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `CreateBy` varchar(100) NOT NULL,
  PRIMARY KEY (`StudentID`,`GroupID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StudentGroups`
--

LOCK TABLES `StudentGroups` WRITE;
/*!40000 ALTER TABLE `StudentGroups` DISABLE KEYS */;
INSERT INTO `StudentGroups` VALUES ('af538b1f6980f2f0f652dbbb8bfb9db144fcc023','2510d2f682360e2c4ef509b0a99ca679','2010-08-05 09:59:21','newtech'),('1f47d837e6eb20b7f0082788bef2d0a707d36a42','0c19db3a94d0bc6df62a95b21469fb90','2010-08-05 10:01:12','newtech'),('aeff0357b0bd5164adb93173dbdcc690','2510d2f682360e2c4ef509b0a99ca679','2010-08-06 10:02:00','newtech'),('f58a0eb9b35166d48545daef0d69dce278d6002e','2510d2f682360e2c4ef509b0a99ca679','2010-08-05 19:00:10','newtech'),('48bc1041d25d0152beadc553a5b854188a86c1d4','2510d2f682360e2c4ef509b0a99ca679','2010-08-05 19:00:04','newtech');
/*!40000 ALTER TABLE `StudentGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Students`
--

DROP TABLE IF EXISTS `Students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Students` (
  `ID` varchar(100) NOT NULL,
  `SN` varchar(100) NOT NULL,
  `School` varchar(20) NOT NULL,
  `Class` varchar(20) NOT NULL,
  `Seat` varchar(10) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Mobile` varchar(20) NOT NULL,
  `Birth` varchar(20) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `ModifyDate` datetime NOT NULL,
  `DueDate` datetime NOT NULL,
  `CreateBy` varchar(100) NOT NULL,
  `ModifyBy` varchar(100) NOT NULL,
  `Removed` tinyint(4) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Students`
--

LOCK TABLES `Students` WRITE;
/*!40000 ALTER TABLE `Students` DISABLE KEYS */;
INSERT INTO `Students` VALUES ('af538b1f6980f2f0f652dbbb8bfb9db144fcc023','1C021KR075J','','','','王小明','','','2010-08-05 09:52:19','2010-08-05 09:59:21','0000-00-00 00:00:00','newtech','newtech',0),('1f47d837e6eb20b7f0082788bef2d0a707d36a42','1A025PES75J','','','','李小強','','','2010-08-05 09:59:59','2010-08-05 10:01:12','0000-00-00 00:00:00','newtech','newtech',0),('aeff0357b0bd5164adb93173dbdcc690','simulator','','','','測試機','','','2010-08-05 13:15:07','2010-08-06 10:02:00','0000-00-00 00:00:00','newtech','newtech',0),('f58a0eb9b35166d48545daef0d69dce278d6002e','1C942BJR75J','','','','許志良','','','2010-08-05 15:39:28','2010-08-05 19:00:10','0000-00-00 00:00:00','newtech','newtech',0),('48bc1041d25d0152beadc553a5b854188a86c1d4','1E944AES6K2','','','','楊淑媖','','','2010-08-05 16:01:42','2010-08-05 19:00:04','0000-00-00 00:00:00','newtech','newtech',0);
/*!40000 ALTER TABLE `Students` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2010-08-06 17:18:11
