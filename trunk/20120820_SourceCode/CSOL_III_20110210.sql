-- MySQL dump 10.13  Distrib 5.1.41, for debian-linux-gnu (i486)
--
-- Host: localhost    Database: CSOL_III
-- ------------------------------------------------------
-- Server version	5.1.41-3ubuntu12.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `FileGroups`
--

DROP TABLE IF EXISTS `FileGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FileGroups` (
  `FileID` varchar(100) NOT NULL,
  `GroupID` varchar(100) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `CreateBy` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FileGroups`
--

LOCK TABLES `FileGroups` WRITE;
/*!40000 ALTER TABLE `FileGroups` DISABLE KEYS */;
INSERT INTO `FileGroups` VALUES ('1a0ab0ee-b672-11df-b01a-080027c36223','5d3ae6a0-b694-11df-b01a-080027c36223','2010-09-02 23:20:49','newtech'),('204eb36a-b672-11df-b01a-080027c36223','5d3ae6a0-b694-11df-b01a-080027c36223','2010-09-02 23:20:49','newtech'),('27e601d2-b672-11df-b01a-080027c36223','5d3ae6a0-b694-11df-b01a-080027c36223','2010-09-02 23:20:49','newtech'),('33d235ec-b66d-11df-b01a-080027c36223','413aa1e2-b69f-11df-b01a-080027c36223','2010-09-02 22:40:25','newtech'),('1a0ab0ee-b672-11df-b01a-080027c36223','0d8c0d82-b69e-11df-b01a-080027c36223','2010-09-03 14:35:44','newtech'),('204eb36a-b672-11df-b01a-080027c36223','0d8c0d82-b69e-11df-b01a-080027c36223','2010-09-03 14:35:44','newtech'),('27e601d2-b672-11df-b01a-080027c36223','0d8c0d82-b69e-11df-b01a-080027c36223','2010-09-03 14:35:44','newtech'),('391a4bfc-b66d-11df-b01a-080027c36223','413aa1e2-b69f-11df-b01a-080027c36223','2010-09-02 22:40:25','newtech'),('3044c610-b66d-11df-b01a-080027c36223','5d3ae6a0-b694-11df-b01a-080027c36223','2010-09-02 23:20:49','newtech'),('31ce14a0-b66d-11df-b01a-080027c36223','5d3ae6a0-b694-11df-b01a-080027c36223','2010-09-02 23:20:49','newtech'),('33d235ec-b66d-11df-b01a-080027c36223','5d3ae6a0-b694-11df-b01a-080027c36223','2010-09-02 23:20:49','newtech'),('1705762c-b672-11df-b01a-080027c36223','413aa1e2-b69f-11df-b01a-080027c36223','2010-09-02 22:40:25','newtech'),('204eb36a-b672-11df-b01a-080027c36223','413aa1e2-b69f-11df-b01a-080027c36223','2010-09-02 22:40:25','newtech'),('27e601d2-b672-11df-b01a-080027c36223','413aa1e2-b69f-11df-b01a-080027c36223','2010-09-02 22:40:25','newtech'),('1705762c-b672-11df-b01a-080027c36223','53c7dde8-b69f-11df-b01a-080027c36223','2010-09-04 11:25:06','newtech'),('1d1e14ce-b672-11df-b01a-080027c36223','53c7dde8-b69f-11df-b01a-080027c36223','2010-09-04 11:25:06','newtech'),('253db182-b672-11df-b01a-080027c36223','53c7dde8-b69f-11df-b01a-080027c36223','2010-09-04 11:25:06','newtech'),('27e601d2-b672-11df-b01a-080027c36223','53c7dde8-b69f-11df-b01a-080027c36223','2010-09-04 11:25:06','newtech');
/*!40000 ALTER TABLE `FileGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FileRelations`
--

DROP TABLE IF EXISTS `FileRelations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FileRelations` (
  `Type` varchar(100) NOT NULL,
  `TypeID` varchar(100) NOT NULL,
  `FileID` varchar(100) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `CreateBy` varchar(100) NOT NULL,
  `BeginDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  PRIMARY KEY (`Type`,`TypeID`,`FileID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FileRelations`
--

LOCK TABLES `FileRelations` WRITE;
/*!40000 ALTER TABLE `FileRelations` DISABLE KEYS */;
INSERT INTO `FileRelations` VALUES ('All','all','53c7dde8-b69f-11df-b01a-080027c36223','2011-01-10 12:30:49','newtech','2010-09-06 00:00:00','2011-02-01 00:00:00'),('Group','2510d2f682360e2c4ef509b0a99ca679','53c7dde8-b69f-11df-b01a-080027c36223','2011-01-10 12:30:49','newtech','2010-09-06 00:00:00','2010-09-15 00:00:00'),('All','all','413aa1e2-b69f-11df-b01a-080027c36223','2011-01-11 10:02:57','newtech','2010-09-04 00:00:00','2011-02-01 00:00:00'),('Group','b38fd3d1fb731216d62ab8e37aef30e0','53c7dde8-b69f-11df-b01a-080027c36223','2011-01-10 12:30:49','newtech','2010-09-06 00:00:00','2010-09-11 00:00:00');
/*!40000 ALTER TABLE `FileRelations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Files`
--

DROP TABLE IF EXISTS `Files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Files` (
  `ID` varchar(100) NOT NULL,
  `ParentID` varchar(100) NOT NULL,
  `IsLeaf` tinyint(4) NOT NULL,
  `Title` varchar(100) NOT NULL,
  `Description` varchar(100) NOT NULL,
  `Path` varchar(100) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `CreateBy` varchar(100) NOT NULL,
  `DueDate` datetime NOT NULL,
  `Size` bigint(20) unsigned NOT NULL,
  `MIME` varchar(100) NOT NULL,
  `Ext` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Files`
--

LOCK TABLES `Files` WRITE;
/*!40000 ALTER TABLE `Files` DISABLE KEYS */;
INSERT INTO `Files` VALUES ('2c475b4a-b66d-11df-b01a-080027c36223','',1,'10. 愛的詩篇','梁文音 - 愛的詩篇','f0e0932f9c5e2d6003b96702468a881d0.mp3','2010-09-02 16:36:25','newtech','2010-09-07 16:36:25',5410142,'audio/mp3','mp3'),('2dad16fa-b66d-11df-b01a-080027c36223','',1,'09. Dry Your Eyes','梁文音 - 愛的詩篇','fe83f7f389f39aeb1092c4d5e4aea347e.mp3','2010-09-02 16:36:27','newtech','2010-09-07 16:36:27',7510965,'audio/mp3','mp3'),('2edc264c-b66d-11df-b01a-080027c36223','',1,'08. 第一眼','梁文音 - 愛的詩篇','f2f90b118ba45e815d956c7db87873b4e.mp3','2010-09-02 16:36:29','newtech','2010-09-07 16:36:29',4008278,'audio/mp3','mp3'),('3044c610-b66d-11df-b01a-080027c36223','',1,'07. 最親愛的 (OT I Love You Anyway)','梁文音 - 愛的詩篇','ffa21f58426ea1d2877c903bab29f4af0.mp3','2010-09-02 16:36:32','newtech','2010-09-07 16:36:32',5015197,'audio/mp3','mp3'),('31ce14a0-b66d-11df-b01a-080027c36223','',1,'06. 我不是你想像那麼勇敢','梁文音 - 愛的詩篇','fc555c262182ed37e7a88a2fdecd1a022.mp3','2010-09-02 16:36:34','newtech','2010-09-07 16:36:34',5293492,'audio/mp3','mp3'),('33d235ec-b66d-11df-b01a-080027c36223','',1,'05. 可以不愛了','梁文音 - 愛的詩篇','f8c849848b65c7a6416dec8bfc4881cbb.mp3','2010-09-02 16:36:38','newtech','2010-09-07 16:36:38',7419232,'audio/mp3','mp3'),('3569f1b0-b66d-11df-b01a-080027c36223','',1,'04. 弦外之音','梁文音 - 愛的詩篇','fdce7d295418c9955b6d8fdac8463d608.mp3','2010-09-02 16:36:40','newtech','2010-09-07 16:36:40',5241273,'audio/mp3','mp3'),('375298b0-b66d-11df-b01a-080027c36223','',1,'03. 雪雨','梁文音 - 愛的詩篇','f0789cd9fcda0e37caa8ac31528110be9.mp3','2010-09-02 16:36:44','newtech','2010-09-07 16:36:44',6206598,'audio/mp3','mp3'),('391a4bfc-b66d-11df-b01a-080027c36223','',1,'02. 薄荷與指甲剪','梁文音 - 愛的詩篇','fde52e1ebe0deab99416dba03e7a37f5b.mp3','2010-09-02 16:36:47','newtech','2010-09-07 16:36:47',4714166,'audio/mp3','mp3'),('40eb0204-b66d-11df-b01a-080027c36223','',1,'01. 最幸褔的事','梁文音 - 愛的詩篇','fa922b7f8e64897ebcc55de2212e51ad1.mp3','2010-09-02 16:37:00','newtech','2010-09-07 16:37:00',5789539,'audio/mp3','mp3'),('154dfb06-b672-11df-b01a-080027c36223','',1,'10.感冒','郭靜 - 下一個天亮','fce8f24092a3b61860e74965e71e26334.mp3','2010-09-02 17:11:34','newtech','2010-09-07 17:11:34',4900819,'audio/mp3','mp3'),('1705762c-b672-11df-b01a-080027c36223','',1,'09.界線','郭靜 - 下一個天亮','f95f658f5b7df54d142a2b104520ff492.mp3','2010-09-02 17:11:37','newtech','2010-09-07 17:11:37',6000317,'audio/mp3','mp3'),('1883a852-b672-11df-b01a-080027c36223','',1,'08.知道','郭靜 - 下一個天亮','fae9d823b6f402383b41847d395271823.mp3','2010-09-02 17:11:39','newtech','2010-09-07 17:11:39',5555898,'audio/mp3','mp3'),('1a0ab0ee-b672-11df-b01a-080027c36223','',1,'07.回音','郭靜 - 下一個天亮','f45a19826e30939c2e04284c4b3c0a8c0.mp3','2010-09-02 17:11:42','newtech','2010-09-07 17:11:42',4981332,'audio/mp3','mp3'),('1ba3f122-b672-11df-b01a-080027c36223','',1,'06.不藥而癒','郭靜 - 下一個天亮','fa4591774aec4f6d3441267b57e1ed350.mp3','2010-09-02 17:11:45','newtech','2010-09-07 17:11:45',5799416,'audio/mp3','mp3'),('1d1e14ce-b672-11df-b01a-080027c36223','',1,'05.慢慢紀念','郭靜 - 下一個天亮','f2962d9ca4f8047e225a75d70e8f15c9e.mp3','2010-09-02 17:11:47','newtech','2010-09-07 17:11:47',5094582,'audio/mp3','mp3'),('204eb36a-b672-11df-b01a-080027c36223','',1,'04.愛情訊息','郭靜 - 下一個天亮','fb913d012832767cf14d89f17c6ad6bee.mp3','2010-09-02 17:11:52','newtech','2010-09-07 17:11:52',6324004,'audio/mp3','mp3'),('2352324e-b672-11df-b01a-080027c36223','',1,'03.大玩笑','郭靜 - 下一個天亮','fead072b597f4e4039d051bd5aec1af20.mp3','2010-09-02 17:11:57','newtech','2010-09-07 17:11:57',4440724,'audio/mp3','mp3'),('253db182-b672-11df-b01a-080027c36223','',1,'02.百分百','郭靜 - 下一個天亮','ff3d474ba9a4371016220aff4fa71d3fc.mp3','2010-09-02 17:12:01','newtech','2010-09-07 17:12:01',6081843,'audio/mp3','mp3'),('27e601d2-b672-11df-b01a-080027c36223','',1,'01.下一個天亮','郭靜 - 下一個天亮','f60570572d17a9f3125aba5818e9c0070.mp3','2010-09-02 17:12:05','newtech','2010-09-07 17:12:05',5851861,'audio/mp3','mp3'),('413aa1e2-b69f-11df-b01a-080027c36223','',0,'測試空2','123','','2010-09-02 22:34:55','newtech','2010-09-07 22:34:55',30309580,'',''),('0d8c0d82-b69e-11df-b01a-080027c36223','',0,'測試空','zzzz','','2010-09-02 22:26:19','newtech','2010-09-07 22:26:19',17157197,'',''),('5d3ae6a0-b694-11df-b01a-080027c36223','',0,'測試3','555666','','2010-09-02 21:16:57','newtech','2010-09-07 21:16:57',34885118,'',''),('53c7dde8-b69f-11df-b01a-080027c36223','',0,'測試空1','555','','2010-09-02 22:35:26','newtech','2010-09-07 22:35:26',23028603,'',''),('76067c46-b967-11df-a126-080027c36223','',1,'01','01.mp3','fc08ccff3da4a9becbc818f7d04202b4e.mp3','2010-09-06 11:33:05','newtech','2010-09-11 11:33:05',2731673,'audio/mp3','mp3'),('78938b8e-b967-11df-a126-080027c36223','',1,'02','02.mp3','ff3d0f6c09eacfbc2dc5c9ef2670b2b2a.mp3','2010-09-06 11:33:10','newtech','2010-09-11 11:33:10',8765957,'audio/mp3','mp3'),('7b1b1bec-b967-11df-a126-080027c36223','',1,'03','03.mp3','f23ac8a4f1372df2f7956ed32afdebc53.mp3','2010-09-06 11:33:14','newtech','2010-09-11 11:33:14',11337457,'audio/mp3','mp3'),('7ccb96c4-b967-11df-a126-080027c36223','',1,'歌-41234','這是歌04.mp3','f80b86e72553e50971ae2a834fd486825.mp3','2010-09-06 11:33:17','newtech','2010-09-11 11:33:17',9512026,'audio/mp3','mp3');
/*!40000 ALTER TABLE `Files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups`
--

DROP TABLE IF EXISTS `Groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups` (
  `ID` varchar(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `CreateBy` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups`
--

LOCK TABLES `Groups` WRITE;
/*!40000 ALTER TABLE `Groups` DISABLE KEYS */;
INSERT INTO `Groups` VALUES ('0c19db3a94d0bc6df62a95b21469fb90','99學測社','2010-08-05 09:59:04','newtech'),('2510d2f682360e2c4ef509b0a99ca679','99學測自','2010-08-05 09:59:11','newtech'),('ede8bdda595023586ba04a7bc3daf3ee','測試1','2010-09-01 19:02:31','newtech'),('1419c29fb442f9e62294901e026e63b3','測試2','2010-09-01 19:02:33','newtech'),('dba8121320ce56d9e4b4b837ce08583b','測試3','2010-09-01 19:02:34','newtech'),('bb5c0f8f4a9d9658a50f598c10ab0465','測試4','2010-09-01 19:02:36','newtech'),('12d4c7be0a1d2dd9a14e5461502cac2b','測試5','2010-09-01 19:02:38','newtech'),('5d45c7460ff92daeb6f183595e287b1e','測試6','2010-09-01 19:02:40','newtech'),('14dfca10ff012c05666b0ac01b049ae6','測試7','2010-09-01 19:02:53','newtech'),('ffcb63d595dc6a7d7bce1faec66d7b23','測試8','2010-09-01 19:02:55','newtech'),('08626adf4423ba800e26906fd67eb116','測試9','2010-09-01 19:03:09','newtech'),('31cbabe7976bca17b515330297957af8','測試10','2010-09-01 19:03:11','newtech'),('3099cb859040108ce1217ed1ed83d377','測試11','2010-09-01 19:03:13','newtech'),('8154c57ac715f07948706941ea00c084','測試12','2010-09-01 19:03:15','newtech'),('c2a6b4c790ba91aaee2b196db1100e32','測試13','2010-09-01 19:03:23','newtech'),('f0c6391ddf49025c9a2c24baebf189ec','測試14','2010-09-01 19:03:24','newtech'),('2c99989231d8cff71cde704f3d95e28c','測試15','2010-09-01 19:03:26','newtech'),('498bfd8d0aad4c07e20fda0fcb6960aa','測試16','2010-09-01 19:03:28','newtech'),('475252c3661d4e3ca46be1fa6e325f3b','測試17','2010-09-01 19:03:30','newtech'),('c673c93a19cf561bd9293e1bcd04cede','測試18','2010-09-01 19:03:41','newtech'),('74286513af875227c91bfdbc59ec6679','測試19','2010-09-01 19:03:43','newtech'),('7f328b33ed93a5de719d6328967fc4ee','測試20','2010-09-01 19:03:44','newtech'),('b38fd3d1fb731216d62ab8e37aef30e0','測試21','2010-09-01 19:03:46','newtech');
/*!40000 ALTER TABLE `Groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Operators`
--

DROP TABLE IF EXISTS `Operators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Operators` (
  `Acc` varchar(50) NOT NULL,
  `Pwd` varchar(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `LastLogin` datetime NOT NULL,
  `LastIP` varchar(100) NOT NULL,
  PRIMARY KEY (`Acc`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Operators`
--

LOCK TABLES `Operators` WRITE;
/*!40000 ALTER TABLE `Operators` DISABLE KEYS */;
INSERT INTO `Operators` VALUES ('newtech','81dc9bdb52d04dc20036dbd8313ed055','newtech','2010-08-03 13:42:23','2010-08-03 13:42:23','192.168.251.7');
/*!40000 ALTER TABLE `Operators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StudentGroups`
--

DROP TABLE IF EXISTS `StudentGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StudentGroups` (
  `StudentID` varchar(100) NOT NULL,
  `GroupID` varchar(100) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `CreateBy` varchar(100) NOT NULL,
  PRIMARY KEY (`StudentID`,`GroupID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StudentGroups`
--

LOCK TABLES `StudentGroups` WRITE;
/*!40000 ALTER TABLE `StudentGroups` DISABLE KEYS */;
INSERT INTO `StudentGroups` VALUES ('af538b1f6980f2f0f652dbbb8bfb9db144fcc023','2510d2f682360e2c4ef509b0a99ca679','2010-08-05 09:59:21','newtech'),('1f47d837e6eb20b7f0082788bef2d0a707d36a42','0c19db3a94d0bc6df62a95b21469fb90','2010-08-05 10:01:12','newtech'),('aeff0357b0bd5164adb93173dbdcc690','2510d2f682360e2c4ef509b0a99ca679','2010-08-06 10:02:00','newtech'),('f58a0eb9b35166d48545daef0d69dce278d6002e','2510d2f682360e2c4ef509b0a99ca679','2010-08-05 19:00:10','newtech'),('48bc1041d25d0152beadc553a5b854188a86c1d4','2510d2f682360e2c4ef509b0a99ca679','2010-08-05 19:00:04','newtech'),('00000000000010008000080027f3cb50','2510d2f682360e2c4ef509b0a99ca679','2010-08-25 14:14:52','newtech');
/*!40000 ALTER TABLE `StudentGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Students`
--

DROP TABLE IF EXISTS `Students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Students` (
  `ID` varchar(100) NOT NULL,
  `SN` varchar(100) NOT NULL,
  `School` varchar(20) NOT NULL,
  `Class` varchar(20) NOT NULL,
  `Seat` varchar(10) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Mobile` varchar(20) NOT NULL,
  `Birth` varchar(20) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `ModifyDate` datetime NOT NULL,
  `DueDate` datetime NOT NULL,
  `CreateBy` varchar(100) NOT NULL,
  `ModifyBy` varchar(100) NOT NULL,
  `Removed` tinyint(4) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Students`
--

LOCK TABLES `Students` WRITE;
/*!40000 ALTER TABLE `Students` DISABLE KEYS */;
INSERT INTO `Students` VALUES ('af538b1f6980f2f0f652dbbb8bfb9db144fcc023','1C021KR075J','','','','王小明','','','2010-08-05 09:52:19','2010-08-05 09:59:21','0000-00-00 00:00:00','newtech','newtech',0),('1f47d837e6eb20b7f0082788bef2d0a707d36a42','1A025PES75J','','','','李小強','','','2010-08-05 09:59:59','2010-08-05 10:01:12','0000-00-00 00:00:00','newtech','newtech',0),('aeff0357b0bd5164adb93173dbdcc690','simulator','','','','測試機','','','2010-08-05 13:15:07','2010-08-06 10:02:00','0000-00-00 00:00:00','newtech','newtech',0),('f58a0eb9b35166d48545daef0d69dce278d6002e','1C942BJR75J','','','','許志良','','','2010-08-05 15:39:28','2010-08-05 19:00:10','0000-00-00 00:00:00','newtech','newtech',0),('48bc1041d25d0152beadc553a5b854188a86c1d4','1E944AES6K2','','','','楊淑媖','','','2010-08-05 16:01:42','2010-08-05 19:00:04','0000-00-00 00:00:00','newtech','newtech',0),('00000000000010008000080027f3cb50','simulator2','','','','test','','','2010-08-25 14:14:36','2010-08-25 14:14:52','0000-00-00 00:00:00','newtech','newtech',0);
/*!40000 ALTER TABLE `Students` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-02-10 16:52:41
