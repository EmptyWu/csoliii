<?php
    $proxies = array(
        '192.168.252.252'
    );

    $mask = ip2long('255.255.255.0');

    $remote_ip = ip2long($_SERVER['REMOTE_ADDR']);
	if(isset($_GET['IP'])) {
		$remote_ip = ip2long($_GET['IP']);
	}
    $masked_ip = $remote_ip & $mask;
    $chosen_proxy = 'DIRECT';
    foreach($proxies as $proxy) {
        $masked_proxy = ip2long($proxy) & $mask;
        if($masked_proxy == $masked_ip) {
            //$chosen_proxy = "DIRECT; PROXY $proxy:3128";
            $chosen_proxy = "PROXY $proxy:3128";
        }
    }
    
    $result = "function FindProxyForURL(url, host)
{
	return '$chosen_proxy';
}";
    echo $result;
?>
