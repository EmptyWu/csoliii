﻿<?php
/*print_r(apc_cache_info());
print("\r\n");
print_r(apc_clear_cache());
print("\r\n");*/


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="學習帶著走" name="istudy" name="新技網路科技" http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="StyleSheet" type="text/css" href="css/n_istudy/n.css" />
		<script type="text/javascript" src="js/prototype.js"></script>
		<script type="text/javascript" src="js/scriptaculous.js"></script>

		<script type="text/javascript" src="js/jscal2.js"></script>
		<script type="text/javascript" src="js/lang/cn.js"></script>
		<script type="text/javascript" src="js/date.commented.js"></script>
		<script type="text/javascript" src="js/overlay.min.js"></script>
		<script type="text/javascript" src="js/tablekit.js"></script>
		
		
<style type="text/css">
</style>
<title>學習帶著走_學習知識管理系統</title>

</head>
<body>
<input id="files_add_button" type="button" value="新增檔案"/>
<div class="subforms">
		<div class="subform" id="files_add_view" style="display:none">
		<div class="title" style="padding: .2em 1em; background-color:#33a;color:#fff;font-size:18px;border-bottom:solid 1px #333">新增檔案</div>
		<div class="controller" style="padding: 1em"></div>
		<div class="toolbar">
			<img alt="完成" src="../images/n_istudy/tbt08_Finish.png" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.getViewLayout()"  />
			<!--<input type="button" value="完成" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.getViewLayout()"/>-->
		</div>
	</div>
</div>
<div class="upload_template" style="display:none">
	<div class="upload">
		<div class="view view_ie">
			<div class="action control">
				<form action="upload/uploadFile.php" method="post" enctype="multipart/form-data" name="upload" target="#{id}">
					<input type="hidden" name="APC_UPLOAD_PROGRESS" value="" />
					<table><tr><td><input type="file" name="file" style="width:55px;height:20px"/></td><td><span class="filename"></span></td><td><input type="submit" value="上傳" class="submit" style="height:20px"/></td></tr></table>
				</form>
				<iframe id="#{id}" name="#{id}"></iframe>
			</div>
			<div class="message" style="display:none"><span class="progressbar"><span class="progress"></span></span><span class="filename"></span>&nbsp;<span class="status">上傳中，請稍候...</span></div>
		</div>
		<div class="view view_else">
			<div class="action"><a href="#" class="button browse">瀏覽檔案...</a></div>
			<div class="message" style="display:none"><span class="progressbar"><span class="progress"></span></span><span class="filename"></span>&nbsp;<span class="status">上傳中，請稍候...</span></div>
			<div class="control">
				<form action="upload/uploadFile.php" method="post" enctype="multipart/form-data" name="upload" target="#{id}">
					<input type="hidden" name="APC_UPLOAD_PROGRESS" value="" /><input type="file" name="file"/><input type="submit" value="上傳"/>
				</form>
				<iframe id="#{id}" name="#{id}"></iframe>
			</div>
	<div class="share_format" align="left"><img src="../images/n_istudy/share_Format.png" /></div>
		</div>
	</div>
</div>
<script type="text/javascript">
function guid() {
	function S4() {
		return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
	}

	return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}
var FileController = Class.create({
initialize:function(addView,addButton){
this.addButton = $(addButton);
this.addView = $(addView);
this.addButton.observe("click", this.add_OnClick);
}
,add_OnClick: function() {
			CurrentController.getAddFileLayout();
		},
		getAddFileLayout: function() {	
		
			this.appendAddFileControl();
			if(!this.addView.visible()) new Overlay().show(this.addView.show());
		},appendAddFileControl: function() {
			if(!$$("#files_add_view .upload .filename").any(function(element) {return element.innerHTML == ""})) {
				var id = guid();
				var html = $$(".upload_template").first().innerHTML.interpolate({"id": id});
				var control = new Element("div").update(html);

				var fail_count = 0;
				var total_count = 0;
				function getProgress() {
					new Ajax.Request("upload/getprogress2.php?id=" + id, {
						onSuccess: function(transport) {
							try {
								total_count ++;
								var result = transport.responseText;
								if(parseInt(result, 10))
									control.down(".progress").setStyle("width:" + result + "%");
								else
									fail_count ++;
								if(control.down(".status").innerHTML.indexOf("完成") == -1 && fail_count < 10 && total_count < 50) getProgress.delay(0.5);
							} catch(e) { 
								if(typeof console == "object") console.log(e) 
							}
						},
						onFailure:function(r){
						alert(r);
						}
					});
				}

				if(Prototype.Browser.IE) {
					control.select(".view").without(control.down(".view_ie")).invoke("remove");
					control.down("input[type='file']").observe("change", (function(event) {
						if(Event.element(event).value == "") {
							control.select(".view .filename").invoke("update");
						}
						else {
							control.select(".view .filename").invoke("update", Event.element(event).value.split(/\\/g).last());
							control.down(".submit").show();
							control.down("form").observe("submit", (function(event) {
								Event.element(event).up(".upload").down(".action").hide();
								Event.element(event).up(".upload").down(".message").show();
								getProgress.delay(0.5);
								this.appendAddFileControl();
							}).bind(this));
						}
					}).bind(this));
					control.down(".control").toggleClassName("control");
				}
				else {
					control.select(".view").without(control.down(".view_else")).invoke("remove");
					control.down(".browse").observe("click", function(event) { Event.element(event).up(".upload").down("input[type='file']").click() });
					control.down("input[type='file']").observe("change", (function(event) {
						if(Event.element(event).value == "") {
							control.down(".view .share_format").show();
							control.down(".view .action").show();
							control.down(".view .filename").update();
							control.down(".view .message").hide();
						}
						else {
							control.down(".view .action").hide();
							control.down(".view .filename").update(Event.element(event).value.split(/\\/g).last());
							control.down(".view .message").show();
							control.down("form").submit();
							control.down(".view .share_format").hide();
							getProgress.delay(0.5);
							this.appendAddFileControl();
						}
					}).bind(this));
				}
				
				control.down("iframe").observe("load", function(event) {
					try {
						var content = Event.element(event).contentWindow.document.body.innerHTML;
						if(content.indexOf("!!success!!") > -1) {
							control.down(".status").update("上傳完成!!");
							control.down(".progress").setStyle("width:100%");
						}
						else if(content.indexOf("!!badtype!!") > -1) {
							control.down(".status").update("格式不正確!!");
							control.down(".progress").setStyle("width:100%");
						}
						else if(content.indexOf("!!failed!!") > -1) {
							control.down(".status").update("上傳失敗，請重新上傳!!");
							control.down(".progress").setStyle("width:100%");
						}
						this.appendAddFileControl();
					} catch(e) { 
						if(typeof console == "object") console.log(e) 
					}
				}.bind(this));

				control.down("form").writeAttribute("action", control.down("form").readAttribute("action") + "?id=" + id);
				control.down("input[name='APC_UPLOAD_PROGRESS']").writeAttribute("value", id);
				this.addView.down(".controller").insert(control);
				this.addView.select(".button").invoke("observe", "click", function(event) { Event.stop(event) });
			}
			
			
		}
});
window.CurrentController = new FileController("files_add_view",  "files_add_button");
</script>
</body>
</html>