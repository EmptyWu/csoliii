<?php
	if($_SESSION['username'] == "") die("<script>opener.location.href='login.html';self.focus();self.close()</script>");
	require_once(dirname(__FILE__).'/../includes/file_io.php');
	require_once(dirname(__FILE__).'/../includes/mysql_tools.php');
	require_once(dirname(__FILE__).'/../includes/ipod_algorithm.php');

	if($_FILES['StudentGroups']['error'] == UPLOAD_ERR_OK && $_FILES['Groups']['error'] == UPLOAD_ERR_OK  && $_FILES['StudentGroups']['error'] == UPLOAD_ERR_OK ){
		$files = array('Students' => $_FILES['Students']['tmp_name'],
			'Groups' => $_FILES['Groups']['tmp_name'],
			'StudentGroups' => $_FILES['StudentGroups']['tmp_name']);
		$sqls = array();
		foreach($files as $table => $filename) {
			array_push($sqls, "DELETE FROM $table WHERE CreateBy = 'System'");
			array_push($sqls, "LOAD DATA LOCAL INFILE '$filename' INTO TABLE $table
					CHARACTER SET UTF8
					FIELDS TERMINATED BY ',' ENCLOSED BY '\"'
					LINES TERMINATED BY '\r\n'
					IGNORE 1 LINES");
		}
		mysql_ExecTransaction($sqls);
		
		$sql = "SELECT ID FROM Students";
		$records = mysql_GetArrayRows($sql);
		$updates = array();
		foreach($records as $record) {
			$ID = $record[0];
			$SID = ipod_getDecimalSum($ID);
			array_push($updates, "UPDATE Students SET SID = '$SID' WHERE ID = '$ID'");
		}
		mysql_ExecTransaction($updates);
		echo '<script>alert("匯入資料完成...");parent.$("import_form").reset()</script>';
	}
	else {
		echo '<script>alert("上傳失敗，請稍候再試...")</script>';
	}
?>