﻿var methods = {
	login:function() {
		if($("username").value.strip() == "") {
			$("message").update("請輸入帳號...");
			$("username").activate();
			return;
		}
		if($("password_ui").value.strip() == "") {
			$("message").update("請輸入密碼...");
			$("password_ui").activate();
			return;			
		}
		
		$$(".message").first().update("登入中，請稍候...");
		$("password").value = hex_md5(hex_md5($("password_ui").value) + $("seed").value);
		$$(".login form").first().request({
			onSuccess:function(transport) {
				if(transport.responseText.indexOf("OK") > -1) {
					$$(".message").first().update("登入成功...");
					(function() {location.href = "/";}).delay(2);
				}
				else {
					$$(".message").first().update("帳號或者是密碼不對喔...");
				}
			}
		});
	},
	seed:function() {
		new Ajax.Request("server/auth.php?act=seed", {
			onSuccess:function(transport) {
				$("seed").value = transport.responseText.replace(/[^\w]+/, "");
			}
		});
	}
};
Event.observe(window,"load",function(){
	Object.extend($$(".login").first(),methods);
	$$(".login").first().seed();
	$$(".login form").first().focusFirstElement();
});