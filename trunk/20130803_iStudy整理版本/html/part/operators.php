﻿<div id="operators_view" class="operators_view">
<div id="operators" class="title"></div>
	<div id="operators_add" class="controller">
		<form id="operators_add_form" method="post" action="server/operators.php?act=add" enctype="application/x-www-form-urlencoded" onsubmit="return false">
			<table>
				<tr>
					<td><label for="operators_add_acc">帳號</label></td><td><input type="text" id="operators_add_acc" name="acc" class="textfield" /></td>
					<td style="padding-left:2em"><label for="operators_add_name">姓名</label></td><td><input type="text" id="operators_add_name" name="name" class="textfield" /></td>
					<td rowspan="2">
						<img alt="新增" src="../images/n_istudy/tbt01_New.png" id="operators_add_button" />
						<!--<input type="button" value="新增" style="margin:auto .2em;height:4em" id="operators_add_button"/>-->
					</td><td rowspan="2"><span id="operators_message" class="message"></span></td>
				</tr>
				<tr>
					<td><label for="operators_add_password">密碼</label></td><td><input type="password" id="operators_add_password" name="password" class="textfield" /></td>
					<td style="padding-left:2em"><label for="operators_add_password_retype">再一次密碼</label></td><td><input type="password" id="operators_add_password_retype" name="password_retype" class="textfield" /></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="operators_grid" class="gridview"></div>
	<div style="display:none">
		<form id="operators_list_form" method="post" action="server/operators.php?act=list" enctype="application/x-www-form-urlencoded" onsubmit="return false"></form>
		<form id="operators_delete_form" method="post" action="server/operators.php?act=delete" enctype="application/x-www-form-urlencoded" onsubmit="return false"><input type="hidden" name="acc"/></form>
	</div>
</div>
<div class="subforms">
	<div class="subform" id="students_modify" style="display:none">
		<div class="title">修改教師資料</div>
		<div class="controller"></div>
		<div class="toolbar">
		<!--<input type="button" value="確定" onclick="$('students').doModify(this)"/><input type="button" value="取消" onclick="Overlay.hide(this.up('.subform').hide())"/>-->
		<img alt="確定" src="../images/n_istudy/tbt11_Determine.png" onclick="$('operators').doModify(this)"/>
		<img alt="取消" src="../images/n_istudy/tbt09_Cancel.png" onclick="Overlay.hide(this.up('.subform').hide())"/>
		</div>
	</div>
</div>
<script type="text/javascript">
	var  methods = {
		message:function(msg,delay,duration) {
			$("operators_message").update(msg).show().highlight();
			if(delay) {
				(function() {$("operators_message").fade({"duration":duration||0.5});}).delay(delay);
			}
		},
		modify:function(id) {
			new Ajax.Request("../server/operators.php?act=list_by_acc", {
				parameters:"acc="+id,
				onSuccess:function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					//var dataset = transport.responseText.evalJSON();
					var table = transport.responseText.evalJSON();
					var group_html = "";
					var html = "<form method=\"post\" action=\"../server/operators.php?act=modify\" enctype=\"application/x-www-form-urlencoded\" onsubmit=\"return false\">";
					html += "<input type=\"hidden\" name=\"acc\" value=\"#{Acc}\"/><input type=\"hidden\" name=\"full_name\" value=\"#{Name}\"/>";
					html += "<table>";
					html += "<tr><th style='width:80px'>姓名</th><td style='width:270px'><input type=\"text\" name=\"new_name\" id=\"new_name\" value=\"#{Name}\" /></td><th style='width:100px'>帳號(不可修改)</th><td style='width:300px'>#{Acc}&nbsp;</td></tr>";
					html += "<tr><th>密碼</th><td>********</td><th>建立時間</th><td>#{CreateDate}&nbsp;</td></tr>";
					html += "<tr><th>重設密碼</th><td><input type=\"password\" name=\"new_pwd\" id=\"new_pwd\" value=\"\" />*請輸入6-8英文數字組合</td><th>最後登入時間</th><td>#{LastLogin}&nbsp;</td></tr>";
					html += "<tr><th>再輸入一次</th><td><input type=\"password\" name=\"new_pwd_again\" id=\"new_pwd_again\" value=\"\" />*請輸入6-8英文數字組合</td><th>登入IP</th><td>#{LastLogin}&nbsp;</td>";
					html += "</tr>";
					html += "</table></form>";
					$$("#students_modify .controller")[0].update(new Template(html).evaluate(table[0]));
					new Overlay().show($$("#students_modify").first().show());				
				}
			});
		},
		doModify:function(holder) {
			var new_pwd = document.getElementById("new_pwd").value
			var new_pwd_again = document.getElementById("new_pwd_again").value
			var regex = /^[\d|a-zA-Z]+$/;
				
			if (new_pwd.length > 0) {
				if (new_pwd.length < 6 || new_pwd.length > 8) {
					alert("新密碼必須6-8英文數字組合");
					return false;						
				}
				if (new_pwd_again.length < 6 || new_pwd_again.length > 8) {
					alert("再輸入密碼必須6-8英文數字組合");
					return false;						
				}
				if (!regex.test(new_pwd)) {
					alert("密碼重設必須為文字與數字組合");
					return false;
				}
 				if (new_pwd != new_pwd_again) {
					alert("新密碼與再輸入密碼必須一致");
					return false;
				}
			}
				
			$$("#students_modify form")[0].request({
				onSuccess:(function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					$("operators").list();
					Overlay.hide(this.up('.subform').hide());
					$("operators").message("修改完成...", 2);
				}).bind(holder)
			});	
		},
		list:function(callback,showmsg) {
			$("operators_list_form").request({
				onSuccess:function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					var table = transport.responseText.evalJSON();
					var html = "<table>";
					html += "<tr><th>帳號</th><th>姓名</th><th>建立時間</th><th>最後登入</th><th>IP</th><th>動作</th></tr>";
					if(table.length == 0) html += "<tr><td colspan=\"6\">目前還沒建立操作人員資料...</td></tr>";
					for(var i=0;i<table.length;i++) {
						var row = table[i];
						html += "<tr>";
						html += "<td>" + row["Acc"] + "</td>";
						html += "<td>" + row["Name"] + "</td>";
						html += "<td>" + row["CreateDate"] + "</td>";
						html += "<td>" + row["LastLogin"] + "</td>";
						html += "<td>" + row["LastIP"] + "</td>";
						html += "<td><img alt=\"修改\" src=\"../images/n_istudy/tbt04_Modify.png\" onclick=\"$('operators').modify('" + row["Acc"] + "');\" />&nbsp;&nbsp;&nbsp;&nbsp;<img alt=\"刪除\" src=\"../images/n_istudy/tbt02_Del.png\" onclick=\"if(confirm('確定要刪除操作者!?')) $$('#operators_delete_form input[name=acc]')[0].value='" + row["Acc"] + "';$('operators_delete_form').async()\" /></td>";
						html += "</tr>";
					}
					html += "</table>";
					$("operators_grid").update(html);
					$("operators_list_form").reset();
				}
			});
		}
	};
	Object.extend($("operators"), methods);

	$("operators_list_form").async = function() {
		$("operators_list_form").request({
			onSuccess:function(transport) {
				if(transport.responseText.indexOf("<script>") > -1) {
					transport.responseText.evalScripts();
				}
				var table = transport.responseText.evalJSON();
				var html = "<table>";
				html += "<tr><th>帳號</th><th>姓名</th><th>建立時間</th><th>最後登入</th><th>IP</th><th>動作</th></tr>";
				if(table.length == 0) html += "<tr><td colspan=\"6\">目前還沒建立操作人員資料...</td></tr>";
				for(var i=0;i<table.length;i++) {
					var row = table[i];
					html += "<tr>";
					html += "<td>" + row["Acc"] + "</td>";
					html += "<td>" + row["Name"] + "</td>";
					html += "<td>" + row["CreateDate"] + "</td>";
					html += "<td>" + row["LastLogin"] + "</td>";
					html += "<td>" + row["LastIP"] + "</td>";
					//html += "<td><input type=\"button\" value=\"刪除\" onclick=\"if(confirm('確定要刪除操作者!?')) $$('#operators_delete_form input[name=acc]')[0].value='" + row["Acc"] + "';$('operators_delete_form').async()\"/></td>";
					html += "<td><img alt=\"修改\" src=\"../images/n_istudy/tbt04_Modify.png\" onclick=\"$('operators').modify('" + row["Acc"] + "');\" />&nbsp;&nbsp;&nbsp;&nbsp;<img alt=\"刪除\" src=\"../images/n_istudy/tbt02_Del.png\" onclick=\"if(confirm('確定要刪除操作者!?')) $$('#operators_delete_form input[name=acc]')[0].value='" + row["Acc"] + "';$('operators_delete_form').async()\" /></td>";
					html += "</tr>";
				}
				html += "</table>";
				$("operators_grid").update(html);
				$("operators_list_form").reset();
			}
		});
	};
	
	$("operators_delete_form").async = function() {
		$("operators_delete_form").request({
			onSuccess:function(transport) {
				if(transport.responseText.indexOf("<script>") > -1) {
					transport.responseText.evalScripts();
				}
				$("operators_delete_form").reset();
				$("operators_list_form").async();
			}
		});
	};
	
	$("operators_add_button").observe("click", function() {
		var valid = true;
		$("operators_add_form").getInputs().each(function(element) {
			if(element.value.strip() == "") {
				element.activate();
				$("operators_message").update("請輸入" + $$("label[for='" + element.id + "']")[0].innerHTML).show();
				(function() {$("operators_message").fade({duration:0.5});}).delay(3);
				valid = false;
				throw $break;
			}
			if(element.id == "operators_add_password") {
				if($("operators_add_password").value != $("operators_add_password_retype").value) {
					$("operators_add_password").value = "";
					$("operators_add_password_retype").value = "";
					element.activate();
					$("operators_message").update("兩次密碼不一樣，請再輸入一次...").show();
					(function() {$("operators_message").fade({duration:0.5});}).delay(3);
					valid = false;
					throw $break;
				}
			}
		});
		if(!valid) {
			return;
		}
		$("operators_add_form").request({
			onSuccess: function(transport) {
				if(transport.responseText.indexOf("<script>") > -1) {
					transport.responseText.evalScripts();
				}
				if(transport.responseText.indexOf("dup_acc") > -1) {
					$("operators_message").update("帳號重覆了...").show();
					(function() {$("operators_message").fade({duration:0.5});}).delay(3);
				}
				else if(transport.responseText.indexOf("dup_acc") > -1) {
					$("operators_message").update("兩次密碼不一樣，請再輸入一次...").show();
					(function() {$("operators_message").fade({duration:0.5});}).delay(3);
				}
				else {
					$("operators_list_form").async();
					$("operators_add_form").reset();
				}
			}
		});
	});

	$("operators_add_form").getInputs().invoke("observe", "keydown", function(event) {
		event = event || window.event;
		if(event.keyCode == Event.KEY_RETURN) {
			$("operators_add_button").click();
		}
	});

	loadCss("css/operators.css");
	$("operators_list_form").async();
	$("operators_add_form").focusFirstElement();
</script>