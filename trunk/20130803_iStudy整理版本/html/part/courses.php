﻿<p class="title" id="courses">課程總覽<a href="VideoConverter.zip" style="font-size:12px;color:#aa3333">▼下載影片轉檔工具</a></p>
<div class="toolbar controller"><input id="start_courses_add" type="button" value="新增課程" /></div>
<div id="courses_view" class="gridview">目前無課程</div>
<div id="courses_add" style="display:none">
	<div class="title">新增課程</div>
	<div id="courses_add_container" class="controller">
		<form id="courses_add_form" action="server/courses.php?act=add" method="post" 
			enctype="application/x-www-form-urlencoded" onsubmit="return false">
			<input type="hidden" id="courses_add_session" name="session" value="" />
			<table>
				<tr>
					<td><label for="courses_add_name">課程名稱：</label></td>
					<td><input class="longtextfield" type="text" name="name" id="courses_add_name" /></td>
				</tr>
				<tr>
					<td><label for="courses_add_description">課程描述：</label></td>
					<td><input class="longtextfield" type="text" name="description" id="courses_add_description"/></td>
				</tr>
			</table>
			<div id="attach_fields"></div>
		</form>
	</div>
	<div class="toolbar"><div id="courses_add_message" class="message"></div>
		<ul><li><input type="button" id="courses_add_submit" value="新增" /></li>
			<li><input type="button" value="取消" onclick="removeModalContainer()" /></li></ul></div>
	<div id="attach_forms"></div>
</div>
<div id="courses_assign" style="display:none">
	<div class="title">指派檔案</div>
	<div class="controller">
		<form method="post" action="server/courses.php?act=assign" enctype="application/x-www-form-urlencoded" onsubmit="return false">
			<input type="hidden" name="act" value="assign"/><input type="hidden" name="id" id="courses_assign_id" />
			<table style="width: auto">
				<tr><td>從：</td><td><input type="text" id="courses_assign_begindate" name="begindate" style="width: 6em" readonly="readonly"/></td><td>到：</td><td><input type="text" id="courses_assign_enddate" name="enddate" style="width: 6em" readonly="readonly"/></td></tr>
				<tr><td colspan="4"><hr/></td></tr>
				<tr><td>所有人</td><td><label><input type="checkbox" name="assign_all" value="1" id="courses_assign_assign_all" />指派給所有人</label></td></tr>
				<tr><td colspan="4"><hr/></td></tr>
				<tr><td style="vertical-align:top">分類</td><td id="courses_assign_groups"></td></tr>
			</table>
		</form>
	</div>
	<div class="toolbar"><input type="button" value="確定" onclick="$('courses').doAssign()"/>
		<input type="button" value="取消" onclick="removeModalContainer()"/></div>
</div>
<script type="text/javascript">
	loadCss("css/courses.css");
	var methods = {
		list:function() {
			function getSize(size) {
				size = parseInt(size, 10);
				if(size < 1024) return size + "B";
				var k = size / 1024;
				if(k < 1024) return (Math.round(k*100)/100) + "KB";
				var m = k / 1024;
				return (Math.round(m*100)/100) + "MB";
			}
			new Ajax.Request("server/courses.php?act=list", {
				onSuccess:function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					var table = transport.responseText.evalJSON();
					var html = "<table>";
					html += "<tr><th>名稱</th><th>描述</th><th>建立時間</th><th>檔案大小</th><th>動作</th></tr>";
					if(table.length == 0) html += "<tr><td colspan=\"5\">目前還沒建立課程...</td></tr>";
					for(var i=0;i<table.length;i++) {
						var row = table[i];
						html += (row['IsLeaf'] == "0") ? "<tr>" : "<tr class=\"Leaf\">";
						html += "<td>" + row['Title'] + "</td>";
						html += "<td>" + row['Description'] + "</td>";
						html += "<td class=\"date\">" + row['CreateDate'] + "</td>";
						html += "<td class=\"numeric\">" + getSize(row['Size']) + "</td>";
						if(row['IsLeaf'] == "0") {
							html += "<td style=\"white-space:nowrap\"><input type=\"button\" value=\"指派\" onclick=\"$('courses').assign('" + row['ID'] + "')\"/>";
							html += "<input type=\"button\" value=\"刪除課程\" onclick=\"if(confirm('確定要刪除課程!?')) $('courses').del('" + row['ID'] + "')\"/></td>";
						}
						else {
							html += "<td><input type=\"button\" value=\"移除檔案\" onclick=\"$('courses').del('" + row['ID'] + "')\"/></td>";
						}
						html += "</tr>";
					}
					html += "</table>";
					$("courses_view").update(html);
				}
			});
		},
		assign:function(id) {
			$$("#courses_assign form")[0].reset();
			Calendar.setup({"dateField": "courses_assign_begindate", "triggerElement": "courses_assign_begindate"});
			Calendar.setup({"dateField": "courses_assign_enddate", "triggerElement": "courses_assign_enddate"});
			new Ajax.Request("server/courses.php?act=list_group_relation", {
				parameters:"id="+id,
				onSuccess:function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
						transport.responseText.stripScripts();
					}
					var dataset = transport.responseText.evalJSON();
					var table = dataset[0];
					
					var fileid = "";
					var begindate = (new Date()).strftime("%Y-%#m-%#d");
					var enddate = (new Date()).add(5).strftime("%Y-%#m-%#d");
					
					if(table.length > 0) {
						$("courses_assign_assign_all").writeAttribute("checked", "checked");
						fileid = table[0]["FileID"];
						begindate = table[0]["BeginDate"] || begindate;
						enddate = table[0]["EndDate"] || enddate;
					}
					
					table = dataset[1];
					var group_format = "<label><input type=\"checkbox\" name=\"groups[]\" value=\"#{ID}\" #{Assigned}/>#{Name}</label>";
					var html = "";
					for(var i=0;i<table.length;i++) {
						html += new Template(group_format).evaluate(table[i]);
						fileid = table[i]["FileID"];
						begindate = table[0]["BeginDate"] || begindate;
						enddate = table[0]["EndDate"] || enddate;
					}
					
					$("courses_assign_id").value = fileid;
					$("courses_assign_begindate").value = begindate.split(" ")[0];
					$("courses_assign_enddate").value = enddate.split(" ")[0];
					$("courses_assign_groups").update(html);
					modalContainer("courses_assign");
				}
			});
		},
		doAssign:function() {
			$$("#courses_assign form")[0].request({
				onSuccess:function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					removeModalContainer();
				}
			});
		},
		del:function(id) {
			$("courses").doDel(id);
		},
		doDel:function(id) {
			new Ajax.Request("server/courses.php?act=del", {
				parameters:"id=" + id,
				onSuccess:function(transport) {
					$("courses").list();
				}
			});
		},
		getDatePeriod:function(days) {
			var today = new Date();
			var nextday = new Date();
			nextday.setDate(today.getDate() + days);
			
			var date_template = new Template("#{0}-#{1}-#{2}");
			
			return date_template.evaluate([today.getFullYear(), (today.getMonth()+1).toPaddedString(2), today.getDate().toPaddedString(2)]) + ' -> ' 
				+ date_template.evaluate([nextday.getFullYear(), (nextday.getMonth()+1).toPaddedString(2), nextday.getDate().toPaddedString(2)]);
		}
	};
	Object.extend($("courses"),methods);
	
	$("courses").list();

	$("start_courses_add").observe("click", function() {
		var session = "course." + Math.random().toString().substring(2);
		function createAttachField(id) {
			var attach_field_html_format = 
				"<div id=\"field_#{id}\" style=\"border:solid 1px #999;padding:5px;margin:5px\"><div id=\"file_area_#{id}\">附加檔案："
				+ "<input id=\"courses_add_file_#{id}\" name=\"file[]\" type=\"hidden\" value=\"#{id}\" />"
				+ "<input id=\"courses_add_file_name_#{id}\" name=\"file_name[]\" type=\"hidden\" value=\"\" />"
				+ "<input id=\"courses_add_fake_browse_#{id}\" class=\"fake_browse\" type=\"button\" value=\"瀏覽...\"/></div>"
				+ "<div>描述：<input class=\"longtextfield\" type=\"text\" "
				+ "id=\"courses_add_description_#{id}\" name=\"file_description[]\" /></div></div>";
			return new Template(attach_field_html_format).evaluate({"id":id});
		}
		function createAttachForm(id) {
			var attach_form_html_format = "<div id=\"form_#{id}\">"
				+ "<form onsubmit=\"\" action=\"upload/upload.php\" method=\"post\" "
				+ "enctype=\"multipart/form-data\" name=\"upload\" target=\"courses_add_iframe_#{id}\">"
				+ "<input type=\"hidden\" name=\"session\" value=\"#{session}\" />"
				+ "<input type=\"hidden\" name=\"APC_UPLOAD_PROGRESS\" value=\"#{id}\" />"
				+ "<input type=\"file\" id=\"courses_add_upload_file_#{id}\" class=\"real_file\" name=\"file\" /></form>"
				+ "<iframe style=\"display: none\" id=\"courses_add_iframe_#{id}\" name=\"courses_add_iframe_#{id}\"></iframe></div>";
			return new Template(attach_form_html_format).evaluate({"id":id,"session":session});
		}
		function createProgressBar(id) {
			var progressbar_html_format = "<div id=\"progressbar_outer_#{id}\" class=\"progressbar_outer\">"
				+ "<div id=\"progressbar_inner_#{id}\" class=\"progressbar_inner\"></div></div>";
			return new Template(progressbar_html_format).evaluate({"id":id});
		}
		function createFileNameMessage(id) {
			if($("courses_add_iframe_" + id).contentWindow.document.body.innerHTML.indexOf("!!failed!!") > -1) {
				(function(id) {
					$('courses_add').descendants().each(function(element){
						if(element.id && element.id.indexOf(id)>-1)element.remove();});}).delay(1,id);
				return new Template("<div id=\"filename_message_#{id}\" style=\"display:inline\">上傳失敗，請再試一下。</div>")
						.evaluate({"id":id});
			}
			else {
				var filename_html_format = "<div id=\"filename_message_#{id}\" style=\"display:inline\">" 
						+ "<a href=\"#\" onclick=\"$('courses_add').descendants()"
						+ ".each(function(element){"
						+ "if(element.id && element.id.indexOf('#{id}')>-1)element.remove();});return false\">移除</a>"
						+ "&nbsp;&nbsp;&nbsp;&nbsp;#{file}</div>";
				var file = $("courses_add_upload_file_" + id).value.split("\\").last();
				file = getFilenameWithoutExt(file);
				return new Template(filename_html_format).evaluate({"id":id,"file":file});
			}
		}
		
		function appendUploadItem() {
			var id = "attach." + Math.random().toString().substring(2);
			$("attach_fields").insert(createAttachField(id));
			$("attach_forms").insert(createAttachForm(id));
			
			var file_onchange = function(event) {
				var element = Event.element(event);
				var id = element.id.split("_").last();
				element.up("form").submit();
				element.hide();
				$("file_area_" + id).insert(createProgressBar(id));
				$("progressbar_outer_" + id).makePositioned();
				getUploadProgress.delay(1,id);
				$("courses_add_fake_browse_" + id).hide();
				appendUploadItem();
			};
			new PeriodicalExecuter(function(pe) {
				try {
					if(!$("courses_add_upload_file_" + id).visible()) {
						pe.stop();return;
					}
					$("courses_add_upload_file_" + id).clonePosition($("courses_add_fake_browse_" + id));
				}
				catch(e){pe.stop();}
			}, 0.5);
			$("courses_add_upload_file_" + id).observe("change", file_onchange);
		}
		
		function getUploadProgress(id) {
			new Ajax.Request("upload/getprogress.php?uid=" + id, {
				method:"get",
				onSuccess:function(transport) {
					$("progressbar_inner_" + id).setStyle({"width":transport.responseText+"%"});
					if(transport.responseText < 100) {
						getUploadProgress.delay(0.5,id);
					}
					else {
						try {
							var upload_response = $("courses_add_iframe_" + id).contentWindow.document.body.innerHTML;
							if(upload_response.indexOf('!!badtype!!') > -1) alert("您上傳的檔案格式不正確...");
							if(upload_response.indexOf('!!failed!!') > -1) alert("上傳失敗，請再試一次...");
							if(upload_response.indexOf('!!badtype!!') > -1 || upload_response.indexOf('!!failed!!') > -1) {
								$('courses_add').descendants().each(function(element) {
										if(element.id && element.id.indexOf(id)>-1) element.remove();
									}
								);
								return;
							}
							var uploaded = upload_response.evalJSON();
							$("progressbar_outer_" + id).hide();
							$("file_area_" + id).insert(createFileNameMessage(id));
							var filename = uploaded.name;
							$("courses_add_file_name_" + id).value = filename;
							filename = getFilenameWithoutExt(filename);
							if($("courses_add_description_" + id).value == "") {
								$("courses_add_description_" + id).value = filename;
							}
						}catch(e){getUploadProgress.delay(0.5,id);}
					}
				}
			});
		}
		
		function getFilenameWithoutExt(filename) {
			var parts = filename.split(".");
			if(parts.length > 1) {
				parts.length -= 1;
				return parts.join(".");
			}
			else {
				return filename;
			}
		}
		
		$("courses_add_form").reset();
		$("attach_fields").update();
		$("attach_forms").update();
		modalContainer('courses_add');
		$("courses_add_session").value = session;
		appendUploadItem();
	});
	
	$("courses_add_submit").observe("click", function() {
		var valid = true;
		var uploaded_count = $$("div[id^='filename_message']").length;
		$("courses_add_form").getInputs("text").each(function(input) {
			if(input.value.strip() == "") {
				if(input.name == "file_description[]") {
					var hidden_file_uploaded_id = input.id.replace("courses_add_description_", "filename_message_");
					if(!$(hidden_file_uploaded_id)) {
						return;
					}
				}
				valid = false;
				throw $break;
			}
		});
		if(!valid || uploaded_count == 0) {
		$("courses_add_message").update("請確定輸入所有欄位或已上傳至少一個檔案...").show();
			(function() {$("courses_add_message").fade({duration:0.5});}).delay(3);
			return;
		}
		
		function requestForm() {
			var uploading_count = 0;
			$$(".progressbar_outer").each(function(element) {if(element.visible()) uploading_count++;});
			if(uploading_count > 0) {
				$("courses_add_message").update("等待附件上傳中...").show();
				requestForm.delay(1);
				return;
			}
			$("courses_add_message").update("正在新增課程...").show();
			$$("#courses_add input[type='button']").invoke("setAttribute", "disabled", "disabled");
			$("courses_add_form").request({
				onSuccess: function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					if(transport.responseText.indexOf("!!success!!") > -1) {
						$("courses_add_message").update("新增課程成功!!").show();
						(function() {
							$("courses_add_message").update().hide();removeModalContainer();
							loadPart("main","part/courses.php");}).delay(2);
					}
					else {
						$("courses_add_message").update("新增課程失敗，請再試一次!!").show();
						(function() {
							$("courses_add_message").update().hide();removeModalContainer();
							loadPart("main","part/courses.php");}).delay(2);
					}
				}
			});
		}
		requestForm();
	});
</script>