﻿<div class="group-wrapper">
	<div id="groups" class="title"></div>
	<div id="groups_view">
		<div id="groups_add" class="controller">
			<form id="groups_add_form" method="post" action="server/groups.php?act=add" enctype="application/x-www-form-urlencoded" onsubmit="return false">
				<table><tr><td><label for="groups_add_name">班級(分類)：</label></td><td><input type="text" id="groups_add_name" name="name" class="textfield" /></td>
					<td>
						<!--<input id="groups_add_button" type="button" value="新增" />-->
						<img id="groups_add_button" alt="新增" src="../images/n_istudy/tbt01_New.png" />
					</td><td><span id="groups_message" class="message"></span></td></tr></table>
			</form>
		</div>
		<div id="groups_grid" class="gridview"></div>
		<div style="display:none">
			<form id="groups_list_form" method="post" action="server/groups.php?act=list" enctype="application/x-www-form-urlencoded" onsubmit="return false"></form>
			<form id="groups_delete_form" method="post" action="server/groups.php?act=delete" enctype="application/x-www-form-urlencoded" onsubmit="return false"><input type="hidden" name="id"/></form>
		</div>
	</div>
</div>
<div class="subforms">
	<div class="subform" id="students_modify" style="display:none">
		<div class="title">修改班級資料</div>
		<div class="controller"></div>
		<div class="toolbar">
		<!--<input type="button" value="確定" onclick="$('students').doModify(this)"/><input type="button" value="取消" onclick="Overlay.hide(this.up('.subform').hide())"/>-->
		<img alt="確定" src="../images/n_istudy/tbt11_Determine.png" onclick="$('groups').doModify(this)"/>
		<img alt="取消" src="../images/n_istudy/tbt09_Cancel.png" onclick="Overlay.hide(this.up('.subform').hide())"/>
		</div>
	</div>
</div>
<script type="text/javascript">
	var  methods = {
		message:function(msg,delay,duration) {
			$("groups_message").update(msg).show().highlight();
			if(delay) {
				(function() {$("groups_message").fade({"duration":duration||0.5});}).delay(delay);
			}
		},
		doModify:function(holder) {
			$$("#students_modify form")[0].request({
				onSuccess:(function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					$("groups").list();
					Overlay.hide(this.up('.subform').hide());
					$("groups").message("修改完成...", 2);
				}).bind(holder)
			});
		},
		checkShareAll: function (shareObj) {
			//var shareAll = CurrentController.assignView.down("input[id='courses_share_all']");
			
			if (shareObj.checked) {
				$$('.share_check').each(function(item) {
					item.checked = true;
				});									
			}
			else {
				$$('.share_check').each(function(item) {
					item.checked = false;
				});					
			}
		},

		modify:function(id) {
			new Ajax.Request("../server/groups.php?act=list_by_id", {
				parameters:"id="+id,
				onSuccess:function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					//var dataset = transport.responseText.evalJSON();
					var dataset = transport.responseText.evalJSON();
					var table = dataset[0];
					var group_html = "";
					var count = 1;
					var sname = "";
					var total = 0;
					
					dataset[1].each(function(row) {
						sname += new Template("<td><label style=\"text-align:left\" value=\"#{name}\"><input class=\"share_check\" type=\"checkbox\" name=\"students[]\" value=\"#{id}\" #{Checked} />#{name}</label></td>").evaluate(row);
						
						if (count % 5 == 0) {
							group_html += "<tr>" + sname + "</tr>"
							sname = "";
							count = 0;
						}
							
						count = count + 1;
						//group_html += new Template("<label style=\"text-align:left\" value=\"#{name}\"><input type=\"checkbox\" name=\"students[]\" value=\"#{id}\" #{Checked} />#{name}</label>").evaluate(row);
					});
					
					if (sname != "") {
						group_html += "<tr>" + sname + "</tr>"						
					}
					
					var html = "<form method=\"post\" action=\"../server/groups.php?act=modify\" enctype=\"application/x-www-form-urlencoded\" onsubmit=\"return false\">";
					html += "<input type=\"hidden\" name=\"id\" value=\"#{ID}\"/>";
					html += "<table>";
					html += "<tr><th style='width:80px'>班級(分類)</th><td style='width:270px'>#{Name}</td><th style='width:120px'>修改班級(分類)名稱</th><td style='width:280px'><input type=\"text\" name=\"class_name\" id=\"class_name\" value=\"\" /></td></tr>";
					html += "<tr><th style='width:80px'>班級成員</th><td colspan=\"3\">"
					if (group_html == "") {
						html += "無班級成員資料";
					}
					else {	
						html += "<div class=\"all\"><div class=\"item\">";
						html += "<label for=\"groups_assign_all\" style=\"display:inline\"><input type=\"checkbox\" onclick=\"$('groups').checkShareAll(this)\" class=\"check\" id=\"groups_share_all\" value=\"all\"/>加入所有學生</label>";
						html += "</div></div><hr/>";
						html += "<div>";
						html += "<div>加入學生</div>";
						//html += "<div>篩選：<input type=\"text\" class=\"filter\" name=\"filter_groups\"/><input type=\"button\" value=\"▼\"/></div>";
						//html += "<div class=\"operators\" style=\"border:dashed 1px #93f\"><a href=\"#\" class=\"switch\" onclick=\"return false\">[展開]</a><div class=\"list\" style=\"display:none\"></div></div>";
						html += "<table>" + group_html + "</table>";
						html += "</div>";
					}
					html += "</td></tr>";
					html += "</table></form>";
					$$("#students_modify .controller")[0].update(new Template(html).evaluate(table[0]));
					
					count = 0; 
					$$('.share_check').each(function(item) {
						if (item.checked) {
							count = count + 1
						}
					});	

					if (count >= dataset[1].length) {
						$("groups_share_all").checked = true;
					}								

					new Overlay().show($$("#students_modify").first().show());				
				}
			})
		},		
		switch_OnClick: function(event, element, hide) {
			element = element || this;
			var list = element.next(".list");
			if(list.visible() || hide) {
				list.hide();
				element.update("[展開]");
			}
			else {
				list.show();
				element.update("[收合]");
			}
			element = null;
		},
		list:function(callback,showmsg) {
			$("groups_list_form").request({
				onSuccess:function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					var table = transport.responseText.evalJSON();
					var html = "<table>";
					html += "<tr><th>班級(分類)</th><th>建立時間</th><th>建立人員</th><th>動作</th></tr>";
					if(table.length == 0) html += "<tr><td colspan=\"4\">目前還沒建立班級(分類)...</td></tr>";
					for(var i=0;i<table.length;i++) {
						var row = table[i];
						html += "<tr>";
						html += "<td>" + row["Name"] + "</td>";
						html += "<td>" + row["CreateDate"] + "</td>";
						html += "<td>" + row["CreateBy"] + "</td>";
						//html += "<td><input type=\"button\" value=\"刪除\" onclick=\"if(confirm('確定要刪除班級(分類)!?')) $$('#groups_delete_form input[name=id]')[0].value='" + row["ID"] + "';$('groups_delete_form').async()\"/></td>";
						html += "<td><img alt=\"修改\" src=\"../images/n_istudy/tbt04_Modify.png\" onclick=\"$('groups').modify('" + row["ID"] + "');\" />&nbsp;&nbsp;&nbsp;&nbsp;<img alt=\"刪除\" src=\"../images/n_istudy/tbt02_Del.png\" onclick=\"if(confirm('確定要刪除班級(分類)!?')) $$('#groups_delete_form input[name=id]')[0].value='" + row["ID"] + "';$('groups_delete_form').async()\" /></td>";
						html += "</tr>";
					}
					html += "</table>";
					
					$("groups_grid").update(html);
					$("groups_list_form").reset();
				}
			});
		}
		
	};
	Object.extend($("groups"), methods);
	
	$("groups_list_form").async = function() {
		$("groups_list_form").request({
			onSuccess:function(transport) {
				if(transport.responseText.indexOf("<script>") > -1) {
					transport.responseText.evalScripts();
				}
				var table = transport.responseText.evalJSON();
				var html = "<table>";
				html += "<tr><th>班級(分類)</th><th>建立時間</th><th>建立人員</th><th>動作</th></tr>";
				if(table.length == 0) html += "<tr><td colspan=\"4\">目前還沒建立班級(分類)...</td></tr>";
				for(var i=0;i<table.length;i++) {
					var row = table[i];
					html += "<tr>";
					html += "<td>" + row["Name"] + "</td>";
					html += "<td>" + row["CreateDate"] + "</td>";
					html += "<td>" + row["CreateBy"] + "</td>";
					//html += "<td><input type=\"button\" value=\"刪除\" onclick=\"if(confirm('確定要刪除班級(分類)!?')) $$('#groups_delete_form input[name=id]')[0].value='" + row["ID"] + "';$('groups_delete_form').async()\"/></td>";
					html += "<td><img alt=\"修改\" src=\"../images/n_istudy/tbt04_Modify.png\" onclick=\"$('groups').modify('" + row["ID"] + "');\" />&nbsp;&nbsp;&nbsp;&nbsp;<img alt=\"刪除\" src=\"../images/n_istudy/tbt02_Del.png\" onclick=\"if(confirm('確定要刪除班級(分類)!?')) $$('#groups_delete_form input[name=id]')[0].value='" + row["ID"] + "';$('groups_delete_form').async()\" /></td>";
					html += "</tr>";
				}
				html += "</table>";
				$("groups_grid").update(html);
				$("groups_list_form").reset();
			}
		});
	};
	

	$("groups_delete_form").async = function() {
		$("groups_delete_form").request({
			onSuccess:function(transport) {
				if(transport.responseText.indexOf("<script>") > -1) {
					transport.responseText.evalScripts();
				}
				$("groups_delete_form").reset();
				$("groups_list_form").async();
			}
		});
	};
	
	$("groups_add_button").observe("click", function() {
		if($("groups_add_name").value.strip() == "") {
			$("groups_add_name").activate();
		}
		else {
			$("groups_add_form").request({
				onSuccess: function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					$("groups_list_form").async();
					$("groups_add_form").reset();
				}
			});
		}
	});

	$("groups_add_form").getInputs().invoke("observe", "keydown", function(event) {
		event = event || window.event;
		if(event.keyCode == Event.KEY_RETURN) {
			$("groups_add_button").click();
		}
	});
	
	loadCss("css/groups.css");
	$("groups_list_form").async();
	$("groups_add_form").focusFirstElement();
</script>