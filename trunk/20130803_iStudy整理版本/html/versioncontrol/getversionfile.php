<?php
$doc_root = $_SERVER['DOCUMENT_ROOT'];
require_once($doc_root . '/includes/simple_html_dom.php');
require_once($doc_root . '/includes/file_io.php');
require_once($doc_root . '/includes/pclzip.lib.php');

$filename=$doc_root.'/includes/version.xml';
if (file_exists($filename)) {
    $xml = simplexml_load_file($filename);
	
    //echo $xml->Account . ":". $xml ->Password;
	}
$version=(string)$xml->version;
$guid=(string)$xml->guid;
$url=(string)$xml->serverurl;
$data=array('guid' => $guid, 'version' => $version);
$data_url = http_build_query($data);
$data_len = strlen ($data_url);
     
$request = array(
    'http' => array (
        'method' => 'POST',
        'content' => $data_url,
        'header' => "Content-type: application/x-www-form-urlencoded\r\n" .
        "Content-Length: " . $data_len . "\r\n"
    ) 
);
$context = stream_context_create($request);
$urllink=(string)$url.'conn.php';
$html = file_get_html($urllink, false, $context);
echo $html .'<BR>';
$j=json_decode($html);
echo count($j).'<br>';
echo $j[0]->version.'<br>';
$filename=(string)$j[0]->filepath;
//檔案路徑
$savepath='file_storage/'.$filename;
echo $url.'file_storage/'.$filename.'<br>';
echo $j[0]->mime.'<br>';
file_put_contents($savepath, file_get_contents($url.'file_storage/'.$filename));

// PclZip
$archive = new PclZip($savepath);
$destination_path_zip='..';

 /*壓縮黨裡面的目錄結構
  * if (($list = $archive->listContent()) == 0) {
    die("Error : ".$archive->errorInfo(true));
    }

    for ($i=0; $i<sizeof($list); $i++) {
        for(reset($list[$i]); $key = key($list[$i]); next($list[$i])) {
            echo "File $i / [$key] = ".$list[$i][$key]."<br>";
        }
        echo "<br>";
    }
  * */
  
// 解壓縮檔案 PCLZIP_OPT_REPLACE_NEWER = 強制複寫目前的檔案 PCLZIP_OPT_PATH=設定路徑
 $archive->extract(PCLZIP_OPT_PATH, $destination_path_zip,PCLZIP_OPT_REPLACE_NEWER);

?>