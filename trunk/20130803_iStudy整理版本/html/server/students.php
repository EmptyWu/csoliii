﻿<?php
	if($_SESSION['username'] == "") die("<script>location.href='login.html';</script>");
	$doc_root = $_SERVER['DOCUMENT_ROOT'];
	require_once($doc_root.'/includes/file_io.php');
	require_once($doc_root.'/includes/mysql_tools.php');
	require_once($doc_root.'/includes/ipod_algorithm.php');
	
	$_SESSION['selected'] = "students";
	
	switch($_POST['act']) {
		case 'add':
			$conn = mysql_GetConnection();
			$sn = mysql_real_escape_string($_POST['sn'], $conn);
			$name = mysql_real_escape_string($_POST['name'], $conn);
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$now = date('c');
			$username = $_SESSION['username'];

			$arr = mysql_GetArrayRows("SELECT COUNT(1) FROM Students WHERE SN = '$sn' AND Name = '$name'");
			if($arr[0][0] > 0) {
				die("dup_sno_name");
			}
			
			$arr = mysql_GetArrayRows("SELECT COUNT(1) FROM Students WHERE ID = '$id'");
			if($arr[0][0] > 0) {
				die('dup_id');
			}
			
			$sql = "INSERT INTO Students VALUES('$id', '$sn', '', '', '', '$name', '', '', '$now', '$now', '0000-00-00 00:00:00', '$username', '$username', 0)";
			mysql_Exec($sql);
			
			$sql = "SELECT s.ID, SN, Name, ModifyDate, ModifyBy, Removed
					FROM Students s
					ORDER BY ModifyDate DESC, CreateDate DESC, SN DESC limit 50";
			$table = mysql_GetArrayRows($sql);
			
//$log = new Logging();
//$log->lfile('mylog.txt'); 
//$log->lwrite(json_encode($table));			
//$log->lclose();	
								
			print(json_encode($table));
			break;
			
		case 'delete':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sql = "DELETE FROM Students WHERE ID = '$id'";
			mysql_Exec($sql);
			break;

		case 'list':
			$conn = mysql_GetConnection();
			$sn = mysql_real_escape_string($_POST['sn'], $conn);
			$name = mysql_real_escape_string($_POST['name'], $conn);
			$username = $_SESSION['username'];
			$sql = "SELECT s.ID, SN, Name, ModifyDate, ModifyBy, Removed
					FROM Students s
					WHERE SN LIKE '$sn%' AND Name LIKE '$name%'
					ORDER BY ModifyDate DESC, CreateDate DESC, SN DESC limit 50";
			$table = mysql_GetArrayRows($sql);
			print(json_encode($table));
			break;

		case 'list_by_id':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sql = "SELECT ID, SN, Name, School, Class, Seat, 
					Mobile, Birth, DueDate, Removed, ModifyDate, ModifyBy
					FROM Students
					WHERE ID = '$id'";
			$dataset = array();
			$table = mysql_GetArrayRows($sql);
			array_push($dataset, $table);
			$sql = "SELECT g.ID, g.Name, CASE WHEN sg.StudentID IS NOT NULL THEN 'checked' ELSE '' END AS Checked
					FROM Groups g
					LEFT JOIN StudentGroups sg
					ON g.ID = sg.GroupID AND sg.StudentID = '$id'
					ORDER BY g.CreateDate DESC, g.Name DESC";
			array_push($dataset, mysql_GetArrayRows($sql));
			print(json_encode($dataset));
			break;
			
		case 'list_by_id_simple':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sql = "SELECT ID, SN, Name
					FROM Students
					WHERE ID = '$id'";
			$table = mysql_GetArrayRows($sql);
			print(json_encode($table));
			break;
			
		case 'mod':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sn = mysql_real_escape_string($_POST['sn'], $conn);
			$name = mysql_real_escape_string($_POST['name'], $conn);
			$school = mysql_real_escape_string($_POST['school'], $conn);
			$class = mysql_real_escape_string($_POST['class'], $conn);
			$seat = mysql_real_escape_string($_POST['seat'], $conn);
			$mobile = mysql_real_escape_string($_POST['mobile'], $conn);
			$birth = mysql_real_escape_string($_POST['birth'], $conn);
			$removed = mysql_real_escape_string($_POST['removed'], $conn);
			$groups = $_POST['groups'];
			$now = date('Y-m-d H:i:s');
			$username = $_SESSION['username'];

			$sqls = array();

			$arr = array();
			array_push($arr, "ID = '$id'");
			array_push($arr, "SN = '$sn'");
			array_push($arr, "School = '$school'");
			array_push($arr, "Class = '$class'");
			array_push($arr, "Seat = '$seat'");
			array_push($arr, "Name = '$name'");
			array_push($arr, "Mobile = '$mobile'");
			array_push($arr, "Birth = '$birth'");
			array_push($arr, "Removed = $removed");
			array_push($arr, "ModifyDate = '$now'");
			array_push($arr, "ModifyBy = '$username'");
			array_push($sqls, "UPDATE Students SET ".join(', ', $arr)." WHERE ID = '$id'");
			array_push($sqls, "DELETE FROM StudentGroups WHERE StudentID = '$id'");
			foreach($groups as $group) {
				$group = mysql_real_escape_string($group, $conn);
				array_push($sqls, "INSERT INTO StudentGroups VALUES('$id', '$group', '$now', '$username')");
			}
			mysql_ExecTransaction($sqls);
			break;
			
		case 'register':
			$conn = mysql_GetConnection();
			$studentid = mysql_real_escape_string($_POST['id'], $conn);
			$challenge = mysql_real_escape_string($_POST['ChallengeCode'], $conn);
			$activation = mysql_real_escape_string($_POST['ActivationCode'], $conn);
			$check = ipod_getActivationCode($challenge, '@#!....-_-....!#@');
			$username = $_SESSION['username'];
			
			$activation = str_replace("\xef\xbb\xbf", '', $activation);
			if($activation == $check) {
				$id = md5("$studentid$challenge$activation");
				$now = date('c');
				$y = date('Y');
				$m = date('m');
				if($m >= 7) {
					$y += 1;
					$m = 1;
				}
				else {
					$m = 7;
				}
				$due = date('c', mktime(0, 0, 0, 1, $m, $y));
				$sql = "INSERT INTO iPods VALUES('$id', '$studentid', '$challenge', '$activation', '$due', '$now', '$username', 0)";
				mysql_Exec($sql);
				echo '!!success!!';
			}
			else {
				echo '!!check_failed!!';
			}
			break;
	}
?>