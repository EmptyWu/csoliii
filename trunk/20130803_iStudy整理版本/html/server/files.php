﻿<?php

	if($_SESSION['username'] == "") die("<script>location.href='login.html';</script>");
	$doc_root = $_SERVER['DOCUMENT_ROOT'];
	require_once($doc_root.'/includes/file_io.php');
	require_once($doc_root.'/includes/mysql_tools.php');
	
	$_SESSION['selected'] = "files";

	switch($_GET['act']) {
		case 'list':
			$conn = mysql_GetConnection();
			$title = str_replace('*', '%', mysql_real_escape_string($_POST['title'], $conn));
			$description = str_replace('*', '%', mysql_real_escape_string($_POST['description'], $conn));
			$username = $_SESSION['username'];
			$sql = "SELECT ID, CASE WHEN Title = '' THEN '(空)' ELSE Title END AS Title,
					CASE WHEN Description = '' THEN '(空)' ELSE Description END AS Description, Size, CreateBy, CreateDate,
					'Y' as Download
					FROM Files
					WHERE IsLeaf = 1 AND Title LIKE '$title%' AND Description LIKE '$description%' and CreateBy='$username'
					ORDER BY CreateDate DESC";
			$table = mysql_GetArrayRows($sql);
					
			$sql = "select FileID, CreateBy from share_files where ShareAcc = '$username' and CreateBy <> '$username'";
			$shareTable = mysql_GetArrayRows($sql);
			$shareArray = array();
			for ( $i=0 ; $i<count($shareTable) ; $i++ ) {
				$exist = FALSE;
				$fid = $shareTable[$i][0];
				for ($j=0; $j<count($table); $j++) {
					if ($fid == $table[$j][0]) {
						$exist = TRUE;
						break;
					}
				}
				
				if (!$exist) {
					$sql = "SELECT ID, CASE WHEN Title = '' THEN '(空)' ELSE Title END AS Title,
							CASE WHEN Description = '' THEN '(空)' ELSE Description END AS Description, Size, CreateBy, CreateDate,
							'N' as Download
							FROM Files
							WHERE ID = '$fid'";
					$tempArray = mysql_GetArrayRows($sql);
					array_push($shareArray, array($tempArray[0][0], $tempArray[0][1], $tempArray[0][2], $tempArray[0][3], 
						$tempArray[0][4], $tempArray[0][5], $tempArray[0][6]));					
				}
			}	
			$count = count($shareArray);
			if ($count > 0) {
				for ($m=0; $m<$count; $m++) {
					array_push($table, array("ID" => $shareArray[$m][0], "Title" => $shareArray[$m][1], 
						"Description" => $shareArray[$m][2], "Size" => $shareArray[$m][3], "CreateBy" => $shareArray[$m][4],
						"CreateDate" => $shareArray[$m][5], "Download" => $shareArray[$m][6]));									
				}
			}				
			print(json_encode($table));
			break;
					
		case 'del':
			$conn = mysql_GetConnection();			
			$selections = json_decode(str_replace('\"', '"', $_POST['json']));

			for($i=0;$i<count($selections);$i++) {
				$id = $selections[$i]->ID;
				$sql = "SELECT ID, ParentID, IsLeaf, Path FROM Files WHERE ID = '$id'";
				$rows = mysql_GetArrayRows($sql);
				if(isset($rows[0])) {
					$file_path = $rows[0]['Path'];
					$path = "$doc_root/file_storage/$file_path"; 
					if(unlink($path)) {
						mysql_Exec("DELETE FROM Files WHERE ID = '$id'");
						mysql_Exec("DELETE FROM FileGroups WHERE FileID = '$id'");
						mysql_Exec("DELETE FROM share_files WHERE FileID = '$id' and CreateBy = 'wmch'");
					}
				}
			}
		
			break;
		
		case 'download':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_GET['id'], $conn);
			$sql = "SELECT ID, ParentID, IsLeaf, Path, Description,CONCAT(title,'.',Ext) fileName FROM Files WHERE ID = '$id'";
			$rows = mysql_GetArrayRows($sql);
			
			if(isset($rows[0])) {
				$file_path = $rows[0]['Path'];
				$path = "$doc_root/file_storage/$file_path";
 				if (file_exists($path)) {
 				$file_size = filesize($path);
					
				$file = fopen($path,"rb"); 
				header("Content-type: application/octet-stream");
				header("Accept-Ranges: bytes");
				header("Accept-Length: " . $file_size);
				header("Content-Disposition: attachment; filename=" . $rows[0]['fileName']);
				ob_end_clean();
				echo fread($file, $file_size);
				fclose($file);
				}
				else {
					echo "File not found!: ".$rows[0]['Description'];
					exit;
				}
			}
			break;
		case 'assignSingle':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sql = "DELETE FROM share_files WHERE FileID = '$id'";

			mysql_Exec($sql);
			$selections = json_decode(str_replace('\"', '"', $_POST['json']));
			$oplength = count($selections);//取總數
			for ( $i=0 ; $i<$oplength ; $i++ ) {
				$acc = $selections[$i]->ACC;
				$sql = "insert into share_files (CreateBy, CreateDate, FileID, ShareAcc) values ('wmch', now(), '$id', '$acc')";

				mysql_Exec($sql);
			}			
			
			break;
		case 'assignOperators':
			$conn = mysql_GetConnection();
			$selections = json_decode(str_replace('\"', '"', $_POST['json']));
			$selections_ops = json_decode(str_replace('\"', '"', $_POST['json_ops']));
			
			for ( $i=0 ; $i<count($selections_ops) ; $i++ ) {
				$acc = $selections_ops[$i]->ACC;
				for ($j=0; $j<count($selections); $j++) {					
					$id = $selections[$j]->ID;
					$sql = "DELETE FROM share_files WHERE ShareAcc = '$acc' and FileID = '$id'";					
					mysql_Exec($sql);
						
					$sql = "insert into share_files (CreateBy, CreateDate, FileID, ShareAcc) values ('wmch', now(), '$id', '$acc')";
					mysql_Exec($sql);
					//}			
				}
			}			
			
			break;
		case 'modifyName':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$text = mysql_real_escape_string($_POST['text'], $conn);
			$sql = "UPDATE Files SET Title = '$text' WHERE ID = '$id'";
			mysql_Exec($sql);
			break;

		case 'modifyDesc':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$text = mysql_real_escape_string($_POST['text'], $conn);
			$sql = "UPDATE Files SET Description = '$text' WHERE ID = '$id'";
			mysql_Exec($sql);
			break;
		case 'getItem':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sql = "SELECT ID, concat(Title, '.', Ext) as FileName FROM Files WHERE ID = '$id'";
			$table = mysql_GetArrayRows($sql);
			print(json_encode($table));
			break;
		case 'getItemList':
			$conn = mysql_GetConnection();
			$selections = json_decode(str_replace('\"', '"', $_POST['json']));
			$tmp = "";
			for($i=0;$i<count($selections);$i++) {
				$id = $selections[$i]->ID;
				$tmp = $tmp . "'$id' ";
			}
			$tmp = trim($tmp);
			$tmp = str_replace(" ", ",", $tmp);						
			$sql = "SELECT ID, concat(Title, '.', Ext) as FileName FROM Files WHERE ID in ($tmp)";
			$table = mysql_GetArrayRows($sql);
			print(json_encode($table));							
			break;
		case 'listShareOperator':
			$conn = mysql_GetConnection();
			$selections = json_decode(str_replace('\"', '"', $_POST['json']));
			$sql = "select Acc, Name, '' as FileID, 0 as Checked, '' as CheckAttr from Operators where Acc <> 'wmch'";
			//$sql = "select a.Acc as Acc, a.Name as Name, (select FileID from share_files where ShareAcc=a.Acc and FileID='$id') as FileID, 0 as Checked, '' as CheckAttr from operators a";
			$tableOperator = mysql_GetArrayRows($sql);
			
			for ($i=0;$i<count($tableOperator);$i++)
			{
				$acc = $tableOperator[$i][0];
				$success = TRUE;
				for($j=0;$j<count($selections);$j++) {
					$id = $selections[$j]->ID;
					$sql = "select FileID from share_files where ShareAcc='$acc' and FileID = '$id'";       
					$shares = mysql_GetArrayRows($sql);
					if (count($shares) == 0) {
						$success = FALSE;
						break;
					}
				}
				if ($success) {
						//$tableOperator[$i][2] = $tableShare[$j][0];
						$tableOperator[$i][3] = 1;
						$tableOperator[$i][4] = 'checked';					
				}
			}
			
			print(json_encode($tableOperator));
			break;
		case 'listOperator':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sql = "select Acc, Name, '' as FileID, 0 as Checked, '' as CheckAttr from Operators where Acc <> 'wmch'";
			$tableOperator = mysql_GetArrayRows($sql);
			 
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sql = "select FileID, ShareAcc from share_files where FileID='$id'";
			$tableShare = mysql_GetArrayRows($sql);

			$oplength = count($tableOperator);//取總數
			$sharelength = count($tableShare);
			for ( $i=0 ; $i<$oplength ; $i++ ) {
				for ($j=0; $j<$sharelength; $j++) {
					if ($tableOperator[$i][0] == $tableShare[$j][1]) {
						$tableOperator[$i][2] = $tableShare[$j][0];
						$tableOperator[$i][3] = 1;
						$tableOperator[$i][4] = 'checked';
						break;
					}
				}
	
			}			
		
			print(json_encode($tableOperator));
			break;
	}
?>