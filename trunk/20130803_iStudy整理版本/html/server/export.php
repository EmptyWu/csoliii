<?php
	$doc_root = $_SERVER['DOCUMENT_ROOT'];
	require_once($doc_root.'/includes/file_io.php');
	require_once($doc_root.'/includes/mysql_tools.php');
	require_once($doc_root.'/includes/ipod_algorithm.php');

	$sql = "SELECT * FROM Students";
	$table = mysql_GetArrayRows($sql);
	
	$html = "<table>";
	$html .= "<tr>";
	foreach($table[0] as $column => $value) {
		if(is_int($column)) continue;
		$html .= "<th>" . $column . "</th>";
	}
	$html .= "</tr>";

	foreach($table as $row) {
		$html .= "<tr>";
		foreach($row as $column => $value) {
			if(is_int($column)) continue;
			$html .= "<td>".$value."</td>";
		}
		$html .= "</tr>";
	}
	$html .= "</table>";
	print($html);
?>