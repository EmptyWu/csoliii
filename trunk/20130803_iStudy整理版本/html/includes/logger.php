<?php
	require_once('file_io.php');
	function logWithPrefix($prefix, $msg) {
		$date = date('Ymd');
		$time = date('H:i:s');
		$doc_root = $_SERVER['DOCUMENT_ROOT'];
		file_appendline("$doc_root/log/{$prefix}_$date.log", "$time\t$msg\r\n");
	}
	
	function log_msg($msg) {
		logWithPrefix('msg', $msg);
	}

	function log_error($msg) {
		logWithPrefix('error', $msg);
	}

	function log_mysql_error($msg) {
		logWithPrefix('mysql_error', $msg);
	}	
?>