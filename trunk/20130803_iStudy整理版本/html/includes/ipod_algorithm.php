<?php
	function ipod_getDecimalSum($string) {
		$string = (string)$string;
		$result = 0;
		for ($i=0;$i<strlen($string);$i++) {
			$pow = $i % 4;
			$result += ord($string[$i]) * pow(256, $pow) % pow(10, 8);
		}
		return substr('00000000' . $result % pow(10, 8), -8);
	}
		
	function ipod_getPCDFromSID($SID) {
		return ipod_getDecimalSum("xxxzzz$SID..--!!xzsx");
	}
?>