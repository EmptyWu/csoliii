<?php
/**
 * Logging class:
 * - contains lfile, lwrite and lclose public methods
 * - lfile sets path and name of log file
 * - lwrite writes message to the log file (and implicitly opens log file)
 * - lclose closes log file
 * - first call of lwrite method will open log file implicitly
 * - message is written with the following format: [d/M/Y:H:i:s] (script name) message
 */

class Logging {
	// declare log file and file pointer as private properties
	private $log_file, $fp;
	// set log file (path and name)
	public function lfile($path) {
		$this -> log_file = $path;
	}

	// write message to the log file
	public function lwrite($message) {
		// if file pointer doesn't exist, then open log file
		if (!is_resource($this -> fp)) {
			$this -> lopen();
		}
		// define script name
		$script_name = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);
		// define current time and suppress E_WARNING if using the system TZ settings
		// (don't forget to set the INI setting date.timezone)
		$time = @date('[d/M/Y:H:i:s]');
		// write current time, script name and message to the log file
		fwrite($this -> fp, "$time ($script_name) $message" . PHP_EOL);
	}

	// close log file (it's always a good idea to close a file when you're done with it)
	public function lclose() {
		fclose($this -> fp);
	}

	// open log file (private method)
	private function lopen() {
		// in case of Windows set default log file
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			$log_file_default = 'c:/php/logfile.txt';
		}
		// set default log file for Linux and other systems
		else {
			$log_file_default = '/tmp/logfile.txt';
		}
		// define log file from lfile method or use previously set default
		$lfile = $this -> log_file ? $this -> log_file : $log_file_default;
		// open log file for writing only and place file pointer at the end of the file
		// (if the file does not exist, try to create it)
		$this -> fp = fopen($lfile, 'a') or exit("Can't open $lfile!");
	}

}

function WriteLog($value) {
	// Logging class initialization
	$log = new Logging();
	// set path and name of log file (optional)
	$log -> lfile('mylog.txt');
	// write message to the log file
	$log -> lwrite($value);

	// close log file
	$log -> lclose();
}

function file_append($filename, $text) {
	if (is_file($filename)) {
		file_put_contents($filename, $text, FILE_APPEND);
	} else {
		file_write($filename, $text);
	}
}

function file_appendline($filename, $text) {
	file_append($filename, "{$text}\n");
}

function file_write($filename, $text) {
	if (file_exists($filename)) {
		file_put_contents($filename, $text);
	} else {
		$file = fopen($filename,"a+"); //開啟檔案
    fwrite($file,$text);
    fclose($file);

	}
}

function file_readall($filename) {
	$content = file_get_contents($filename);
	return $content;
}

function file_readline($filename) {
	return preg_split('/\r?\n/', file_readall($filename));
}

function dir_DeleteTree($dir) {
	if (!file_exists($dir))
		return true;
	if (!is_dir($dir))
		return unlink($dir);
	foreach (scandir($dir) as $item) {
		if ($item == '.' || $item == '..')
			continue;
		if (!dir_DeleteTree($dir . DIRECTORY_SEPARATOR . $item))
			return false;
	}
	return rmdir($dir);
}

function dir_CleanUploadTempFolder($path) {
	if (!is_dir($path))
		return;
	$check_hours = 24;
	$upload_session_list = glob("$path*");
	$now = time();
	foreach ($upload_session_list as $upload_session) {
		if (is_file($upload_session)) {
			unlink($upload_session);
		} else {
			$upload_file_list = glob("$upload_session/*");
			$should_remove = false;
			foreach ($upload_file_list as $upload_file) {
				if (is_dir($upload_file))
					continue;
				$mtime = filemtime($upload_file);
				$hours_from_now = floor(($now - $mtime) / (60 * 60));
				if ($hours_from_now >= $check_hours) {
					$should_remove = true;
					break;
				}
			}
			if ($should_remove)
				dir_DeleteTree($dir);
		}
	}
}
?>