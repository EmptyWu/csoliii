<?php
	require_once(dirname(__FILE__).'/../includes/file_io.php');
	$mime_filter = array('text/plain','text/html',
					'application/pdf','application/msword','application/rtf','application/vnd.ms-powerpoint',
					'application/x-zip-compressed',
					'image/x-ms-bmp','image/jpeg','image/gif','image/png',
					'video/mp4',
					'audio/x-aiff','audio/mpeg','audio/mp3');
	$Extension_filter=array('html','htm',
							'pdf','doc','docx',
							'xls','xlsx',
							'key','pages','numbers',
							'jpg','png','tiff','gif',
							'mp3','aiff','m4a','wav',
							'txt','rtf',
							'ppt','pptx',
							'mov','mp4','m4v',
							'zip');
	if($_FILES['file']['error'] == UPLOAD_ERR_OK){
	$File_Extension = explode(".", $_FILES['file']['name']);	
		//if(!in_array($_FILES['file']['type'], $mime_filter)) {
		if(!in_array($File_Extension[count($File_Extension)-1], $Extension_filter)) {
			die('!!badtype!!');
		}
		$path = '../upload_tmp/' . $_POST['session'] . '/';
		if(!is_dir($path)) mkdir($path);
		$path .= $_POST['APC_UPLOAD_PROGRESS'];
		if(move_uploaded_file($_FILES['file']['tmp_name'], $path)){
			file_write($path.'.info',json_encode($_FILES['file']));
			unset($_FILES['file']['tmp_name']);
			echo json_encode($_FILES['file']);
		}
		else {
			echo '!!failed!!';
		}
	}
?>
