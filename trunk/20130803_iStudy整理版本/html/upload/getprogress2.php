<?php
header('Expires: Tue, 08 Oct 1991 00:00:00 GMT');
header('Cache-Control: no-cache, must-revalidate');

if(isset($_GET['id'])) {
	$status = apc_fetch('upload_' . $_GET['id']);
	if($status) {
		if($status['total'] == 0) {
			echo "not_found";
		}
		else {
			echo round($status['current']/$status['total']*100);
		}
	}
	else {
		echo "apc_fetch_failed";
	}
}
else {
	echo "id_not_defined";
}
?>