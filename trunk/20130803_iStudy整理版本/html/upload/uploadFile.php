<?php
	require_once(dirname(__FILE__).'/../includes/file_io.php');
	require_once(dirname(__FILE__).'/../includes/mysql_tools.php');
	require_once(dirname(__FILE__).'/../includes/logger.php');
	
	$mime_filter = array('text/plain','text/html',
					'application/pdf',
					'application/msword','application/rtf','application/vnd.openxmlformats-officedocument.wordprocessingml.document',
					'application/vnd.ms-powerpoint','application/x-zip-compressed',
					'image/x-ms-bmp','image/jpeg','image/gif','image/png',
					'video/mp4',
					'audio/mp3','audio/mpeg');
	$Extension_filter=array('html','htm',
							'pdf','doc','docx',
							'xls','xlsx',
							'key','pages','numbers',
							'jpg','png','tiff','gif',
							'mp3','aiff','m4a','wav',
							'txt','rtf',
							'ppt','pptx',
							'mov','mp4','m4v',
							'zip');
	$File_Extension = explode(".", $_FILES['file']['name']);				
	//log_msg($_FILES['file']['name'] ."==". $_FILES['file']['type'] ."==". $File_Extension[count($File_Extension)-1]);
	if($_FILES['file']['error'] == UPLOAD_ERR_OK){
		//if(!in_array($_FILES['file']['type'], $mime_filter)) {
		if(!in_array($File_Extension[count($File_Extension)-1], $Extension_filter)) {
			die('!!badtype!!');
		}
		
		$username = $_SESSION['username'];
		$size = $_FILES['file']['size'];
		$mime = $_FILES['file']['type'];
		
		$file_name_without_ext = "";
		$file_ext = "";
		$filename = "";
		$path = "";
		$fullpath = "";
		do {
			$file_name_without_ext = urldecode(pathinfo(str_replace('%2F', '/', urlencode($_FILES['file']['name'])), PATHINFO_FILENAME));
			$file_ext = urldecode(pathinfo(str_replace('%2F', '/', urlencode($_FILES['file']['name'])), PATHINFO_EXTENSION));
			$filename = $file_name_without_ext.".".$file_ext;
			$path = "f".md5(date("YmdHis").$_SERVER["REMOTE_ADDR"]).".".$file_ext;
			$fullpath = '../file_storage/'.$path;
		} while (file_exists($fullpath));
		
		if(move_uploaded_file($_FILES['file']['tmp_name'], $fullpath)){
			$sql = "INSERT INTO `Files`(ID,ParentID,IsLeaf,Title,Description,Path,CreateDate,CreateBy,DueDate,Size,MIME,Ext) VALUES(uuid(), '', 1, '$file_name_without_ext', '$filename', '$path', now(), '$username', adddate(now(), 5), $size, '$mime', '$file_ext')";
			mysql_Exec($sql);
			echo '!!success!!';
		}
		else {
			echo '!!failed!!';
		}
	}
?>