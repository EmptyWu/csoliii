﻿var answers = [];
var current = 0;
var debug = false;
var chosenanswers = [];

for(var i=0;i<quiz_list.length;i++) {
	answers[i] = [];
}
location.hash = answers.collect(function(ans){return answers.indexOf(ans)+"="+ans.join(";")}).join("&");

Event.observe(window, "load", function() {
	$$("a[href='#']").invoke("observe", "click", function(event) {event.stop()});
	$("prev").observe("click", go_prev);
	$("next").observe("click", go_next);
	$("prev2").observe("click", go_prev);
	$("next2").observe("click", go_next);
	$("done").observe("click", done);
	$("done2").observe("click", done);
	update_title(quiz_title);
	goto_quiz(1);
});

function update_title(title) {
	$("title").update(title);
}
function update_quiz_index(current, total) {
	$("quiz_index").update(" #{current} / #{total} ".interpolate({current:current,total:total}));
	$("quiz_index2").update(" #{current} / #{total} ".interpolate({current:current,total:total}));
}
function go_prev() {
	if(current>0) goto_quiz(--current+1);
	else goto_quiz((current=quiz_list.length-1)+1);
}
function go_next() {
	if(current<quiz_list.length-1) goto_quiz(++current+1);
	else goto_quiz((current=0)+1);
}
function goto_quiz(num) {
	current = num-1;
	update_quiz_index(num, quiz_list.length);
	
	load_quiz(quiz_list[current]);
	update_bubbles();
	window.scrollTo(0,1);
}
function update_bubbles() {
	$$("#bubbles a.button").invoke("stopObserving");
	$$("#bubbles2 a.button").invoke("stopObserving");

	var bubbles = get_bubbles_html();
	$("bubbles").update(bubbles);
	$("bubbles2").update(bubbles);
	$$("#bubbles a.button").invoke("observe", "click", bubbles_button_click);
	$$("#bubbles2 a.button").invoke("observe", "click", bubbles_button_click);
}
function get_bubbles_html() {
	var types = quiz_types[current].split(";");
	var choices = quiz_choices[current].split(";");
	var lis = [];
	for(var i=0;i<types.length;i++) {
		var ans = answers[current][i];
		lis.push("<li class='#{type}'>#{buttons}</li>".interpolate({
			type: types[i] == "多選" ? "multiple" : "single",
			buttons: $A(choices[i]).collect(function(c) {return "<a class='button "+((ans==c)?"black":"blue")+"' href='#'>"+c+"</a>"}).join("")
		}));
	}
	return "<ul>#{lis}</ul>".interpolate({lis:lis.join("")});
}
function bubbles_button_click(event) {
	event.stop();
	var type = event.element().up("li").className;
	var index = li_index(event.element().up("li"));
	var ans = event.element().innerHTML;
	var choosing = event.element().hasClassName("blue");
	
	if(choosing) {
		if(type == "single") {
			event.element().up("li").select(".button").invoke("removeClassName", "black").invoke("addClassName", "blue");
			event.element().removeClassName("blue").addClassName("black");
		}
		else {
			event.element().removeClassName("blue").addClassName("black");
		}
		choose(type,index,ans);
	}
	else {
		event.element().removeClassName("black").addClassName("blue");
		reject(type,index,ans);
	}
	update_bubbles();
}
function li_index(li) {
	return li.up("#bubbles") ? $("bubbles").select("li").indexOf(li) : $("bubbles2").select("li").indexOf(li);
}
function choose(type,idx,c) {
	if(type=="single")
		answers[current][idx] = c;
	else
		answers[current][idx] = answers[current][idx] ? $A(answers[current][idx] + c).sort().join("") : c;

	location.hash = answers.collect(function(ans){return answers.indexOf(ans)+"="+ans.join(";")}).join("&");
	if(debug) chosenanswers.push("#{a}-#{b}-#{c}".interpolate({a:current,b:idx,c:answers[current][idx]}));
	if(debug) $("answer_record").update("<ul>"+chosenanswers.collect(function(c){return "<li>"+c+"</li>"}).join("")+"</ul>");
}
function reject(type,idx,c) {
	if(type=="single")
		answers[current][idx] = "";
	else
		answers[current][idx] = answers[current][idx] ? $A(answers[current][idx]).without(c).sort().join("") : "";

	location.hash = answers.collect(function(ans){return answers.indexOf(ans)+"="+ans.join(";")}).join("&");
	if(debug) chosenanswers.push("#{a}-#{b}-#{c}".interpolate({a:current,b:idx,c:answers[current][idx]}));
	if(debug) $("answer_record").update("<ul>"+chosenanswers.collect(function(c){return "<li>"+c+"</li>"}).join("")+"</ul>");
}
function load_quiz(src) {
	$("quiz").update("<iframe src='#{src}' onload='resize_iframe()'></iframe>".interpolate({src:src}));
}
function resize_iframe() {
	$$("#quiz iframe").first().setStyle({height:$$("#quiz iframe").first().contentWindow.document.documentElement.clientHeight+"px"});
}
function done() {
	if(confirm("確定要交卷??")) {
		if(typeof quiz_exercise === "undefined") {
			$("controls").update();
			$("controls2").update();
			$("bubbles").update();
			$("bubbles2").update();
			location.hash += "&done";
			$("done").hide();
			$("done2").hide();
			location.href = "done.html" + location.hash;
		}
		else {
			location.href = "feedback.html" + location.hash;
		}
	}
}