﻿var answers = [];
var match_result = [];

Event.observe(window, "load", function() {
	answers = get_answers();
	$("exit").observe("click", exit);
	$("title").update("解答：" + quiz_title);
	match_answers();
	draw_ui();
});

function get_answers() {
	var result = [];
	var pairs = location.hash.substring(1).split("&");
	pairs.each(function(o) {var p=o.split("=");result[p[0]]=p[1]});
	return result;
}

function match_answers() {
	match_result = [];
	for(var i=0;i<quiz_types.length;i++) {
		var result = [];
		result["index"] = (i+1).toPaddedString(2);
		result["quiz"] = quiz_list[i];
		result["detail"] = quiz_details[i];
		
		var types = quiz_types[i].split(";");
		var quiz_ans = quiz_answers[i].split(";");
		var ans = answers[i].split(";");
		for(var j=0;j<types.length;j++) {
			result.push({"index": (j+1).toPaddedString(2), "type": types[j], "q_ans": quiz_ans[j], "ans": ans[j] || "未作答", "correct": (quiz_ans[j] == ans[j])});
		}
		match_result.push(result);
	}
}

function draw_ui() {
	$$("#detail a.detail").invoke("stopObserving");
	var items = [];
	match_result.each(function(result) {
		var item = ["<div>題目 - #{index}</div><iframe src='#{quiz}' onload='resize_iframe(this)' style='width:100%;background:#fff;-webkit-border-radius:8px'></iframe>".interpolate(result)];
		result.each(function(r) {
			item.push("<span style='color:#{color}'>#{index}. #{result} &rarr; 填答:#{ans} &rarr; 答案:#{q_ans} &rarr; 題型:#{type}</span>".interpolate(Object.extend(r, {result: r.correct?"V":"X", color:r.correct?"navy":"tomato"})));
		});
		if(result["detail"]) item.push(result["detail"].split(";").collect(function(d) {return "<a href='#{d}' style='display:inline-block;font-size:80%;margin-right:5px' class='button green detail'>詳解</a>".interpolate({d:d})}).join(""));
		items.push("<ul>" + item.collect(function(i) {return "<li>" + i + "</li>"}).join("") + "</ul>");
	});
	$("detail").update(items.join(""));
	$$("#detail a.detail").invoke("observe", "click", show_detail);
}

function show_detail(event) {
	var href = event.element().readAttribute("href");
	if(href.indexOf(".mp3") > -1 || href.indexOf(".mp4") > -1) return;
	event.stop();
	location.href = "detail.html#" + href;
}

function resize_iframe(element) {
	$(element).setStyle({height:$(element).contentWindow.document.documentElement.clientHeight+"px"});
}

function exit() {
	if(confirm("確定要離開??")) {
		location.href = "blank.html" + location.hash;
	}
}