//
//  DownloadObject.h
//  DownloadCenter
//
//  Created by Chen Wei Ting on 2010/10/31.
//  Copyright 2010 TTU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IKDownloadDelegate.h"
#import "SSZipArchive.h"

@interface DownloadObject : NSObject {
	
	int ser;
	
	NSString *key;
	
	NSURL *url;
	NSString *path;
	
	BOOL autoResume;
	int timeOut;
	int fileType;
	id<IKDownloadDelegate> innrCenterDelegate;
	
	NSString *tmpStoreFile;
	NSURLConnection* connection;
	NSMutableData* data;
	
	unsigned long long zipFileSize ;
	unsigned long long nowReadSize ; //已經讀到的大小，要計算百分比的
	
}
@property(nonatomic, retain)NSString *key;
@property(nonatomic, retain)NSURL *url;
@property(nonatomic, retain)NSString *path;
@property(assign)BOOL autoResume;
@property(assign)int timeOut;
@property(assign)int fileType;
@property(assign)int ser;
@property(assign)id<IKDownloadDelegate> innrCenterDelegate;

-(NSString *)genTmpFile:(NSString *)str;
-(void)startDownload;
//停止下載
-(void)stopDownload;
@end
