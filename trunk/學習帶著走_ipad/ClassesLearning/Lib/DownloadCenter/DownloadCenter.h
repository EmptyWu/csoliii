//
//  DownloadCenter.h
//  DownloadCenter
//
//  Created by Chen Wei Ting on 2010/10/31.
//  Copyright 2010 TTU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DownloadObject.h"
#import "IKDownloadDelegate.h"

@interface DownloadCenter : NSObject <IKDownloadDelegate> {

	
	NSMutableArray *queue;
	NSMutableArray *startDownloadQueue;
	
	id<IKDownloadDelegate> delegate;
	
	
	int maxActive;
	int downloadSeries;
	int nowActiveConnect;
	int haveRec;
	BOOL isRunning;
}
@property(nonatomic, assign)id<IKDownloadDelegate> delegate;
@property(nonatomic)int haveRec;
@property(nonatomic)int nowActiveConnect;

+(DownloadCenter *)defaulCenter;
+(void)releaseCenter;

-(int)amountInQueue;

#pragma mark -
-(id)initWithMaxActive:(int)_maxActive;
-(int)addFilePath:(NSURL *)url ToPath:(NSString *)path Key:(NSString *)key FileType:(int)fileType Timeout:(int)timeout autoResume:(BOOL)resume;
-(int)addFilePath:(NSURL *)url ToPath:(NSString *)path Key:(NSString *)key FileType:(int)fileType;
-(void)stopAndRemoveObject:(NSString *)key;

@end
