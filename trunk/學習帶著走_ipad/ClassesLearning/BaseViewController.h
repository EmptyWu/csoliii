//
//  BaseViewController.h
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/1/29.
//  Copyright 2011 TTU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "define.h"
#import "Utility.h"
#import "NewTechUtility.h"
#import "FileListViewController.h"
#import "IKDownloadDelegate.h"
#import "DownloadCenter.h"


@interface BaseViewController : UINavigationController <IKDownloadDelegate>{

	NSThread *backThread;
	
	
	NSMutableArray *fileList;
	
	DownloadCenter *downloadCenter;
	
	
	NSMutableDictionary *storeDownObject;
	NSMutableDictionary *updateProgressBar;
	
	Boolean isChange;
	
	
	FileListViewController *rootViewController;
}
@property(nonatomic, retain)FileListViewController *rootViewController;
@property(nonatomic, retain)NSMutableDictionary *updateProgressBar;
@property(nonatomic, retain)NSMutableDictionary *storeDownObject;
@property(nonatomic, retain)DownloadCenter *downloadCenter;
@property(nonatomic, retain)NSThread *backThread;
@property(nonatomic, retain)NSMutableArray *fileList;

//從server下載清單
-(void)loadDataInBackGround;
//載入檔案清單
-(void)loadFileList;
//下載一各檔案
-(void)loadOneFile:(NSString *)Id AtUrl:(NSString *)url;

//與新的清單合併
-(void)mergeNewListFile:(NSArray *)newList;
//把新的list 加到 舊的list
-(void)mergeNewList:(NSArray *)newList ToTargetList:(NSMutableArray *)target;
//下載檔案
-(void)startLoadFile:(NSArray *)arrList;

//與更新Bar綁定
-(void)unBindUpdateProgress:(UITableViewCell *)cellUnit;
-(void)BindUpdateProgress:(UITableViewCell *)cellUnit Key:(NSString *)key;
-(void)updateProgressBarWithKey:(NSString *)key;
@end
