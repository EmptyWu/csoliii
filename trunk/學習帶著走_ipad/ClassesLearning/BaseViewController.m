    //
//  BaseViewController.m
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/1/29.
//  Copyright 2011 TTU. All rights reserved.
//

#import "BaseViewController.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@implementation BaseViewController
@synthesize fileList,backThread;
@synthesize downloadCenter,storeDownObject,updateProgressBar,rootViewController;

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[rootViewController release];
	[updateProgressBar release];
	[storeDownObject release];
	[downloadCenter release];
	[backThread release];
	[fileList release];
    [super dealloc];
}

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	//判斷tmp目錄是否存在
	NSLog(@"ppp=%@",[Utility genTmpFolder]);
	if (![[NSFileManager defaultManager] fileExistsAtPath:[Utility genTmpFolder]] )
	{
		[[NSFileManager defaultManager] createDirectoryAtPath:[Utility genTmpFolder] attributes:nil];
	}
	
	
	
	
	DownloadCenter *dd = [[DownloadCenter alloc] initWithMaxActive:10];
	dd.delegate = self;
	self.downloadCenter = dd;
	[dd release];
	
	
	NSMutableDictionary *stp = [[NSMutableDictionary alloc] init];
	self.storeDownObject = stp;
	[stp release];
	
	stp = [[NSMutableDictionary alloc] init];
	self.updateProgressBar = stp;
	[stp release];
	
	
	//製作根畫面
	FileListViewController *listViewController = [[FileListViewController alloc] initWithStyle:UITableViewStylePlain];
	self.rootViewController = listViewController;
	[listViewController release];
	rootViewController.title = @"Home";
	[self pushViewController:listViewController animated:NO];
	
	//製作編輯來源的選單
	[listViewController makeEditBtn];
	
	
	[self loadData];
	
	
	//下載更新檔案
	[NSThread detachNewThreadSelector:@selector(loadDataInBackGround) toTarget:self withObject:nil];
	
	
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeSource:) name:@"ChangeSource" object:nil];
	
	
	
	
	
}


-(void)changeSource:(NSNotification *)notifaction
{
	isChange = YES;
	
	//把原本的 list檔刪除
	//NSString *path = [NewTechUtility getFileListPath];
	//[[NSFileManager defaultManager] removeItemAtPath:path error:nil];
	
	
	
	//重新載入
	[self loadData];
	
	
	isChange = NO;
	
	[self popToRootViewControllerAnimated:YES];
}


-(void)loadData
{
	//載入目前檔案清單
	[self loadFileList];
	
	
	
	//載入list
	
	rootViewController.dataList = self.fileList;
	

	
	
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}



#pragma mark -
#pragma mark Funciton
//從server下載清單
-(void)loadDataInBackGround
{
	//NSLog(@"In Background");
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	while (YES) {
		
		
		if ( isChange )
		{
			[NSThread sleepForTimeInterval:kSleepInterval];
			continue;
		}
		
		//取得 這個資料源的描述檔
		NSString *urlStr = [NewTechUtility getDataUrl];
		
		NSURL *url = [NSURL URLWithString:urlStr];
		//NSLog(@"Host=%@",[url host]);
		
		//檢查網路是否活著
		if ( [Utility hostAvailable:[url host]] )
		{
			
			//urlStr = @"http://192.168.1.67/csoliii/ipod/a1.php";
			//urlStr = @"http://www.google.com";
			NSLog(@"xxxxurlStr=%@",urlStr);
			
			NSString *data = @"";
			//NSString *data =[NSString stringWithContentsOfURL:[NSURL URLWithString:urlStr] encoding:NSUTF8StringEncoding error:nil];
			
			
			ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
			[request setTimeOutSeconds:4];
			[request startSynchronous];
			NSError *error = [request error];
			if (!error) {
				//NSString *response = [request responseString];
				NSData *dd = [request responseData];
				data = [[[NSString alloc] initWithData:dd encoding:NSUTF8StringEncoding] autorelease] ;
				
			}			
			//NSLog(@"Data=%@",data);
			NSArray *newData = [data JSONValue];
			
			if ( newData == nil )
			{
				NSLog(@"json 資料載入錯誤");
			}
			else {
				[self mergeNewListFile:newData];
				
				[self startLoadFile:fileList];
			}

				
			//[self loadFileList];
		}
		NSLog(@"In Background");
		
		UIViewController  *vc = (UIViewController*)self.topViewController;
		if ( vc != nil && [vc isKindOfClass:[FileListViewController class]] )
		{
			FileListViewController *vvc = (FileListViewController*)vc;
			[(FileListViewController*)vvc.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
		}
		
		
		
		[NSThread sleepForTimeInterval:kSleepInterval];
		//break;
	}
	
	
	
	[pool release];
	
}

//載入檔案清單
-(void)loadFileList;
{
	NSString *path = [NewTechUtility getFileListPath];
	if ( [[NSFileManager defaultManager] fileExistsAtPath:path])
	{
		NSMutableArray *arr = [NSMutableArray arrayWithContentsOfFile:path];
		self.fileList = arr;
	}
	else {
		NSMutableArray *arr = [[NSMutableArray alloc] init];
		self.fileList = arr;
		[arr release];
		[self.fileList writeToFile:path atomically:YES];
	}

	
}

//與新的清單合併
-(void)mergeNewListFile:(NSArray *)newList;
{
	[self mergeNewList:newList ToTargetList:self.fileList];
	
	[self.fileList writeToFile:[NewTechUtility getFileListPath] atomically:YES];
}

//把新的list 加到 舊的list
-(void)mergeNewList:(NSArray *)newList ToTargetList:(NSMutableArray *)target
{
	
	
	//先建立TMP的結構
	//NSMutableDictionary *tmpDict = [[NSMutableDictionary alloc] init];
	
	
	//先比對這一層，有哪些需要新增
	NSDictionary *preProcessDict = nil ;
	for(NSDictionary *dict in newList )
	{
		//[target addObject:dict];
		NSString *Id = [dict valueForKey:@"ID"];
		
		BOOL isFind = NO;
		NSDictionary *targetFindDict = nil;
		//尋找是否已經存在
		for(NSDictionary *targetDict in target)
		{
			if ([Id isEqualToString:[targetDict valueForKey:@"ID"]])
			{
				targetFindDict = targetDict;
				isFind = YES;
				break;
			}
			
		}
		
		//如果不存在，則新增
		if ( isFind ) //如果存在，則往下一層判斷
		{
			//NSLog(@"發現資料id=%@",Id);
			NSArray *arr = [dict valueForKey:@"Items"];
			//如果有下一層，則處理
			if ( [arr count] > 0 )
			{
				NSMutableArray *targetArr = [targetFindDict valueForKey:@"Items"];
				if ( [targetArr count] == 0 )
				{
					[targetFindDict setValue:arr forKey:@"Items"];
				}
				else
				{
					[self mergeNewList:arr ToTargetList:targetArr];
				}

			}
		}
		else //如果不存在，責直接新增
		{
			
			//找到上一個位置
			if ( preProcessDict == nil )
			{
				//NSLog(@"新增1");
				[target insertObject:dict atIndex:0];
			}
			else 
			{
				int indsertIndex = -1;
				NSString *preId = [preProcessDict valueForKey:@"ID"];
				//for(NSDictionary *targetDict in target)
				for(int jj=0;jj<[target count];jj++)
				{
					
					NSDictionary *ddp  = [target objectAtIndex:jj];
					//NSLog(@"preId=%@  vvv=%@",preId,[ddp valueForKey:@"ID"]);
					if ([preId isEqualToString:[ddp valueForKey:@"ID"]])
					{
						indsertIndex = jj;
						break;
					}
					
				}
				
				if (indsertIndex == -1)
				{
					//NSLog(@"新增2");
					[target addObject:dict];
				}
				else
				{
					//NSLog(@"新增3=%d",indsertIndex);
					[target insertObject:dict atIndex:indsertIndex+1];
				}

			}

		}

		preProcessDict = dict;
	}
	
	
	//先檢查是否有檔案要移除的
	//for(NSDictionary *dict in target)
	NSMutableArray *readyRemoveList = [[NSMutableArray alloc] init];
	for(int i=0;i<[target count];i++)
	{
		NSDictionary *dict = [target objectAtIndex:i];
		//檢查看這一個id有沒有存在
		NSString *Id = [dict valueForKey:@"ID"];
		
		BOOL isFind = NO;
		//尋找是否已經存在
		for(NSDictionary *targetDict in newList)
		{
			if ([Id isEqualToString:[targetDict valueForKey:@"ID"]])
			{
				isFind = YES;
				break;
			}
			
		}
		
		//如果不存在，則新增
		if ( !isFind )
		{
			//移除item
			//NSLog(@"移除item id=%@",Id);
			//[target removeObject:dict];
			[readyRemoveList addObject:dict];
		}
		
		
	}
	
	//移除item
	for(NSDictionary *dd in readyRemoveList)
	{
		[target removeObject:dd];
	}
	[readyRemoveList release];
	
	
}

//下載檔案
-(void)startLoadFile:(NSArray *)arrList;
{
	//開始下載檔案
	for (NSDictionary *dict in arrList) {
		
		int typeId = [NewTechUtility getType:[dict valueForKey:@"Type"]];
		if ( typeId == Type_Folder )
		{
			[self startLoadFile:[dict valueForKey:@"Items"]];
			continue;
		}
		
		//下載檔案
		NSString *Id = [dict valueForKey:@"ID"];
		NSString *url = [dict valueForKey:@"Url"];
		
		//NSLog(@"ccurl=%@",url);
		
		[self loadOneFile:Id AtUrl:url];
	}
}


//下載一各檔案
-(void)loadOneFile:(NSString *)Id AtUrl:(NSString *)url
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	//製作儲存的檔名
	NSString *fileName = [Utility genKey:[NSURL URLWithString:url]];
	fileName = [NSString stringWithFormat:@"%@-%@",Id,fileName];
	
	NSString *toPath = [Utility genDocumentPath:fileName];
	
	NSString *tmpCheckPath = [Utility genTmpCheckFile:Id];
	//NSLog(@"tmpCheckPath=%@",tmpCheckPath);
	
	//檢查這一各檔案是否已經下載完成了
	if ([fileManager fileExistsAtPath:tmpCheckPath])
	{
		return;
	}
	
	//檢查是不是已經在下載中 ，如果沒有則下載
	if ( [storeDownObject valueForKey:Id] == nil )
	{
		[downloadCenter addFilePath:[NSURL URLWithString:url] ToPath:toPath Key:Id FileType:1];
		NSMutableDictionary *info = [[NSMutableDictionary alloc] init];
		[info setValue:[NSNumber numberWithInt:0] forKey:@"Length"];
		[info setValue:[NSNumber numberWithInt:0] forKey:@"CurrentLength"];
		[storeDownObject setValue:info forKey:Id];
		[info release];
	}
}


#pragma mark -
#pragma mark DownloadCenterDelegate
-(void)downloadStartWithId:(int)pid target:(id)target Key:(NSString *)key;
{
	NSLog(@"開始下載 %@",key);
}
-(void)downloadFinish:(int)pid target:(id)target Key:(NSString *)key;
{
	NSLog(@"下載完成 %@",key);
	NSString *tmpCheckPath = [Utility genTmpCheckFile:key];
	[@"OK" writeToFile:tmpCheckPath atomically:YES];
	
	
	UITableViewCell *cell = (UITableViewCell *)[updateProgressBar valueForKey:key];
	
	UIProgressView *vv = (UIProgressView *)[cell viewWithTag:Cell_ProgresslView];
	vv.hidden = YES;
	
	[storeDownObject removeObjectForKey:key];
}

//發生下載失敗，則重新發送下載
-(void)downloadFailWithId:(int)pid target:(id)target Key:(NSString *)key;
{
	NSLog(@"發生下載錯誤 重新發送下載");
	
	DownloadObject *obj = (DownloadObject *)target;
	
	[downloadCenter addFilePath:obj.url ToPath:obj.path Key:key FileType:1];
	
	
}

-(void)updateFileLength:(int)length pid:(int)pid target:(id)target Key:(NSString *)key;
{
	NSMutableDictionary *info = [storeDownObject valueForKey:key];
	[info setValue:[NSNumber numberWithInt:length] forKey:@"Length"];
	[self updateProgressBarWithKey:key];
}
-(void)updateCurrentLoadLength:(int)length pid:(int)pid target:(id)target Key:(NSString *)key;
{
	NSLog(@"Update key=%@ len=%d",key,length);
	NSMutableDictionary *info = [storeDownObject valueForKey:key];
	[info setValue:[NSNumber numberWithInt:length] forKey:@"CurrentLength"];
	[self updateProgressBarWithKey:key];
}




//與更新Bar綁定
-(void)unBindUpdateProgress:(UITableViewCell *)cellUnit;
{
	//[updateProgressBar removeObjectForKey:key];
	
	//[updateProgressBar k
	
	NSEnumerator *enumerator = [updateProgressBar keyEnumerator];
	id key;
	
	while ((key = [enumerator nextObject])) {
		/* code that uses the returned key */
		if ( [updateProgressBar valueForKey:key] == cellUnit )
		{
			break;
			
		}
		
	}
	if ( key != nil )
	{
		[updateProgressBar removeObjectForKey:key];
	}
	
}
-(void)BindUpdateProgress:(UITableViewCell *)cellUnit Key:(NSString *)key;
{
	if (![updateProgressBar valueForKey:key] )
	{
		[updateProgressBar setValue:cellUnit forKey:key];
	}
	
	
	[self updateProgressBarWithKey:key];
}

-(void)updateProgressBarWithKey:(NSString *)key;
{
	UITableViewCell *cell = (UITableViewCell *)[updateProgressBar valueForKey:key];
	
	UIProgressView *vv = (UIProgressView *)[cell viewWithTag:Cell_ProgresslView];
	
	//更新目前狀態
	NSMutableDictionary *info = [storeDownObject valueForKey:key];
	int totalLength = [[info valueForKey:@"Length"] intValue];
	int currentLength = [[info valueForKey:@"CurrentLength"] intValue];
	
	if ( totalLength > 0 )
	{
		vv.progress = currentLength / (float)totalLength;
	}
	else {
		vv.progress = 0;
	}
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
