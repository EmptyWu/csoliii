//
//  define.h
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/1/29.
//  Copyright 2011 TTU. All rights reserved.
//

#import <UIKit/UIKit.h>

//下載檔案的url
#define	kDataUrl	@"http://dl.dropbox.com/u/7213153/NewTech/data.txt" //Wei Ting
#define	kDataUrl	@"http://192.168.0.100/ipod/a1.php?a=00000000000010008000080027f3cb50" //Test


//#define	kDataUrl	@"http://dl.dropbox.com/u/12027291/data.txt"


typedef enum
{
	Device_iPhone = 50000,
	Device_iPad,
	vitt
}DeviceId;


typedef enum
{
	Cell_ImageView = 60000,
	Cell_TitleView,
	Cell_DescriptionLabelView,
	Cell_TimeStampLabelView,
	Cell_AddOnLabelView,
	Cell_ProgresslView,
	CellView
}_CellView;


typedef enum
{
	Type_Folder = 230,
	Type_Document,
	Type_Music,
	Type_Movie,
	Type_Test,
    Type_Html,
	TypeId,
    Type_Pages
}_TypeId;

//###常用常數
//多久檢查一次檔案更新
#define kSleepInterval	20
//測試網路是否存活host
#define	kTestStie	@"www.google.com"

//定義一些常用的檔名

//目前的清單
#define	kResourceFile	@"ResourceList.plist"

//目前的清單
#define	kListFileName	@"NewTechList.plist"
//下載暫存的清單
#define	kListTmpFileName	@"NewTechList_tmp.plist"