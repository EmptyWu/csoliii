//
//  DownloadCenter.m
//  DownloadCenter
//
//  Created by Chen Wei Ting on 2010/10/31.
//  Copyright 2010 TTU. All rights reserved.
//

#import "DownloadCenter.h"


@implementation DownloadCenter

@synthesize nowActiveConnect,haveRec;
@synthesize delegate;

static DownloadCenter *center;

+(DownloadCenter *)defaulCenter;
{

	if (center == nil )
	{
		center = [[DownloadCenter alloc] init];
		
	}
	
	
	return center;
}

+(void)releaseCenter;
{
	if ( center != nil )
	{
		[center release];
		center = nil;
	}
}

//初始話
-(id)init
{

	self = [self initWithMaxActive:1];
	
	return self;
	
}


-(id)initWithMaxActive:(int)_maxActive
{
	
	if ( self = [super init] )
	{
		queue = [[NSMutableArray alloc] init];
		startDownloadQueue = [[NSMutableArray alloc] init];
		maxActive = _maxActive;
		nowActiveConnect = 0;
		downloadSeries = 1;
		haveRec = 0;
		isRunning = YES;
		//[NSThread detachNewThreadSelector:@selector(startCheck) toTarget:self withObject:nil];
		[self performSelector:@selector(startCheck) withObject:nil];
		
	}
	return self;
	
	
}



#pragma mark -
#pragma mark 
-(int)amountInQueue;
{
	return [startDownloadQueue count] + [queue count];
}
-(void)startCheck
{
	//NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	//while (isRunning) {
		//NSLog(@"isRunning");
		
		if ( [queue count] > 0 && [startDownloadQueue count] <maxActive )
		{
			DownloadObject *obj = [queue objectAtIndex:0];
			[startDownloadQueue addObject:obj];
			[queue removeObject:obj];
			[obj startDownload];
			nowActiveConnect ++;
			[self performSelector:@selector(startCheck) withObject:nil afterDelay:.4f ];
			return ;
		}
	
	[self performSelector:@selector(startCheck) withObject:nil afterDelay:2.0f ];
	
		
		//[NSThread sleepForTimeInterval:10];
	//}
	
	//[pool release];
}

//在pad加入檔案資料
-(int)addFilePath:(NSURL *)url ToPath:(NSString *)path Key:(NSString *)key FileType:(int)fileType Timeout:(int)timeout autoResume:(BOOL)resume;
{
	@synchronized(self)
	{
		haveRec ++;
		DownloadObject *obj = [[DownloadObject alloc] init];
		
		obj.ser = downloadSeries;
		obj.key = key;
		obj.url = url;
		obj.path = path;
		obj.timeOut = timeout;
		obj.autoResume = resume;
		obj.fileType = fileType;
		obj.innrCenterDelegate = self;
		
		[queue addObject:obj];
		[obj release];
		
		//[obj startDownload];
		return downloadSeries++;
	}
	
}

//加入一個下載的檔案
-(int)addFilePath:(NSURL *)url ToPath:(NSString *)path Key:(NSString *)key FileType:(int)fileType;
{

	return [self addFilePath:url ToPath:path Key:key FileType:fileType Timeout:60 autoResume:YES];
			
}



//把一個下載的檔案移除
-(void)stopAndRemoveObject:(NSString *)key;
{
	//看在再下載Queue裡面
	for( DownloadObject *obj in startDownloadQueue )
	{
		if ( [obj.key isEqualToString:key] )
		{
			[obj stopDownload];
			
			[startDownloadQueue removeObject:obj];
			nowActiveConnect --;
			break;
			
		}
	}
	
	//看在再下載Queue裡面
	for( DownloadObject *obj in queue )
	{
		if ( [obj.key isEqualToString:key] )
		{
			
			[queue removeObject:obj];
			
			break;
			
		}
	}
}

#pragma mark -
#pragma mark IKDownloadDelegate
-(void)downloadStartWithId:(int)pid target:(id)target Key:(NSString *)key;
{
	//NSString *bookId = [data valueForKey:@"BookId"];
	NSLog(@"downloadStartWithId IN Center");
	[delegate downloadStartWithId:pid target:target Key:key];
}
-(void)downloadFinish:(int)pid target:(id)target Key:(NSString *)key;
{
	[startDownloadQueue removeObject:target];
	nowActiveConnect --;
	
	//if ( [delegate respondsToSelector:@selector(downloadFinish:target:Key:)] )
	//{	  
		[delegate downloadFinish:pid target:target Key:key];
	//}
}

-(void)downloadFailWithId:(int)pid target:(id)target Key:(NSString *)key;
{
	[startDownloadQueue removeObject:target];
	nowActiveConnect --;

	//if ( [delegate respondsToSelector:@selector(downloadFailWithId:target:Key:)] )
	//{
		[delegate downloadFailWithId:pid target:target Key:key];
	//}
	
}

-(void)updateFileLength:(int)length pid:(int)pid target:(id)target Key:(NSString *)key;
{
	[delegate updateFileLength:length pid:pid target:target Key:key];
}
-(void)updateCurrentLoadLength:(int)length pid:(int)pid target:(id)target Key:(NSString *)key;
{
	[delegate updateCurrentLoadLength:length pid:pid target:target Key:key];

}




-(void)dealloc
{
	NSLog(@"deallc");

	isRunning = NO;
	[startDownloadQueue release];
	[queue release];
	[super dealloc];
}

@end
