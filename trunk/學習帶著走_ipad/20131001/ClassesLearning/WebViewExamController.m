    //
//  WebViewController.m
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/2/12.
//  Copyright 2011 TTU. All rights reserved.
//

#import "WebViewExamController.h"
#import "DocumentationInteractionViewController.h"

@implementation WebViewExamController
@synthesize path,webView;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

-(void)viewWillAppear:(BOOL)animated
{

	[super viewWillAppear:YES];
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
	//[self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
	self.navigationController.navigationBar.translucent = NO;
    
    // Ｔitle新增按鈕
    if([path rangeOfString:@".ibooks"].location != NSNotFound)
    {
        UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithTitle:@"另開" style:UIBarButtonItemStyleBordered target:self action:@selector(ChangeSource:)];
        super.navigationItem.rightBarButtonItem = btn;
        [btn release];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:YES];
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
	//[self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
	self.navigationController.navigationBar.translucent = NO; 
	
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	CGRect rect = self.view.frame;
	rect.origin.x = 0;
	rect.origin.y = 0;
	
	UIWebView *vv = [[UIWebView alloc] initWithFrame:rect];
    self.webView = vv;
    vv.delegate = self;
	vv.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight ;
	[self.view addSubview:vv];
	[vv release];
	vv.userInteractionEnabled = YES;
	vv.multipleTouchEnabled = YES;
	vv.scalesPageToFit = YES;
	
	[vv loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
	
	
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *url = [request URL];
    NSLog(@"url=%@",url);
    
    return  YES;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    webView.delegate = nil;
    [webView release];
	[path release];
    [super dealloc];
}

- (void) ChangeSource:(id)sender {
    DocumentationInteractionViewController *documentInteractionController = [[DocumentationInteractionViewController alloc] init];
    documentInteractionController.path = path;
    documentInteractionController.interacting = NO;
    
    FPPopoverController *fpop = [[FPPopoverController alloc] initWithViewController:documentInteractionController];
    fpop.arrowDirection = FPPopoverArrowDirectionUp;
    [fpop presentPopoverFromBarButtonItem:sender];
}
@end
