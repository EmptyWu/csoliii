//
//  AddSourceViewController.h
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/3/15.
//  Copyright 2011 TTU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "define.h"
#import "Utility.h"

@interface AddSourceViewController : UIViewController {

	
	IBOutlet UITextField *tfName;
	IBOutlet UITextField *tfUrl;
	
}

@property(nonatomic, retain)IBOutlet UITextField *tfName;
@property(nonatomic, retain)IBOutlet UITextField *tfUrl;

-(IBAction)save;
@end
