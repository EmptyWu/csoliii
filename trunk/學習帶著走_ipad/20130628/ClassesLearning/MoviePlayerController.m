    //
//  MoviePlayerController.m
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/2/12.
//  Copyright 2011 TTU. All rights reserved.
//

#import "MoviePlayerController.h"


@implementation MoviePlayerController
@synthesize path,mp;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	MPMoviePlayerController *mpsss = [[MPMoviePlayerController alloc] initWithContentURL:
	 [NSURL fileURLWithPath:path]];
	self.mp = mpsss;
	
	mp.scalingMode = MPMovieScalingModeAspectFill;
	mp.controlStyle = MPMovieControlStyleEmbedded;
	
	CGRect rect = self.view.frame;
	rect.origin.y = 0;
	mp.view.frame = rect;
	[self.view addSubview:mp.view];
	//[mp play];
}

-(void)viewWillDisappear:(BOOL)animated
{

	[super viewWillDisappear:YES];
	[mp stop];
	
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    [mp stop];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[mp release];
	[path release];
    [super dealloc];
}


@end
