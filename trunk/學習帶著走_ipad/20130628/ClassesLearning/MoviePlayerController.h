//
//  MoviePlayerController.h
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/2/12.
//  Copyright 2011 TTU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface MoviePlayerController : UIViewController {

	NSString *path;
	MPMoviePlayerController *mp;
}
@property(nonatomic, retain)MPMoviePlayerController *mp;
@property(nonatomic, retain)NSString *path;

@end
