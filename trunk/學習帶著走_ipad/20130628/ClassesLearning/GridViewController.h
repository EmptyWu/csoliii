//
//  GridViewController.h
//  ClassesLearning
//
//  Created by Admin on 13/1/4.
//
//

#import <UIKit/UIKit.h>
@class GridViewController;

@protocol GridViewControllerDelegate <NSObject>

- (void)didSelectViewIn: (GridViewController *)controller selectedViewIndex: (int) viewIndex;

@end

@interface GridViewController : UIView
@property (nonatomic) BOOL allowRefreshWithShake;
@property (nonatomic) double animationDelayStep;
@property (nonatomic, strong) NSArray *cellSubViews;
@property (nonatomic, strong) UIColor *cellBackgroundColor;
@property (nonatomic, strong) id<GridViewControllerDelegate> delegate;

@end
