//
//  WebViewController.h
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/2/12.
//  Copyright 2011 TTU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPopoverController.h"

@interface WebViewExamController : UIViewController<UIWebViewDelegate> {

	NSString *path;
    UIWebView *webView;
}
@property(nonatomic, retain)UIWebView *webView;
@property(nonatomic, retain)NSString *path;

@end
