//
//  SourceController.m
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/3/15.
//  Copyright 2011 TTU. All rights reserved.
//

#import "SourceController.h"
#import "Utility.h"
#import "AppDelegate_iPad.h"
@implementation SourceController
@synthesize data;

#pragma mark -
#pragma mark Initialization

/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
	
	
	//load data

	
	UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithTitle:@"編輯" style:UIBarButtonItemStyleBordered target:self action:@selector(toggle:)];
	
	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     self.navigationItem.rightBarButtonItem = btn;
	
	[btn release];
}

-(void)toggle:(id)sender
{
	[self.tableView setEditing:!self.tableView.editing animated:YES];
	
	if ( self.tableView.editing )
	{
		UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithTitle:@"新增" style:UIBarButtonItemStyleBordered target:self action:@selector(addNew:)];
		self.navigationItem.leftBarButtonItem = btn;
	}
	else {
		self.navigationItem.leftBarButtonItem = nil;
	}

}

-(void)addNew:(id)sender
{
	AddSourceViewController *vc = [[AddSourceViewController alloc] initWithNibName:@"AddSourceViewController" bundle:nil];
	
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	//kListFileName :NewTechList.plist
    //kResourceFile :ResourceList.plist
	self.data = [NSMutableArray arrayWithContentsOfFile:[Utility genDocumentPath:kListFileName]];
	
	
	
	[self.tableView setEditing:YES animated:NO];
	
	[self toggle:nil];
	
	[self.tableView reloadData];
}

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [data count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 90;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		
		UILabel *lbName = [[UILabel alloc] initWithFrame:CGRectMake(30, 10, 290, 20)];
		lbName.tag = 1001;
		[cell addSubview:lbName];
		[lbName release];
		
		lbName = [[UILabel alloc] initWithFrame:CGRectMake(30, 40, 290, 40)];
		lbName.numberOfLines = 0;
		lbName.font = [UIFont systemFontOfSize:14];
		lbName.tag = 1002;
		[cell addSubview:lbName];
		[lbName release];
		
    }
    
	UILabel *lbName = (UILabel *)[cell viewWithTag:1001];
	UILabel *lbUrl = (UILabel *)[cell viewWithTag:1002];
	
    // Configure the cell...
	NSDictionary *dict = [data objectAtIndex:indexPath.row];
	
	lbName.text = [dict valueForKey:@"Name"];
	lbUrl.text = [dict valueForKey:@"URL"];
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	NSLog(@"xxxxxxxx");
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
		[data removeObjectAtIndex:indexPath.row];
		[data writeToFile:[Utility genDocumentPath:kListFileName] atomically:YES];
		
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}



/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    */
	
	// Configure the cell...
	selectIndex = indexPath.row;
	NSDictionary *dict = [data objectAtIndex:selectIndex];
	
	NSString *msg = [NSString stringWithFormat:@"你確定要切換成[%@]這個來源",[dict valueForKey:@"Name"]];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"通知" message:msg delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定",nil];
	
	[alert show];
	[alert release];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	AppDelegate_iPad *appDelegate = [[UIApplication sharedApplication] delegate];
	
	if (buttonIndex == 1 )
	{
		NSDictionary *dict = [data objectAtIndex:selectIndex];
		NSString *url = [dict valueForKey:@"URL"];
		appDelegate.urlPath = url;
		//UIApplicationDelegate *ddd = (UIApplicationDelegate*)[[UIApplication sharedApplication] delegate];
		
		
		NSString *srcP = [Utility genDocumentPath:@"ActiveUrl.txt"];
		[url writeToFile:srcP atomically:YES];
		
		
		[[NSNotificationCenter defaultCenter] postNotificationName:@"ChangeSource" object:url];
		
		
		
	}
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end

