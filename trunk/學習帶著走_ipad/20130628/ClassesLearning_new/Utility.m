//
//  Utility.m
//  DotBookV09
//
//  Created by Chen Wei Ting on 2010/11/23.
//  Copyright 2010 TTU. All rights reserved.
//

#import "Utility.h"


@implementation Utility

//清除所有subview
+(void)clearSubviewInView:(UIView *)_view;
{
	
	for(UIView *vv in _view.subviews)
	{
		
		[vv removeFromSuperview];
	}
}
//根據網址，轉成要暫存的檔名
+(NSString *)genKey:(NSURL *)_url;
{
	if ( _url == nil )
		return @"nil";
	
	if ( [_url isFileURL ] ) //如果是檔案
	{
		return @"file";
	}
	NSString *s = [_url path];
	s = [s stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
	return s;
}
//產生url檔案下載要暫存到tmp目錄的完整路徑
+(NSString *)genStroeFileInTempFolder:(NSURL *)_url;
{
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *tmpPath = [documentPaths objectAtIndex:0];
	
	tmpPath = [tmpPath stringByAppendingPathComponent:@"/tmp/"];
	
	if ( [[Utility genKey:_url] isEqualToString:@"file"] )
		return @"file";
	
	return [tmpPath stringByAppendingPathComponent:[Utility genKey:_url]];
	
}

//產生暫存判斷的檔案
+(NSString *)genTmpCheckFile:(NSString *)_file;
{
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *tmpPath = [documentPaths objectAtIndex:0];
	
	tmpPath = [tmpPath stringByAppendingPathComponent:@"/tmp/"];
	
	NSString *ff = [_file stringByAppendingString:@"_file.ok"];
	
	return [tmpPath stringByAppendingPathComponent:ff];
}

//產生暫存的目錄
+(NSString *)genTmpFolder;
{
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *tmpPath = [documentPaths objectAtIndex:0];
	
	tmpPath = [tmpPath stringByAppendingPathComponent:@"/tmp/"];
	
	//NSString *ff = [_file stringByAppendingString:@"_file.ok"];
	
	//return [tmpPath stringByAppendingPathComponent:ff];
	return tmpPath;
}

+(NSString *)getFileDir
{
	return  [[NSBundle mainBundle] resourcePath];
	//NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	//NSString *tmpPath = [documentPaths objectAtIndex:0];
	//return tmpPath;
	
}

+(NSString *)genResourcePath:(NSString *)path
{
	return  [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:path];
}

+(NSString *)genDocumentPath:(NSString *)path;
{
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *tmpPath = [documentPaths objectAtIndex:0];
	
	return [tmpPath stringByAppendingPathComponent:path];
}

+(NSString *)getFileNameFromURLString:(NSString *)url
{
	
	NSArray *parts = [url componentsSeparatedByString:@"/"];
	NSString *filename = [parts objectAtIndex:[parts count]-1];
	return filename;
}

+(NSString *)getFileNameFromURL:(NSURL *)url
{
	
	//NSString *url = @"http://www.google.com/a.pdf";
	NSArray *parts = [[url path] componentsSeparatedByString:@"/"];
	NSString *filename = [parts objectAtIndex:[parts count]-1];
	return filename;
}

//根據完整目錄取得json內容
+(NSDictionary *)fetchJSONConent:(NSString *)path;
{
	NSString *str = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
	//NSLog(@"str=%@",str);
	
	return [str JSONValue];
	
}

//取得json描述黨
+(NSDictionary *)fetchJSONFile:(NSString *)fileName;
{
	
	NSString *path =  [[self getFileDir] stringByAppendingPathComponent:fileName];
	
	return [[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil] JSONValue];
	
}

//取得檔案路徑
+(NSString *)fetchMediaPath:(NSString *)fileName;
{
	
	//NSLog(@"fetchMediaPath=%@",fileName);
	NSURL *url = [NSURL URLWithString:fileName];
	//NSString *path = [url path];
	//path = [path stringByReplacingOccurrencesOfString:@"/" withString:@""];
	NSString *path = [Utility getFileNameFromURL:url];
	
	//NSLog(@"path=%@",path);
	//[[NSFileManager defaultManager] ge
	
	return [[self getFileDir] stringByAppendingPathComponent:path];
	
}



//測試網路是否順暢
+ (BOOL) hostAvailable: (NSString *) theHost
{
	
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithName(CFAllocatorGetDefault(), [theHost UTF8String]);
	SCNetworkReachabilityFlags flags;
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	if (!didRetrieveFlags)
	{
		herror("Error. Could not recover network reachability flags\n");
		return NO;
	}
	BOOL isReachable = flags & kSCNetworkFlagsReachable;
	return isReachable ? YES : NO;
}


//取得設備的種類
+(int)getDeviceType;
{

	NSString *deviceType = [UIDevice currentDevice].model;
	if ( [deviceType isEqualToString:@"iPad"] )
	{
		return Device_iPad;
	}
	else {
		return Device_iPhone;
	}

	//NSLog(@"deviceType=%@",deviceType);
	
	
}


@end
