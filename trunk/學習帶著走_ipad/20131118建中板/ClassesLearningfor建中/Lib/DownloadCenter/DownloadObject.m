//
//  DownloadObject.m
//  DownloadCenter
//
//  Created by Chen Wei Ting on 2010/10/31.
//  Copyright 2010 TTU. All rights reserved.
//

#import "DownloadObject.h"


@implementation DownloadObject

@synthesize key,ser,timeOut,autoResume,url,path,fileType;
@synthesize innrCenterDelegate;

-(NSString *)genTmpFile:(NSString *)str;
{
	NSString *ss = [str stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
	
	return [NSTemporaryDirectory() stringByAppendingPathComponent:ss];
	
}
//開始下載
-(void)startDownload;
{
	if (connection!=nil) {
		[connection cancel];
		[connection release];
		connection = nil;
	}
    if (data!=nil) { 
		[data release]; 
		data = nil;
	}
	
	
	tmpStoreFile = [NSTemporaryDirectory() stringByAppendingFormat:@"tmp_file_%d.tmp",ser];
	[tmpStoreFile retain];
	
	if ( [[NSFileManager defaultManager] fileExistsAtPath:[self genTmpFile:path]] )
	{
		if(innrCenterDelegate != nil)
		{
			[innrCenterDelegate downloadFinish:ser target:self Key:key];	
		}
		return ;
	}
	
	//先計算 已經下載多大的檔案了
	unsigned long long rangeStart = 0;
	if ( ![[NSFileManager defaultManager] fileExistsAtPath:path] )
	{
		[[NSFileManager defaultManager] createFileAtPath:path contents:nil attributes:nil];
		zipFileSize = 0; //預設是沒有檔案大小的
		nowReadSize = 0;
	}
	else
	{
		
		NSError *error = nil;
		NSDictionary *attributes = [[NSFileManager defaultManager] 
									attributesOfItemAtPath:path error:&error];
		
		if (!error) {
			NSNumber *size = [attributes objectForKey:NSFileSize];
			rangeStart = [size unsignedLongLongValue];
			zipFileSize = rangeStart ; //把目前的檔案大小，設定為當下檔案大小
			nowReadSize = zipFileSize;
		}	
	}
	
	NSMutableURLRequest* request = nil;
	
	if ( fileType == 2 ) //透過開發 下載程式
	{
		NSString *strUrl = [url absoluteString];
		NSString *newUrl = [NSString stringWithFormat:@"%@&index=%lld",strUrl,rangeStart];
		//NSLog(@"newUrl = %@",newUrl);
		request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:newUrl]
										  cachePolicy:NSURLRequestReloadRevalidatingCacheData
									  timeoutInterval:60.0f];
		
	}
	else if (fileType == 1 )  //標準http 檔案直接連結作法
	{
		//設定續傳範圍
		NSString *rangeValue = [NSString stringWithFormat:@"bytes=%lld-",rangeStart];
		
		//NSLog(@"rangeValue=%@",rangeValue);
		
		request = [NSMutableURLRequest requestWithURL:url
															   cachePolicy:NSURLRequestReloadRevalidatingCacheData
														   timeoutInterval:60.0f];
		
		[request addValue:rangeValue forHTTPHeaderField:@"RANGE"];
	}

	
	
    connection = [[NSURLConnection alloc]
				  initWithRequest:request delegate:self startImmediately:YES];
	
	//if( connection == nil )
	//	NSLog(@"shif");
	
	if (innrCenterDelegate != nil )
	{
		[innrCenterDelegate downloadStartWithId:ser target:self  Key:key];
	}
	
}

//停止下載
-(void)stopDownload;
{
	NSLog(@"stopDownload");
	//downloadStateMachine = KDLSM_INIT;
	if ( connection != nil )
	{
		[connection cancel];
	}
}

#pragma mark -
#pragma mark URL Connection Delegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	//NSLog(@"didReceiveResponse %@",[response MIMEType]);
	
	
	zipFileSize += [response expectedContentLength]; //把預期會抓到的大小加到整個檔案大小
	//NSLog(@"zipFileSize=%lld expectedContentLength=%lld",zipFileSize,[response expectedContentLength]);
	
	if (innrCenterDelegate != nil )
	{
		[innrCenterDelegate updateFileLength:zipFileSize pid:ser target:self Key:key];
	}
	
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	NSLog(@"didFailWithError");
	
	if(innrCenterDelegate != nil)
	{
		[innrCenterDelegate downloadFailWithId:ser target:self Key:key];	
	}
}

- (void)connection:(NSURLConnection *)theConnection
	didReceiveData:(NSData *)incrementalData
{
	//NSLog(@"didReceiveData ser=%d ct=%d",ser,[incrementalData length]);
	
	if ( theConnection != connection )
		return ;
	
	NSFileHandle *handle = [NSFileHandle fileHandleForWritingAtPath:path];
	[handle seekToEndOfFile];
	[handle writeData:incrementalData];
	[handle closeFile];
	
	nowReadSize += [incrementalData length];
	
	if (innrCenterDelegate != nil )
	{
		[innrCenterDelegate updateCurrentLoadLength:nowReadSize pid:ser target:self Key:key];
	}
	
}

- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection
{
	[connection release];
    connection=nil;
	
	//[@"ok" writeToFile:[self genTmpFile:path] atomically:YES];
	
	NSLog(@"connectionDidFinishLoading");
	
	if(innrCenterDelegate != nil)
	{
		[innrCenterDelegate downloadFinish:ser target:self Key:key];	
	}
    
    //判斷需不需要解壓縮
    NSRange rg = [path rangeOfString:@".zip"];
    if ( rg.location != NSNotFound )
    {
        //開始解壓縮
        NSString *zipPath = [path stringByReplacingOccurrencesOfString:@".zip" withString:@"-zip"];
        zipPath = [zipPath stringByAppendingPathComponent:@"/"];
        [SSZipArchive unzipFileAtPath:path toDestination:zipPath];
    }
    
}


-(void)dealloc
{

	//NSLog(@"Object %d dealloc",ser);
	[key release];
	[tmpStoreFile release];
	[url release];
	[path release];
	[super dealloc];
}

@end
