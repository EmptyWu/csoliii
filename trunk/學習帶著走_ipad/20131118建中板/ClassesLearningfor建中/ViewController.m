//
//  ViewController.m
//  ClassesLearning
//
//  Created by Admin on 13/1/4.
//
//

#import "ViewController.h"
#import "GridViewController.h"

@interface ViewController () <GridViewControllerDelegate>

@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) GridViewController *gvController;

@end

@implementation ViewController

@synthesize images = _images;
@synthesize gvController = _gvController;

- (GridViewController*)gvController
{
    if (_gvController == nil)
    {
        _gvController = [[GridViewController alloc] initWithFrame:CGRectMake(10, 0, 300, 430)];
        _gvController.backgroundColor = [UIColor clearColor];
        _gvController.delegate = self;
        _gvController.cellBackgroundColor = [UIColor darkGrayColor];
        
        [self.view addSubview: _gvController];
    }
    return _gvController;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self performSelector:@selector(setLoadedImages) withObject:nil afterDelay:1.0f];
}

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/

- (void)viewDidLoad
{
    //[super viewDidLoad];
	// Do any additional setup after loading the view.
    //[super viewDidAppear:animated];
    //[self performSelector:@selector(setLoadedImages) withObject:nil afterDelay:2.0f];

    //self.navigationItem.title = @"Sliding Grid View";
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib
    
    //self.view.backgroundColor = [UIColor whiteColor];
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.jpg"]];
    
    NSMutableArray *imgs = [[NSMutableArray alloc] init];
    
    for (int i=1; i<21; i++)
    {
        NSString *imageName = [NSString stringWithFormat:@"%d", i];
        UIImage *img = [UIImage imageNamed:imageName];
        UIImageView *imgView = [[UIImageView alloc] initWithImage: img];
        [imgs addObject: imgView];
    }
    
    self.images = imgs;
    
    self.gvController.allowRefreshWithShake = YES;

}

- (void) setLoadedImages
{
    self.gvController.cellSubViews = self.images;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didSelectViewIn:(GridViewController *)controller selectedViewIndex:(int)viewIndex
{
    NSLog (@"Selected image idx: %d", viewIndex);
}

@end
