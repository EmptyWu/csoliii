//
//  FileListViewController.m
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/2/10.
//  Copyright 2011 TTU. All rights reserved.
//

#import "FileListViewController.h"
#import "BaseViewController.h"
#import "SourceController.h"

@implementation FileListViewController
@synthesize dataList;

#pragma mark -
#pragma mark Initialization

- (void)dealloc {
	[dataList release];
    [super dealloc];
}

/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


#pragma mark -
#pragma mark View lifecycle

-(void)makeEditBtn;
{
	UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithTitle:@"來源" style:UIBarButtonItemStyleBordered target:self action:@selector(ChangeSource)];
	
	
	self.navigationItem.rightBarButtonItem = btn;
	[btn release];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	//[self.tableView reloadData];
}

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
	BaseViewController *baseViewC = (BaseViewController *)self.navigationController;
	
	 
	 for(UITableViewCell *cell in [self.tableView visibleCells])
	 {
		 [baseViewC unBindUpdateProgress:cell];
	 }
}

/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return YES;
}



-(void)ChangeSource
{
	SourceController *svc = [[SourceController alloc] init];
	
	
	[self.navigationController pushViewController:svc animated:YES];
	
	[svc release];
	
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	//NSLog(@"xxxxxxxxcount=%d",[dataList count]);
    return [dataList count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
	//int type = [Utility getDeviceType];
	
	
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		
		//放icon
		UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 64, 64)];
		imageV.tag = Cell_ImageView;
		[cell addSubview:imageV];
		[imageV release];
		
		UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(84, 10, 220, 24)];
		titleLb.backgroundColor = [UIColor clearColor];
		titleLb.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
		titleLb.tag = Cell_TitleView;
		titleLb.font = [UIFont boldSystemFontOfSize:24];
		[cell addSubview:titleLb];
		[titleLb release];
		
		titleLb = [[UILabel alloc] initWithFrame:CGRectMake(84, 38, 220, 18)];
		titleLb.backgroundColor = [UIColor clearColor];
		titleLb.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
		titleLb.tag = Cell_DescriptionLabelView;
		titleLb.font = [UIFont systemFontOfSize:16];
		[cell addSubview:titleLb];
		[titleLb release];
		
		titleLb = [[UILabel alloc] initWithFrame:CGRectMake(84, 56, 140, 14)];
		titleLb.backgroundColor = [UIColor clearColor];
		titleLb.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
		titleLb.tag = Cell_TimeStampLabelView;
		titleLb.font = [UIFont systemFontOfSize:12];
		titleLb.textColor = [UIColor redColor];
		[cell addSubview:titleLb];
		[titleLb release];
		
		titleLb = [[UILabel alloc] initWithFrame:CGRectMake(260, 60, 60, 12)];
		titleLb.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
		titleLb.tag = Cell_AddOnLabelView;
		titleLb.font = [UIFont systemFontOfSize:12];
		titleLb.textColor = [UIColor blueColor];
		[cell addSubview:titleLb];
		[titleLb release];
		
		
		UIProgressView *progress = [[UIProgressView alloc] initWithFrame:CGRectMake(84, 75, 150, 10)];
		progress.tag = Cell_ProgresslView;
		[cell addSubview:progress];
		[progress release];
    }
	
	//Cancel Accessory
	cell.accessoryType = UITableViewCellAccessoryNone;
	
	BaseViewController *baseViewC = (BaseViewController *)self.navigationController;
	[baseViewC unBindUpdateProgress:cell];
	
	NSDictionary *dict = [dataList objectAtIndex:indexPath.row];
	
	NSString *Id = [dict valueForKey:@"ID"];
	NSString *Title = [dict valueForKey:@"Title"];
	
	UIProgressView *progressV = (UIProgressView *)[cell viewWithTag:Cell_ProgresslView];
    
    if ( [Id isEqualToString:@"5539c5f722b84b7971e2608a500e0fc901f66100NODATA"] ) {
        progressV.hidden = YES;
    }
    else {
        progressV.hidden = NO;
    }
    
	progressV.progress = 0;
	UIImageView *imgV = (UIImageView*)[cell viewWithTag:Cell_ImageView];
	UILabel *titleLb = (UILabel*)[cell viewWithTag:Cell_TitleView];
	UILabel *descriptionLb = (UILabel*)[cell viewWithTag:Cell_DescriptionLabelView];
	UILabel *timeStampLb = (UILabel*)[cell viewWithTag:Cell_TimeStampLabelView];
	UILabel *addOnLb = (UILabel*)[cell viewWithTag:Cell_AddOnLabelView];
	
//    Boolean result = [Id isEqualToString:ids];
//    if (result) {
//        titleLb.text = @"您目前沒有可閱覽課程";
//        descriptionLb.text = @"";
//        timeStampLb.text = @"";
//        addOnLb.text = @"";
//    }
//    else {
        titleLb.text = [dict valueForKey:@"Title"];
        descriptionLb.text = [dict valueForKey:@"Description"];
        timeStampLb.text = [dict valueForKey:@"TimeStamp"];
        addOnLb.text = [dict valueForKey:@"AddOn"];
//    }
	
	
	int typeId = [NewTechUtility getType:[dict valueForKey:@"Type"]];
	NSString *iconFile = [NewTechUtility getTypeIcon:typeId];
	
	UIImage *image = [UIImage imageNamed:iconFile];
	imgV.image = image;
	
	//判斷是不是目錄
	if ( typeId == Type_Folder )
	{
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		progressV.hidden = YES;
	}
	
	//判斷是否已經下載完成
	if ( [[NSFileManager defaultManager] fileExistsAtPath:[Utility genTmpCheckFile:Id]])
	{
		progressV.hidden = YES;
	}else
	{
		//Bind to update process
		
		[baseViewC  BindUpdateProgress:cell Key:Id];
	}

	
	
	
	//cell.textLabel.text = [dict valueForKey:@"Title"];
    
    // Configure the cell...
    
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
	 */
	
	NSDictionary *dict = [dataList objectAtIndex:indexPath.row];
	int typeId = [NewTechUtility getType:[dict valueForKey:@"Type"]];
	NSString *Id = [dict valueForKey:@"ID"];
	NSString *Title = [dict valueForKey:@"Title"];
	
    if ( [Id isEqualToString:@"5539c5f722b84b7971e2608a500e0fc901f66100NODATA"] ) {
        return;
    }
	
    //判斷是否已經下載完成
	if ( (![[NSFileManager defaultManager] fileExistsAtPath:[Utility genTmpCheckFile:Id]]) && typeId!=Type_Folder)
	{
		return ;
	}
	
	switch (typeId) {
		case Type_Folder:
		{
			FileListViewController *vc = [[FileListViewController alloc] initWithStyle:UITableViewStylePlain];
			vc.dataList = [dict valueForKey:@"Items"];
			vc.title = [dict valueForKey:@"Title"];
			[self.navigationController pushViewController:vc animated:YES];
			[vc release];
		
			break;
		}
		
		case Type_Document:
		case Type_Test:
		{
			//製作儲存的檔名
			NSString *fileName = [Utility genKey:[NSURL URLWithString:[dict valueForKey:@"Url"]]];
			fileName = [NSString stringWithFormat:@"%@-%@",[dict valueForKey:@"ID"],fileName];
			
			NSString *path = [Utility genDocumentPath:fileName];
			
			
			WebViewController *vv = [[WebViewController alloc] init];
			vv.path = path;
			[self.navigationController pushViewController:vv animated:YES];
			[vv release];
			break;
		}
		
		case Type_Movie:
		case Type_Music:
		{
			
			//製作儲存的檔名
			NSString *fileName = [Utility genKey:[NSURL URLWithString:[dict valueForKey:@"Url"]]];
			fileName = [NSString stringWithFormat:@"%@-%@",[dict valueForKey:@"ID"],fileName];
			
			NSString *path = [Utility genDocumentPath:fileName];
			
			MPMoviePlayerViewController *vv = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:path]];
			[self presentMoviePlayerViewControllerAnimated:vv];
			[vv release];
																									   
			
			/*
			FullScreenVideoPlayController *vv = [[FullScreenVideoPlayController alloc] init];
			vv.path = path;
			//[self.navigationController pushViewController:vv animated:YES];
			//[vv release];
			[self presentModalViewController:vv animated:YES];
			[vv playVideo];
			[vv release];
			 */
			break;
		}
        case Type_Html:
		{
			
			//製作儲存的檔名
			NSString *fileName = [Utility genKey:[NSURL URLWithString:[dict valueForKey:@"Url"]]];
			fileName = [NSString stringWithFormat:@"%@-%@",[dict valueForKey:@"ID"],fileName];
			fileName = [fileName stringByReplacingOccurrencesOfString:@".zip" withString:@"-zip"];
            fileName = [fileName stringByAppendingPathComponent:@"/index.html"];
			NSString *path = [Utility genDocumentPath:fileName];
			
			WebViewExamController *vv = [[WebViewExamController alloc] init];
			vv.path = path;
			[self.navigationController pushViewController:vv animated:YES];
			[vv release];
			break;
		}


		default:
			break;
	}
	
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ( [Utility getDeviceType] == Device_iPhone )
	{
		return 90;
	}
	else {
		return 100;
	}

}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


@end

