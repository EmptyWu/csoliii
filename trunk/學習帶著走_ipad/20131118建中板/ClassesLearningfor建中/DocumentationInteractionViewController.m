//
//  DocumentationInteractionViewController.m
//  ClassesLearning
//
//  Created by Admin on 13/1/7.
//
//

#import "DocumentationInteractionViewController.h"

@interface DocumentationInteractionViewController ()

@end

@implementation DocumentationInteractionViewController
@synthesize path;
@synthesize interacting;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
    UIView *rootView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
    
    self.view = rootView;
}

-(void) viewDidAppear:(BOOL)animated {
    if (interacting == NO) {
        interacting = YES;
        NSString *documentPath = path;
        NSURL *documentURL = [NSURL fileURLWithPath:documentPath isDirectory:NO];
        documentationInteractionController = [UIDocumentInteractionController interactionControllerWithURL:documentURL];
        [documentationInteractionController retain];
        documentationInteractionController.delegate = self;
        //[documentationInteractionController presentPreviewAnimated:YES];
        [documentationInteractionController presentOpenInMenuFromRect:CGRectZero inView:self.view.viewForBaselineLayout animated:YES];
    }
	[super viewDidAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return UIInterfaceOrientationIsPortrait(interfaceOrientation);
    return YES;
}

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
	return self;
}

@end
