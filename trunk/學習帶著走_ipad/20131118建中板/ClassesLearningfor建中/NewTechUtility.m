//
//  NewTechUtility.m
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/1/29.
//  Copyright 2011 TTU. All rights reserved.
//

#import "NewTechUtility.h"
#import "Utility.h"
#import "define.h"

@implementation NewTechUtility


//取得清單檔案路徑
+(NSString *)getFileListPath
{
	//抓取目前設定的url
	//NSString *url 
	//先抓目前url是多少
	
	NSString *up = [NewTechUtility getDataUrl];
	up = [up stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
	
	return [Utility genDocumentPath:up];
	//return [Utility genDocumentPath:kListFileName];
}

+(NSString *)getDataUrl;
{
	UIDevice *device = [UIDevice currentDevice];
    NSString *AfinUrl=AccountSourceFile;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filepathzccount=[NSString stringWithFormat:@"%@%@",@"/",AfinUrl];
    NSString *filePath = [documentsDirectory stringByAppendingString:filepathzccount];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableDictionary *plistDict;
    if ([fileManager fileExistsAtPath: filePath]) //檢查檔案是否存在
    {
        plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    }

    //抓取drives id
	NSString *did = [plistDict objectForKey:@"IDFV"];//[device.identifierForVendor UUIDString];// device.uniqueIdentifier;
	//NSLog(@"did=%@",did);
	NSString *finUrl = nil ;
    //取得Documents 路徑
	NSString *srcP = [Utility genDocumentPath:@"ActiveUrl.txt"];
	if ( [[NSFileManager defaultManager] fileExistsAtPath:srcP] )
	{
		finUrl = [NSString stringWithContentsOfFile:srcP];
	}
	else
	{
		//抓取陣列的第一個
		NSArray *arr = [NSMutableArray arrayWithContentsOfFile:[Utility genDocumentPath:kListFileName]];
		NSDictionary *info = [arr objectAtIndex:0];
		NSString *url = [info valueForKey:@"URL"];
		[url writeToFile:srcP atomically:YES];
		finUrl =  url;
	}

	
	if( finUrl != nil )
	{
		NSRange range = [finUrl rangeOfString:@"?"];
		if (range.location == NSNotFound )
		{
			finUrl = [NSString stringWithFormat:@"%@?id=%@",finUrl,did];
		}
		else {
			finUrl = [NSString stringWithFormat:@"%@&id=%@",finUrl,did];
		}

	}
			
	return finUrl;
	
}


//取得類別
+(int)getType:(NSString *)typeString;
{
	if ([typeString isEqualToString:@"Document"])
	{
		return Type_Document;
	}
	else if ([typeString isEqualToString:@"Folder"])
	{
		return Type_Folder;
	}
	else if ([typeString isEqualToString:@"Movie"])
	{
		return Type_Movie;
	}
	else if ([typeString isEqualToString:@"Music"])
	{
		return Type_Music;
	}
	else if ([typeString isEqualToString:@"Test"])
	{
		return Type_Test;
	}
    else if ([typeString isEqualToString:@"Html"])
	{
		return Type_Html;
	}
    else if ([typeString isEqualToString:@"pages"])
    {
        return  Type_Pages;    
    }
	else {
		return Type_Document;
	}

}

//取得類別的icon
+(NSString *)getTypeIcon:(int)typeId;
{
	switch (typeId) {
		case Type_Document:
			return @"Document.png";
			break;
		case Type_Test:
			return @"Test.png";
			break;
		case Type_Music:
			return @"Music.png";
			break;
		case Type_Folder:
			return @"Folder.png";
			break;
		case Type_Movie:
			return @"Movie.png";
			break;
        case Type_Html:
			return @"Document.png";
			break;
        case Type_Pages:
            return @"pages.png";
            break;
		default:
			break;
	}
}

@end
