//
//  FullScreenVideoPlayController.h
//  DotBookV10
//
//  Created by Chen Wei Ting on 2010/12/1.
//  Copyright 2010 TTU. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MediaPlayer/MediaPlayer.h>

@interface FullScreenVideoPlayController : UIViewController {

	NSString *path;
	int mode ;
	
	NSString *playFile;
	
	
	MPMoviePlayerController *mp;
	BOOL isPlayFinish;
}
@property(nonatomic, retain)MPMoviePlayerController *mp;
@property(nonatomic, retain)NSString *path;
@property(nonatomic, assign)int mode ;

-(void)playVideo;
@end
