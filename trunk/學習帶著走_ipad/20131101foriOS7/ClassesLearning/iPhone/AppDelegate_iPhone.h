//
//  AppDelegate_iPhone.h
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/1/29.
//  Copyright 2011 TTU. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "BaseViewController.h"
#import "Account.h"

@interface AppDelegate_iPhone : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	
	//BaseViewController *viewController;
	Account *viewAccount;
    
	NSString *urlPath;
}
@property(nonatomic, retain)NSString *urlPath;
@property (nonatomic, retain) IBOutlet UIWindow *window;
//@property(nonatomic, retain)IBOutlet BaseViewController *viewController;
@property(nonatomic, retain)IBOutlet Account *viewController;

@end

