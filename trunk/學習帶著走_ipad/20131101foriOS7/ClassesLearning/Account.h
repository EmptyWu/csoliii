//
//  Account.h
//  ClassesLearning
//
//  Created by qq on 13/10/22.
//
//

#import <UIKit/UIKit.h>
#import "define.h"
#import "Utility.h"
//#import "FileListViewController.h"
#import "IKDownloadDelegate.h"


@interface Account : UIViewController<IKDownloadDelegate>
{
    
}
@property (retain, nonatomic) IBOutlet UITextField *lblsn;
@property (retain, nonatomic) IBOutlet UITextField *lblname;
- (IBAction)push:(id)sender;
//@property(nonatomic, retain)FileListViewController *rootViewController;
@end
