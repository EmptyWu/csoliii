//
//  NewTechUtility.h
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/1/29.
//  Copyright 2011 TTU. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NewTechUtility : NSObject {

}

//取得清單檔案路徑
+(NSString *)getFileListPath;
//取得類別
+(int)getType:(NSString *)typeString;
//取得類別的icon
+(NSString *)getTypeIcon:(int)typeId;


+(NSString *)getDataUrl;
@end
