//
//  FileListViewController.h
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/2/10.
//  Copyright 2011 TTU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
#import "define.h"
#import "NewTechUtility.h"
#import "WebViewController.h"
#import "MoviePlayerController.h"
#import "FullScreenVideoPlayController.h"
#import "WebViewExamController.h"

@interface FileListViewController : UITableViewController {

	NSArray *dataList;
	
}

@property(nonatomic, retain)NSArray *dataList;
-(void)makeEditBtn;

@end
