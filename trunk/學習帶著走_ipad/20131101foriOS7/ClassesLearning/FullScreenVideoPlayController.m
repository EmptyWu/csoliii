    //
//  FullScreenVideoPlayController.m
//  DotBookV10
//
//  Created by Chen Wei Ting on 2010/12/1.
//  Copyright 2010 TTU. All rights reserved.
//

#import "FullScreenVideoPlayController.h"

@implementation FullScreenVideoPlayController
@synthesize path,mp,mode;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	
	self.view.backgroundColor = [UIColor blackColor];
	
}

-(void)playVideo
{
	self.mp =  [[MPMoviePlayerController alloc] initWithContentURL:
				[NSURL fileURLWithPath:path]];
	[mp release];
	mp.scalingMode = MPMovieScalingModeAspectFit;
	mp.controlStyle = MPMovieControlStyleFullscreen;
	[mp setFullscreen:YES];
	mp.view.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin
	|UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	
	[mp prepareToPlay];
	
	// Register that the load state changed (movie is ready)
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(moviePlayerLoadStateChanged:) 
												 name:MPMoviePlayerLoadStateDidChangeNotification 
											   object:nil];
	
	// Register that the load state changed (movie is ready)
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(moviePlayBackDidFinish:) 
												 name:MPMoviePlayerDidExitFullscreenNotification 
											   object:nil];
	
	// Register to receive a notification when the movie has finished playing. 
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(moviePlayBackDidFinish:) 
												 name:MPMoviePlayerPlaybackDidFinishNotification 
											   object:nil];
	
}


-(void)viewWillAppear:(BOOL)animated
{
	
	//[[UIApplication sharedApplication] setStatusBarHidden:YES];
	//self.wantsFullScreenLayout = YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
	//[[UIApplication sharedApplication] setStatusBarHidden:NO];
}



#pragma mark -
#pragma mark 跟影片播放狀態有關係的
/*---------------------------------------------------------------------------
 * For 3.2 and 4.x devices
 * For 3.1.x devices see moviePreloadDidFinish:
 *--------------------------------------------------------------------------*/
- (void) moviePlayerLoadStateChanged:(NSNotification*)notification 
{
	// Unless state is unknown, start playback
	if ([mp loadState] != MPMovieLoadStateUnknown)
	{
		CGRect rct;
		if ( UIDeviceOrientationIsPortrait( [[UIApplication sharedApplication] statusBarOrientation]) )
		{
			rct = CGRectMake(0, 0, 768, 1004);
			
		}
		else {
			rct = CGRectMake(0, 0, 1024, 748);
			
		}
		
		rct = self.view.frame;
		rct.origin.y = -20;
		
		isPlayFinish = NO;
		// Remove observer
		[[NSNotificationCenter 	defaultCenter] 
		 removeObserver:self
		 name:MPMoviePlayerLoadStateDidChangeNotification 
		 object:nil];
		
		//CGRect rct = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
		
		NSLog(@"main rct='%@",NSStringFromCGRect(rct));
		[mp.view setFrame:rct];
		// Add movie player as subview
		
		[self.view addSubview:mp.view];
		[mp play];
	}
}

- (void) ExitFullscreenNotification:(NSNotification*)notification 
{  
	NSLog(@"ExitFullscreenNotification");
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification 
{    
	
	NSLog(@"moviePlayBackDidFinish");
	if ( isPlayFinish )
		return;
	
		
 	// Remove observer
	[[NSNotificationCenter 	defaultCenter] 
	 removeObserver:self
	 name:MPMoviePlayerPlaybackDidFinishNotification 
	 object:nil];
	
	[[NSNotificationCenter 	defaultCenter] 
	 removeObserver:self
	 name:MPMoviePlayerDidExitFullscreenNotification 
	 object:nil];
		
	[mp stop];
	[mp.view removeFromSuperview];
	//[mp release];
	self.mp = nil;
	
	isPlayFinish = YES;
	@try {
		[self dismissModalViewControllerAnimated:NO];
	}
	@catch (NSException * e) {
		NSLog(@"moviePlayBackDidFinish NSException");
	}
	
	
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[mp release];
	[path release];
    [super dealloc];
}


@end
