//
//  Utility.h
//  DotBookV09
//
//  Created by Chen Wei Ting on 2010/11/23.
//  Copyright 2010 TTU. All rights reserved.
//

#import "define.h"
#import <netdb.h>
#import <CFNetwork/CFSocketStream.h>
#import <SystemConfiguration/SCNetworkReachability.h>

@interface Utility : NSObject {

}
+(void)clearSubviewInView:(UIView *)_view;
//根據網址，轉成要暫存的檔名
+(NSString *)genKey:(NSURL *)_url;
//產生url檔案下載要暫存到tmp目錄的完整路徑
+(NSString *)genStroeFileInTempFolder:(NSURL *)_url;

//產生暫存判斷的檔案
+(NSString *)genTmpCheckFile:(NSString *)_file;
//產生暫存的目錄
+(NSString *)genTmpFolder;

+(NSString *)getFileDir;
+(NSString *)genResourcePath:(NSString *)path;
+(NSString *)genDocumentPath:(NSString *)path;
+(NSString *)getFileNameFromURLString:(NSString *)url;
+(NSString *)getFileNameFromURL:(NSURL *)url;

//取得設備的種類
+(int)getDeviceType;



//根據完整目錄取得json內容
+(NSDictionary *)fetchJSONConent:(NSString *)path;

//取得json描述黨
+(NSDictionary *)fetchJSONFile:(NSString *)fileName;

//取得檔案路徑
+(NSString *)fetchMediaPath:(NSString *)fileName;

#pragma mark -
#pragma mark 通用型函數
+(int)TransAlignString:(NSString *)align;
//設定賬號與序號
+(BOOL)setAccount:(NSString*)name secondKind:(NSString*)sn thirdKid:(NSString*)idfv;

//測試網路是否順暢
+ (BOOL) hostAvailable: (NSString *) theHost;
@end
