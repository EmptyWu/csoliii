//
//  AddSourceViewController.m
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/3/15.
//  Copyright 2011 TTU. All rights reserved.
//

#import "AddSourceViewController.h"


@implementation AddSourceViewController
@synthesize tfName,tfUrl;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

-(IBAction)save;
{
	NSMutableArray *data = [NSMutableArray arrayWithContentsOfFile:[Utility genDocumentPath:kResourceFile]];
	
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
	[dict setValue:tfName.text forKey:@"Name"];
	[dict setValue: tfUrl.text forKey:@"URL"];
	
	[data addObject:dict];
	[dict release];
	
	[data writeToFile:[Utility genDocumentPath:kResourceFile] atomically:YES];
	
	[self.navigationController popViewControllerAnimated:YES];
	
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[tfUrl release];
	[tfName release];
    [super dealloc];
}


@end
