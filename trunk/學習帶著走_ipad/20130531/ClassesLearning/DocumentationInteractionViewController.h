//
//  DocumentationInteractionViewController.h
//  ClassesLearning
//
//  Created by Admin on 13/1/7.
//
//

#import <UIKit/UIKit.h>

@interface DocumentationInteractionViewController : UIViewController <UIDocumentInteractionControllerDelegate> {
    UIDocumentInteractionController *documentationInteractionController;
    BOOL interacting;
    NSString *path;
}
@property(nonatomic, retain)NSString *path;
@property(nonatomic)BOOL interacting;

@end
