//
//  AppDelegate_iPad.m
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/1/29.
//  Copyright 2011 TTU. All rights reserved.
//

#import "AppDelegate_iPad.h"

@implementation AppDelegate_iPad

@synthesize window;
@synthesize viewController;
@synthesize urlPath;
#pragma mark -
#pragma mark Application lifecycle
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSString *finUrl = kResourceFile ;
	//判斷儲存resource的檔案是否存在
	NSString *resDocuPath = [Utility genDocumentPath:finUrl];
	if (![[NSFileManager defaultManager] fileExistsAtPath:resDocuPath] )
	{
		//[[NSFileManager defaultManager] createDirectoryAtPath:[Utility genTmpFolder] attributes:nil];
		//Copy File
		NSString *resResPath = [Utility genResourcePath:finUrl];
		[[NSFileManager defaultManager] copyItemAtPath:resResPath toPath:resDocuPath error:nil];
		
	}
    
	
	NSMutableArray *data = [NSMutableArray arrayWithContentsOfFile:[Utility genDocumentPath:finUrl]];
	NSDictionary *dict = [data objectAtIndex:0];
	self.urlPath = [dict valueForKey:@"URL"];

    finUrl=kListFileName;
    NSString *resDocuPath_Newtech = [Utility genDocumentPath:finUrl];
	if (![[NSFileManager defaultManager] fileExistsAtPath:resDocuPath_Newtech] )
	{
		//[[NSFileManager defaultManager] createDirectoryAtPath:[Utility genTmpFolder] attributes:nil];
		//Copy File
		NSString *resResPath = [Utility genResourcePath:finUrl];
		[[NSFileManager defaultManager] copyItemAtPath:resResPath toPath:resDocuPath_Newtech error:nil];
		
	}
    
	
	NSMutableArray *data_Newtech = [NSMutableArray arrayWithContentsOfFile:[Utility genDocumentPath:finUrl]];
	NSDictionary *dict_Newtech = [data_Newtech objectAtIndex:0];
	self.urlPath = [dict_Newtech valueForKey:@"URL"];

	
	//self.urlPath = 
	
    // Override point for customization after application launch.
    //[self.window addSubview:viewController.view];
    //[self.viewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [NSThread sleepForTimeInterval:5.0];
    [self.window setRootViewController:viewController];
    [self.window makeKeyAndVisible];
    
    //Override point for customization after applicationlaunch.
    //Add the view controller’s view to the window anddisplay.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
	[urlPath release];
	[viewController release];
    [window release];
    [super dealloc];
}
/*
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    if(toInterfaceOrientation == UIInterfaceOrientationPortrait)
    {
        return YES;
    }
    else
    {
        return NO;
    }}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}*/

@end
