//
//  IKDownloadDelegate.h
//  MagV
//
//  Created by Chen Wei Ting on 2010/7/9.
//  Copyright 2010 TTU. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol IKDownloadDelegate

@optional
-(void)downloadStartWithId:(int)pid target:(id)target Key:(NSString *)key;
-(void)downloadFinish:(int)pid target:(id)target Key:(NSString *)key;
-(void)updateFileLength:(int)length pid:(int)pid target:(id)target Key:(NSString *)key;
-(void)updateCurrentLoadLength:(int)length pid:(int)pid target:(id)target Key:(NSString *)key;
-(void)downloadFailWithId:(int)pid target:(id)target Key:(NSString *)key;

@end
