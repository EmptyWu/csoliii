//
//  WebViewController.h
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/2/12.
//  Copyright 2011 TTU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPopoverController.h"
//#import "PopTableViewController.h"


@interface WebViewController : UIViewController {

	NSString *path;
    BOOL isPad;
}
@property(nonatomic, retain)NSString *path;
@property(nonatomic)BOOL isPad;
@end
