//
//  SourceController.h
//  ClassesLearning
//
//  Created by Chen Wei Ting on 2011/3/15.
//  Copyright 2011 TTU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddSourceViewController.h"

@interface SourceController : UITableViewController<UIAlertViewDelegate> {

	NSMutableArray *data;

	int selectIndex;
	
}
@property(nonatomic, retain)NSMutableArray *data;

@end
