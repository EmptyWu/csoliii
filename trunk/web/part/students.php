﻿<div class="student-wrapper">
	<p id="students" class="title"></p>
	<div id="students_view">
		<div id="students_management" class="controller">
			<form id="students_management_form" method="post" action="server/students.php" enctype="application/x-www-form-urlencoded" onsubmit="return false">
				<table>
					<tr>
						<td><label for="students_add_sn">電子書包序號(短)：</label></td>
						<td><input type="text" id="students_add_sn" name="sn" class="textfield" /></td>
						<td><label for="students_add_id">電子書包序號(長)：</label></td>
						<td><input type="text" id="students_add_id" name="id" class="textfield" style="width:20em" /></td>
						<td><label for="students_add_name">姓名：</label></td>
						<td><input type="text" id="students_add_name" name="name" class="textfield" /></td>
						<td><input type="button" value="新增" id="students_add_button"/><input type="button" value="查詢" id="students_list_button"/></td>
						<td><span id="students_message" class="message"></span></td>
					</tr>
				</table>
			</form>
		</div>
		<div id="students_grid" class="gridview"></div>
		<div style="display:none">
			<form id="students_delete_form" method="post" action="server/students.php" enctype="application/x-www-form-urlencoded" onsubmit="return false"><input type="hidden" name="act" value="delete"/><input type="hidden" name="id"/></form>
		</div>
	</div>
</div>
<div class="subforms">
	<div class="subform" id="students_detail" style="display:none">
		<div class="title">詳細資料</div>
		<div class="controller"></div>
		<div class="toolbar"><input type="button" value="關閉" onclick="Overlay.hide(this.up('.subform').hide())"/></div>
	</div>
	<div class="subform" id="students_modify" style="display:none">
		<div class="title">修改學生資料</div>
		<div class="controller"></div>
		<div class="toolbar"><input type="button" value="確定" onclick="$('students').doModify(this)"/><input type="button" value="取消" onclick="Overlay.hide(this.up('.subform').hide())"/></div>
	</div>
	<div class="subform" id="students_enhance" style="display:none">
		<div class="title">加強建議</div>
		<div class="controller"></div>
		<div class="toolbar"><input type="button" value="完成" onclick="Overlay.hide(this.up('.subform').hide())"/></div>
	</div>
</div>
<script type="text/javascript">
	var methods = {
		message:function(msg,delay,duration) {
			$("students_message").update(msg).show().highlight();
			if(delay) {
				(function() {$("students_message").fade({"duration":duration||0.5});}).delay(delay);
			}
		},
		async:function(act,callback,showmsg) {
			var valid = true;
			$("students_management_form").getInputs().each(function(element) {
				if(element.value.strip() == "") {
					if(valid) {
						element.activate();
						if(showmsg) $("students").message("請輸入" + $$("label[for='" + element.id + "']")[0].innerHTML.replace("：", ""),2);
					}
					valid = false;
				}
			});
			if(act == "add" && !valid) return;
			$("students_management_form").request({
				parameters:"act=" + act,
				onSuccess:function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					if(Object.isFunction(callback)) callback(transport.responseText);
				}
			});
		},
		add:function(callback,showmsg) {
			$("students").async("add",function(responseText) {
				if(Object.isFunction(callback)) callback(responseText);			
			},showmsg);
		},
		list:function(callback,showmsg) {
			$("students").async("list",function(responseText) {
				var table = responseText.evalJSON();
				var actionTemplate ="";// "<input type=\"button\" value=\"加強建議\" onclick=\"$('students').enhance('#{id}')\" />";
				actionTemplate += "<input type=\"button\" value=\"詳細資料\" onclick=\"$('students').detail('#{id}');\" />";
				actionTemplate += "<input type=\"button\" value=\"修改\" onclick=\"$('students').modify('#{id}');\" />";
				actionTemplate += "<input type=\"button\" value=\"刪除\" onclick=\"if(confirm('確定要刪除學生!?'))$('students').del('#{id}');\"/>";
				
				var html = "<table>";
				html += "<tr><th>電子書包序號</th><th>姓名</th><th>修改時間</th><th>修改人員</th><th>停權</th><th>動作</th></tr>";
				if(table.length == 0) html += "<tr><td colspan=\"7\">目前還沒建立學生資料...</td></tr>";
				for(var i=0;i<table.length;i++) {
					var row = table[i];
					html += "<tr>";
					html += "<td>" + row["SN"] + "<br/>" + row["ID"] + "</td>";
					html += "<td>" + row["Name"] + "</td>";
					html += "<td>" + row["ModifyDate"] + "</td>";
					html += "<td>" + row["ModifyBy"] + "</td>";
					html += "<td>" + ((row["Removed"] == "0") ? "否" : "是") + "</td>";
					html += "<td>" + new Template(actionTemplate).evaluate({"id":row["ID"],"iPod_Register":row["iPod_Register"]}) + "</td>";
					html += "</tr>";
				}
				html += "</table>";
				$("students_grid").update(html);
				if(Object.isFunction(callback)) callback(responseText);
			},showmsg);
		},
		enhance:function(id) {
			new Ajax.Request("fake/diagnose.html", {
				onSuccess: function(transport) {
					$$("#students_enhance .controller").first().update(transport.responseText);
					TableKit.Sortable.init($$("#students_enhance table").first());
					new Overlay().show($$("#students_enhance").first().show(), {});
				}
			});
		},
		detail:function(id) {
			new Ajax.Request("server/students.php", {
				parameters:"act=list_by_id&id="+id,
				onSuccess:function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					var dataset = transport.responseText.evalJSON();
					var html = "電子書包序號： #{SN} / #{ID}<br/>";
					html += "姓名： #{Name}<br/>";
					html += "學校： #{School}<br/>";
					html += "班級： #{Class}<br/>";
					html += "座號： #{Seat}<br/>";
					html += "電話： #{Mobile}<br/>";
					html += "生日： #{Birth}<br/>";
					html += "停權： #{Removed}<br/>";
					html += "修改時間： #{ModifyDate}<br/>";
					html += "修改人員： #{ModifyBy}<br/>";
					
					var groupNames = [];
					for(var i=0;i<dataset[1].length;i++) {
						if(dataset[1][i]['Checked'] == 'checked') {
							groupNames.push(dataset[1][i]['Name']);
						}
					}
					if(groupNames.length == 0) 
						html += "屬於班級(分類)： <span class=\"highlight\">(無)</span>";
					else
						html += "屬於班級(分類)： " + groupNames.join(', ').escapeHTML();
					
					$$("#students_detail .controller")[0].update(new Template(html).evaluate(dataset[0][0]));
					new Overlay().show($$("#students_detail").first().show());
					//modalContainer("students_detail");
				}
			});
		},
		modify:function(id) {
			new Ajax.Request("server/students.php", {
				parameters:"act=list_by_id&id="+id,
				onSuccess:function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					var dataset = transport.responseText.evalJSON();
					var table = dataset[0];
					var group_html = "";
					dataset[1].each(function(row) {
						group_html += new Template("<label style=\"text-align:left\"><input type=\"checkbox\" name=\"groups[]\" value=\"#{ID}\" #{Checked} />#{Name}</label>").evaluate(row);
					});
					var html = "<form method=\"post\" action=\"server/students.php\" enctype=\"application/x-www-form-urlencoded\" onsubmit=\"return false\">";
					html += "<input type=\"hidden\" name=\"act\" value=\"mod\"/><input type=\"hidden\" name=\"id\" value=\"#{ID}\"/>";
					html += "<table>";
					html += "<tr><th style='width:80px'>電子書包序號</th><td style='width:200px'><input type=\"text\" name=\"sn\" value=\"#{SN}\" /></td><th style='width:80px'>姓名</th><td style='width:200px'><input type=\"text\" name=\"name\" value=\"#{Name}\" /></td></tr>";
					html += "<tr><th>學校</th><td><input type=\"text\" name=\"school\" value=\"#{School}\" /></td><th>班級</th><td><input type=\"text\" name=\"class\" value=\"#{Class}\" /></td></tr>";
					html += "<tr><th>座號</th><td><input type=\"text\" name=\"seat\" value=\"#{Seat}\" /></td><th>電話</th><td><input type=\"text\" name=\"mobile\" value=\"#{Mobile}\" /></td></tr>";
					html += "<tr><th>生日</th><td><input type=\"text\" name=\"birth\" value=\"#{Birth}\" /></td>";
					if(table[0]["Removed"] == "0") {
						html += "<th>停權</th><td><select name=\"removed\"><option value=\"1\">是</option><option value=\"0\" selected=\"selected\">否</option></select></td>";
					}
					else {
						html += "<th>停權</th><td><select name=\"removed\"><option value=\"1\" selected=\"selected\">是</option><option value=\"0\">否</option></select></td>";
					}
					html += "</tr>";
					if(group_html == "") group_html = "<span class=\"highlight\">(無)</span>";
					html += "<tr><th style=\"vertical-align:top\">班級(分類)</th><td>" + group_html + "</td><th>&nbsp;</th><td>&nbsp;</td></tr>";
					html += "</table></form>";
					
					$$("#students_modify .controller")[0].update(new Template(html).evaluate(table[0]));
					new Overlay().show($$("#students_modify").first().show());
				}
			});
		},
		doModify:function(holder) {
			$$("#students_modify form")[0].request({
				onSuccess:(function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					$("students").list();
					Overlay.hide(this.up('.subform').hide());
					$("students").message("修改完成...", 2);
				}).bind(holder)
			});
		},
		del:function(id,callback) {
			$$('#students_delete_form input[name=id]')[0].value = id;
			$("students_delete_form").request({
				onSuccess:function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					$("students_delete_form").reset();
					$("students").list();
					if(Object.isFunction(callback)) callback(responseText);
					$("students").message("刪除完成...", 2);
				}
			});
		}
	};
	Object.extend($("students"), methods);
	
	$("students_add_button").observe("click", function() {
		$("students").add(function(responseText) {
			if(responseText.indexOf("dup_sno_name") > -1) {
				$("students").message("編號及姓名重覆了...", 2);
				return;
			}
			if(responseText.indexOf("dup_sid") > -1) {
				$("students").message("新增失敗...", 2);
				return;
			}
			$("students").list(function(responseText){
				$("students").message("新增完成...", 2);
				$("students_management_form").focusFirstElement().reset();
			});
		},true);
	});
	
	$("students_list_button").observe("click", function() {
		$("students").list(function(responseText) {
			$("students").message("查詢完成...", 2);
			$("students_management_form").focusFirstElement().reset();
		});
	});
	
	$("students_management_form").getInputs().invoke("observe", "keydown", function(event) {
		event = event || window.event;
		if(event.keyCode == Event.KEY_RETURN) {
			$("students_list_button").click();
		}
	});

	loadCss("css/students.css");
	$("students").list();
	$("students_management_form").focusFirstElement();
</script>
