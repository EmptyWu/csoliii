﻿<div class="file-wrapper">
	<p class="title"></p>
	<div id="files_controller" class="controller"><input id="files_add_button" type="button" value="新增檔案"/><input id="files_filter_button" type="button" value="篩選檔案"/><span id="files_message" class="highlight" style="margin:0 1em"></span></div>
	<div id="files_view" class="gridview"></div>
</div>
<div class="subforms">
	<div class="subform" id="files_add_view" style="display:none">
		<div class="title" style="padding: .2em 1em; background-color:#33a;color:#fff;font-size:18px;border-bottom:solid 1px #333">新增檔案</div>
		<div class="controller" style="padding: 1em"></div>
		<div class="toolbar"><input type="button" value="完成" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.getViewLayout()"/></div>
	</div>
	<div class="subform" id="files_filter_view" style="display:none">
		<div class="title" style="padding: .2em 1em; background-color:#33a;color:#fff;font-size:18px;border-bottom:solid 1px #333">篩選檔案</div>
		<div class="controller" style="padding: 1em">
			<div>檔案名稱：<input type="text" name="title"/></div>
			<div>檔案描述：<input type="text" name="description"/></div>
		</div>
		<div class="toolbar"><input type="button" value="完成" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.setFilter()"/><input type="button" value="清除" onclick="Overlay.hide(this.up('.subform').hide());CurrentController.clearFilter()"/></div>
	</div>
</div>
<div class="upload_template" style="display:none">
	<div class="upload">
		<div class="view view_ie">
			<div class="action control">
				<form action="upload/uploadFile.php" method="post" enctype="multipart/form-data" name="upload" target="#{id}">
					<input type="hidden" name="APC_UPLOAD_PROGRESS" value="" />
					<table><tr><td><input type="file" name="file" style="width:55px;height:20px"/></td><td><span class="filename"></span></td><td><input type="submit" value="上傳" class="submit" style="height:20px"/></td></tr></table>
				</form>
				<iframe id="#{id}" name="#{id}"></iframe>
			</div>
			<div class="message" style="display:none"><span class="progressbar"><span class="progress"></span></span><span class="filename"></span>&nbsp;<span class="status">上傳中，請稍候...</span></div>
		</div>
		<div class="view view_else">
			<div class="action"><a href="#" class="button browse">瀏覽檔案...</a></div>
			<div class="message" style="display:none"><span class="progressbar"><span class="progress"></span></span><span class="filename"></span>&nbsp;<span class="status">上傳中，請稍候...</span></div>
			<div class="control">
				<form action="upload/uploadFile.php" method="post" enctype="multipart/form-data" name="upload" target="#{id}">
					<input type="hidden" name="APC_UPLOAD_PROGRESS" value="" /><input type="file" name="file"/><input type="submit" value="上傳"/>
				</form>
				<iframe id="#{id}" name="#{id}"></iframe>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var FileController = Class.create({
		initialize: function(view, addView, messageView, addButton, filterView, filterButton) {
			this.view = $(view);
			this.addView = $(addView);
			this.addButton = $(addButton);
			this.messageView = $(messageView);
			this.filterView = $(filterView);
			this.filterButton = $(filterButton);
			
			this.filter = {title: "", description: ""};
			this.retry = 0;
			this.addButton.observe("click", this.add_OnClick);
			this.filterButton.observe("click", this.filter_OnClick);
			this.getViewLayout(this.filter);
		},
		getViewLayout: function(filter) {
			filter = filter || CurrentController.filter;
			showLoading();
			new Ajax.Request("server/files.php?act=list", {
				parameters: filter,
				onSuccess: function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					CurrentController.retry = 0;
					var table = transport.responseText.evalJSON();
					CurrentController.organizeViewLayout(table);
					//CurrentController.view.previous(".title").update("管理檔案(" + table.length + ")");
					hideLoading();
				},
				onFailure: function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					if($(CurrentController.view.identify()) && CurrentController.retry < 3) {
						CurrentController.showMessage("更新資料失敗，等待重試中...");
						(function() {CurrentController.getViewLayout()}).delay(5);
						CurrentController.retry++;
					}
					if(CurrentController.retry >= 3) {
						CurrentController.showMessage("更新資料失敗，請稍候再試...");
					}
				}
			});
		},
		organizeViewLayout: function(table) {
			var html = "<table>";
			html += "<tr><th>檔案名稱</th><th>檔案描述</th><th>建立時間</th><th>檔案大小</th><th>動作</th></tr>";
			if(table.length == 0) html += "<tr><td colspan=\"5\">目前還沒建立課程...</td></tr>";
			for(var i=0;i<table.length;i++) {
				table[i]["SizeView"] = Utility.getSize(table[i]['Size']);
				var tmp = "<tr>";
				tmp += "<td><a alt=\"點一下可修改檔名...\" title=\"點一下可修改檔名...\" onclick=\"CurrentController.showModifyName(this);return false\" href=\"#\">#{Title}</a><input type=\"hidden\" value=\"#{ID}\"/><input type=\"text\" style=\"display:none;width:90%\" onblur=\"CurrentController.modifyName(this)\"/></td>";
				tmp += "<td><a alt=\"點一下可修改描述...\" title=\"點一下可修改描述...\" onclick=\"CurrentController.showModifyDesc(this);return false\" href=\"#\">#{Description}</a><input type=\"hidden\" value=\"#{ID}\"/><input type=\"text\" style=\"display:none;width:90%\" onblur=\"CurrentController.modifyDesc(this)\"/></td>";
				tmp += "<td>#{CreateDate}</td>";
				tmp += "<td>#{SizeView}</td>";
				tmp += "<td><input class=\"files_remove\" type=\"button\" value=\"移除檔案\"/><input type=\"hidden\" value=\"#{ID}\"/></td>";
				tmp += "</tr>";
				html += new Template(tmp).evaluate(table[i]);
			}
			html += "</table>";
			this.view.update(html);
			this.view.select(".files_remove").invoke("observe", "click", this.remove_OnClick);
		},
		getAddFileLayout: function() {
			this.appendAddFileControl();
			if(!this.addView.visible()) new Overlay().show(this.addView.show());
		},
		appendAddFileControl: function() {
			if(!$$("#files_add_view .upload .filename").any(function(element) {return element.innerHTML == ""})) {
				var id = guid();
				var html = $$(".upload_template").first().innerHTML.interpolate({"id": id});
				var control = new Element("div").update(html);

				var fail_count = 0;
				var total_count = 0;
				function getProgress() {
					new Ajax.Request("upload/getprogress.php?id=" + id, {
						onSuccess: function(transport) {
							try {
								total_count ++;
								var result = transport.responseText;
								if(parseInt(result, 10))
									control.down(".progress").setStyle("width:" + result + "%");
								else
									fail_count ++;
								if(control.down(".status").innerHTML.indexOf("完成") == -1 && fail_count < 10 && total_count < 50) getProgress.delay(0.5);
							} catch(e) { if(typeof console == "object") console.log(e) }
						}
					});
				}

				if(Prototype.Browser.IE) {
					control.select(".view").without(control.down(".view_ie")).invoke("remove");
					control.down("input[type='file']").observe("change", (function(event) {
						if(Event.element(event).value == "") {
							control.select(".view .filename").invoke("update");
						}
						else {
							control.select(".view .filename").invoke("update", Event.element(event).value.split(/\\/g).last());
							control.down(".submit").show();
							control.down("form").observe("submit", (function(event) {
								Event.element(event).up(".upload").down(".action").hide();
								Event.element(event).up(".upload").down(".message").show();
								getProgress.delay(0.5);
								this.appendAddFileControl();
							}).bind(this));
						}
					}).bind(this));
					control.down(".control").toggleClassName("control");
				}
				else {
					control.select(".view").without(control.down(".view_else")).invoke("remove");
					control.down(".browse").observe("click", function(event) { Event.element(event).up(".upload").down("input[type='file']").click() });
					control.down("input[type='file']").observe("change", (function(event) {
						if(Event.element(event).value == "") {
							control.down(".view .action").show();
							control.down(".view .filename").update();
							control.down(".view .message").hide();
						}
						else {
							control.down(".view .action").hide();
							control.down(".view .filename").update(Event.element(event).value.split(/\\/g).last());
							control.down(".view .message").show();
							control.down("form").submit();
							getProgress.delay(0.5);
							this.appendAddFileControl();
						}
					}).bind(this));
				}
				
				control.down("iframe").observe("load", function(event) {
					try {
						var content = Event.element(event).contentWindow.document.body.innerHTML;
						if(content.indexOf("!!success!!") > -1) {
							control.down(".status").update("上傳完成!!");
							control.down(".progress").setStyle("width:100%");
						}
						else if(content.indexOf("!!badtype!!") > -1) {
							control.down(".status").update("格式不正確!!");
							control.down(".progress").setStyle("width:100%");
						}
						else if(content.indexOf("!!failed!!") > -1) {
							control.down(".status").update("上傳失敗，請重新上傳!!");
							control.down(".progress").setStyle("width:100%");
						}
						this.appendAddFileControl();
					} catch(e) { if(typeof console == "object") console.log(e) }
				}.bind(this));

				control.down("form").writeAttribute("action", control.down("form").readAttribute("action") + "?id=" + id);
				control.down("input[name='APC_UPLOAD_PROGRESS']").writeAttribute("value", id);
				this.addView.down(".controller").insert(control);
				this.addView.select(".button").invoke("observe", "click", function(event) { Event.stop(event) });
			}
			
			/*
			if(Prototype.Browser.Gecko) {
				var html = "<div class=\"upload_control\" style=\"margin-bottom:.5em\" id=\"upload_control_#{id}\">";
				html += "<form action=\"upload/uploadFile.php\" method=\"post\" enctype=\"multipart/form-data\" name=\"upload\" target=\"files_iframe_#{id}\">";
				html += "<input type=\"hidden\" name=\"APC_UPLOAD_PROGRESS\" value=\"#{id}\" />";
				html += "<a href=\"#\" style=\"color: black\" onclick=\"return false\" style=\"display:block\">瀏覽檔案...</a>";
				html += "<input type=\"file\" style=\"position:absolute;opacity:0\" class=\"real_file\" name=\"file\" ";
				html += "onmouseover=\"this.previous().setStyle('text-decoration:underline;color:#66f')\" ";
				html += "onmouseout=\"this.previous().setStyle('text-decoration:none;color:#666')\" />";
				html += "</form>";
				html += "<iframe style=\"display: none\" id=\"files_iframe_#{id}\" name=\"files_iframe_#{id}\"></iframe>";
				html += "<div class=\"progressmsg\"></div><div class=\"progressbar\" style=\"display:none;width:90%;height:20px;border:solid 1px black\" id=\"progressbar_#{id}\"><div class=\"progress\" style=\"width:0;height:24px;background-color:#33f\" id=\"progress_#{id}\"></div></div>";
				html += "</div>";
				html = new Template(html).evaluate({"id": id});
				this.addView.select(".controller")[0].insert(html);
				
				(function() {
					var upload_control = $("upload_control_" + id);
					var real_file = upload_control.select(".real_file")[0];
					var anchor = upload_control.select("a")[0];
					var width = real_file.getWidth() + "px";
					var height = real_file.getHeight() + "px";
					anchor.setStyle({"width": width, "height": height});
					real_file.clonePosition(anchor);
					real_file.observe("change", CurrentController.uploadFile_OnChange);
					anchor = null;
					real_file = null;
					upload_control = null;
				}).delay(1);
			}
			else {
				var html = "<div class=\"upload_control\" style=\"margin-bottom:.5em\" id=\"upload_control_#{id}\">";
				html += "<form action=\"upload/uploadFile.php\" method=\"post\" enctype=\"multipart/form-data\" name=\"upload\" target=\"files_iframe_#{id}\">";
				html += "<input type=\"hidden\" name=\"APC_UPLOAD_PROGRESS\" value=\"#{id}\" />";
				html += "<a href=\"#\" style=\"color: black\" onclick=\"$(this).next().click();return false\">瀏覽檔案...</a>";
				html += "<input type=\"file\" style=\"position:absolute;left:-9999px;filter:alpha(opacity=0);opacity:0\" class=\"real_file\" name=\"file\" />";
				html += "</form>";
				html += "<iframe style=\"display: none\" id=\"files_iframe_#{id}\" name=\"files_iframe_#{id}\"></iframe>";
				html += "<div class=\"progressmsg\"></div><div class=\"progressbar\" style=\"display:none;width:90%;height:24px;border:solid 1px black\" id=\"progressbar_#{id}\"><div class=\"progress\" style=\"width:0;height:24px;background-color:#33f\" id=\"progress_#{id}\"></div></div>";
				html += "</div>";
				html = new Template(html).evaluate({"id": id});
				this.addView.select(".controller")[0].insert(html);
				$("upload_control_" + id).select(".real_file")[0].observe("change", this.uploadFile_OnChange);
			}
			*/
		},
		showMessage: function(message) {
			CurrentController.messageView.update(message);
			(function() {CurrentController.messageView.update();view=null}).delay(3);
		},
		add_OnClick: function() {
			CurrentController.getAddFileLayout();
		},
		filter_OnClick: function() {
			new Overlay().show($$("#files_filter_view").first().show());
		},
		remove_OnClick: function() {
			if(confirm('確定要刪除課程!?')) {
				new Ajax.Request("server/files.php?act=del", {
					parameters: "id=" + this.next().value,
					onSuccess:function(transport) {
						if(transport.responseText.indexOf("<script>") > -1) {
							transport.responseText.evalScripts();
						}
						CurrentController.showMessage("刪除完成...");
						CurrentController.getViewLayout();
					}
				});
			}
		},
		showModifyName: function(element) {
			var text = element.innerHTML;
			element.hide().next("input[type='text']").show().activate().value = text;
			element = null;
		},
		showModifyDesc: function(element) {
			var text = element.innerHTML;
			element.hide().next("input[type='text']").show().activate().value = text;
			element = null;
		},
		modifyName: function(element) {
			var id = element.previous().value;
			var text = element.value;
			new Ajax.Request("server/files.php?act=modifyName", {
				parameters:{"id":id, "text":text},
				onComplete: function() {
					CurrentController.getViewLayout();
				}
			});
		},
		modifyDesc: function(element) {
			var id = element.previous().value;
			var text = element.value;
			new Ajax.Request("server/files.php?act=modifyDesc", {
				parameters:{"id":id, "text":text},
				onComplete: function() {
					CurrentController.getViewLayout();
				}
			});
		},
		setFilter: function() {
			CurrentController.filter.title = CurrentController.filterView.down("input[name='title']").value;
			CurrentController.filter.description = CurrentController.filterView.down("input[name='description']").value;
			if(CurrentController.filter.title == "" && CurrentController.filter.description == "")
				CurrentController.filterButton.setStyle("color:black");
			else
				CurrentController.filterButton.setStyle("color:blue");				
			CurrentController.getViewLayout();
		},
		clearFilter: function() {
			CurrentController.filterView.down("input[name='title']").value = "";
			CurrentController.filterView.down("input[name='description']").value = "";
			CurrentController.setFilter();
		},
		uploadFile_OnChange: function() {
			var form = this.up("form");
			var upload_control = this.up(".upload_control");
			var progressmsg = upload_control.down(".progressmsg");
			var progressbar = upload_control.down(".progressbar");

			if(Prototype.Browser.IE) form.click();
			else form.submit();
			form.hide();
			progressmsg.update("正在上傳 " + Utility.getFilename(this.value));
			progressbar.show();
			
			CurrentController.monitorUploadProgress(this.previous("input[name='APC_UPLOAD_PROGRESS']").value);
			CurrentController.appendAddFileControl();
			
			form = null;
			upload_control = null;
			progressmsg = null;
			progressbar = null;
		},
		monitorUploadProgress: function(id) {
			new Ajax.Request("upload/getprogress.php?uid=" + id, {
				method:"get",
				onSuccess:function(transport) {
					if(transport.responseText.indexOf("<script>") > -1) {
						transport.responseText.evalScripts();
					}
					var percent = parseFloat(transport.responseText);
					$("progress_"+id).setStyle({width:percent.toString()+"%"});
					if(percent < 100)
						CurrentController.monitorUploadProgress.delay(0.5, id);
					else {
						try {
							var upload_response = $("files_iframe_" + id).contentWindow.document.body.innerHTML;
							if(upload_response.indexOf('!!badtype!!') > -1) alert("您上傳的檔案格式不正確...");
							if(upload_response.indexOf('!!failed!!') > -1) alert("上傳失敗，請再試一次...");
							if(upload_response.indexOf('!!badtype!!') > -1 || upload_response.indexOf('!!failed!!') > -1) {
								$("upload_control_"+id).remove();
								return;
							}
							var filename = Utility.getFilenameWithoutExt($("progressbar_"+id).up().down("input[type='file']").value);
							$("progressbar_"+id).hide().previous().update("檔案<span class=\"highlight\">" + filename + "</span>新增完成...");
						}catch(e){CurrentController.monitorUploadProgress.delay(0.5, id)}
					}
				}
			})
		}
	});
	
	window.CurrentController = new FileController("files_view", "files_add_view", "files_message", "files_add_button", "files_filter_view", "files_filter_button");
</script>
