﻿<p class="title">匯入資料</p>
<div id="import_view">
	<div id="import_add" class="controller">
		<form id="import_form" method="post" action="server/import.php" enctype="multipart/form-data" onsubmit="return false" target="import_frame">
			<table>
				<tr>
					<td><label>Students</label></td>
					<td><input type="file" name="Students"/></td>
				</tr>
				<tr>
					<td><label>Groups</label></td>
					<td><input type="file" name="Groups"/></td>
				</tr>
				<tr>
					<td><label>StudentGroups</label></td>
					<td><input type="file" name="StudentGroups"/></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:center"><input type="submit" id="import_submit" value="開始匯入"/></td>
				</tr>
			</table>
		</form>
		<p style="color:#cc3333">注意：這個動作會重設系統所有學生相關資料喔...</p>
		<p><a href="DatabaseConverter.zip">▼下載資料庫自動轉出程式</a></p>
	</div>
	<iframe id="import_frame" name="import_frame" style="display:none"></iframe>
</div>
<script type="text/javascript">
	$("import_submit").observe("click", function() {
		var inputs = $("import_form").getInputs();
		for(var i=0;i<inputs.length;i++) {
			if(inputs[i].value.indexOf(inputs[i].name) == -1) {
				alert("檔案可能選錯了喔...");
				return;
			}
		}
		if(confirm("您確定要匯入資料嗎?\n這個動作會重設系統所有學生相關資料喔...")) {
			$("import_form").submit();
		}
	});
	
	loadCss("css/import.css");
	$("import_form").focusFirstElement();
</script>