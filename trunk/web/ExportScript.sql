[Students]
SELECT SUBSTRING(sys.fn_VarBinToHexStr(HashBytes('MD5', si.ID + si.Name)), 3, 32) AS ID, 
	'' AS SID, si.ID AS SNo, s.Name AS School, si.SchoolClass AS Class,	'' AS Seat,
	si.Name AS Name, si.Mobile AS Mobile, CONVERT(varchar(256), si.BirthDay, 23) AS Birth,
	CONVERT(varchar(256), GETDATE(), 121) AS CreateDate, CONVERT(varchar(256), GETDATE(), 121) AS ModifyDate, 
	'0000-00-00' AS DUEDATE, 'System' AS CreateBy, 'System' AS ModifyBy, 0 AS Removed
FROM StuInfo si
INNER JOIN School s
ON si.HighSch = s.ID

[Groups]
SELECT sc.ID AS ID, CAST(sc.ID AS VARCHAR(100)) + '_' + sc.Name AS Name, CONVERT(varchar(256), GETDATE(), 121) AS CreateDate, 'System' AS CreateBy 
FROM Class c
INNER JOIN SubClass sc
ON c.ID = sc.ClassID

[StudentGroups]
SELECT SUBSTRING(sys.fn_VarBinToHexStr(HashBytes('MD5', si.ID + si.Name)), 3, 32) AS StudentID,
	sc.SubClassID AS GroupID, CONVERT(varchar(256), GETDATE(), 121) AS CreateDate, 'System' AS CreateBy 
FROM StuInfo si
INNER JOIN StuClass sc
ON si.ID = sc.StuID
