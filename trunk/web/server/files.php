﻿<?php
	if($_SESSION['username'] == "") die("<script>location.href='login.html';</script>");
	$doc_root = $_SERVER['DOCUMENT_ROOT'];
	require_once($doc_root.'/includes/file_io.php');
	require_once($doc_root.'/includes/mysql_tools.php');
	
	switch($_GET['act']) {
		case 'list':
			$conn = mysql_GetConnection();
			$title = str_replace('*', '%', mysql_real_escape_string($_POST['title'], $conn));
			$description = str_replace('*', '%', mysql_real_escape_string($_POST['description'], $conn));
			$username = $_SESSION['username'];
			$sql = "SELECT ID, CASE WHEN Title = '' THEN '(空)' ELSE Title END AS Title,
					CASE WHEN Description = '' THEN '(空)' ELSE Description END AS Description, Size, CreateDate
					FROM Files
					WHERE IsLeaf = 1 AND Title LIKE '$title%' AND Description LIKE '$description%' and CreateBy='$username'
					ORDER BY CreateDate DESC";
			$table = mysql_GetArrayRows($sql);
			print(json_encode($table));
			break;
					
		case 'del':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$sql = "SELECT ID, ParentID, IsLeaf, Path FROM Files WHERE ID = '$id'";
			$rows = mysql_GetArrayRows($sql);
			
			if(isset($rows[0])) {
				$file_path = $rows[0]['Path'];
				$path = "$doc_root/file_storage/$file_path";
				if(unlink($path)) {
					mysql_Exec("DELETE FROM Files WHERE ID = '$id'");
					mysql_Exec("DELETE FROM FileGroups WHERE FileID = '$id'");
				}
			}
			break;
		
		case 'modifyName':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$text = mysql_real_escape_string($_POST['text'], $conn);
			$sql = "UPDATE Files SET Title = '$text' WHERE ID = '$id'";
			mysql_Exec($sql);
			break;

		case 'modifyDesc':
			$conn = mysql_GetConnection();
			$id = mysql_real_escape_string($_POST['id'], $conn);
			$text = mysql_real_escape_string($_POST['text'], $conn);
			$sql = "UPDATE Files SET Description = '$text' WHERE ID = '$id'";
			mysql_Exec($sql);
			break;
	}
?>