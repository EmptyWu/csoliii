<?php
	header("Content-Type: text/plain; charset=utf-8");
	require_once('../includes/mysql_tools.php');
	require_once('../includes/logger.php');
	require_once('../includes/ipod_algorithm.php');
	require_once('../includes/cfpropertylist-1.0.5/CFPropertyList.php');
	
	$id = $_GET['id'];
	if(!isset($id)) {
		echo '[]';
		return;
	}
	
	$sql = "SELECT CASE WHEN f.IsLeaf = 1 THEN MD5(CONCAT(fl.ParentID,fl.ID)) ELSE fl.ID END AS ID,
				fl.ParentID, f.IsLeaf, f.Title, f.Title AS `Name`, f.Description,
				f.CreateDate  AS `TimeStamp`, f.Size, f.MIME, f.Ext, 
				CASE WHEN f.Size < 1024 THEN CONCAT(f.Size,' B')
					WHEN f.Size < 1024 * 1024 THEN CONCAT(ROUND(f.Size / 1024, 2), ' KB')
					ELSE CONCAT(ROUND(f.Size / 1024 / 1024, 2), ' MB') END AS `AddOn`,
				CASE WHEN f.MIME IN ('audio/x-aiff','audio/mpeg','audio/mp3') THEN 'Music-File' 
					WHEN f.MIME IN ('video/mp4') THEN 'Movie-File' 
					WHEN f.MIME IN ('application/x-zip-compressed') THEN 'Html-File' 
					WHEN f.IsLeaf = 0 THEN 'Folder' ELSE 'Library-File' END AS `Icon`,
				CASE WHEN f.MIME IN ('audio/x-aiff','audio/mpeg','audio/mp3') THEN 'Music' 
					WHEN f.MIME IN ('video/mp4') THEN 'Movie' 
					WHEN f.MIME IN ('application/x-zip-compressed') THEN 'Html' 
					WHEN f.MIME IN ('application/zip') THEN 'Html'
					WHEN f.IsLeaf = 0 THEN 'Folder' ELSE 'File' END AS `Type`,
				fr.CreateDate, fr.BeginDate, fr.EndDate, fr.EndDate AS ExpireDate,
				DATEDIFF(fr.EndDate, CURDATE()) AS DaysLeft, f.Path
			FROM FileRelations fr
			INNER JOIN 
				(SELECT fg.FileID AS ID, f.ID AS ParentID
				FROM Files f
				INNER JOIN FileGroups fg
				ON f.ID = fg.GroupID
				UNION
				SELECT ID, '' AS ParentID
				FROM Files
				WHERE IsLeaf = 0
				ORDER BY ParentID, ID) /*FileList*/ fl
			ON fl.ParentID = fr.FileID OR fl.ID = fr.FileID
			INNER JOIN Files f
			ON f.ID = fl.ID
			INNER JOIN Students s
			ON s.ID = '$id' AND s.Removed = 0
			WHERE (fr.Type = 'All'
				OR (fr.Type = 'Group' AND fr.TypeID IN (SELECT GroupID FROM StudentGroups WHERE StudentID = '$id'))
				OR (fr.Type = 'Student' AND fr.TypeID = '$id')) AND DATEDIFF(fr.EndDate, CURDATE()) >= 0
				and fr.BeginDate <=now() and fr.EndDate >=now()
			Group BY fr.EndDate DESC, fr.FileID, fl.ID
			ORDER BY fr.EndDate DESC, f.CreateDate DESC, f.IsLeaf ASC";
	
	$table = mysql_GetIndexedArrayRows($sql);
	foreach($table as &$row) {
		if($row['IsLeaf'] == 1) {
			$host = $_SERVER['HTTP_HOST'];
			$path = $row["Path"];
			$row['Url'] = "http://$host/file_storage/$path";
			$row['DownloadCompleted'] = "http://$host/ipod/b.php?id=$id".urlencode("\t".$row['Url']);
		}
		unset($row["Path"]);
	}
	//echo count($table);
	print(json_encode(transform($table)));
	
	function transform($table) {
		$result = Array();
		foreach($table as $row) {
			if($row['ParentID'] == "" && $row['Type'] == 'Folder') {
				$row['Items'] = find_children($table, $row['ID']);
				unset($row['ParentID']);
				unset($row['IsLeaf']);
				unset($row['Size']);
				unset($row['MIME']);
				unset($row['Ext']);
				unset($row['CreateDate']);
				unset($row['ExpireDate']);
				unset($row['DaysLeft']);

				$result[] = $row;
			}
		}
		return $result;
	}
	function find_children($table, $id) {
		$result = Array();
		foreach($table as $row) {
			if($row['ParentID'] == $id) {
				if($row['Type'] == 'Folder') {
					$result[] = find_children($table, $id);
				}
				else {
					unset($row['ParentID']);
					unset($row['IsLeaf']);
					unset($row['Size']);
					unset($row['MIME']);
					unset($row['Ext']);
					unset($row['CreateDate']);
					unset($row['ExpireDate']);
					unset($row['DaysLeft']);

					$result[] = $row;
				}
			}
		}
		return $result;
	}
?>
