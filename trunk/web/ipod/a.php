<?php
	require_once('../includes/mysql_tools.php');
	require_once('../includes/logger.php');
	require_once('../includes/ipod_algorithm.php');
	require_once('../includes/cfpropertylist-1.0.5/CFPropertyList.php');
	
	$a = $_GET['a'];
	$b = $_GET['b'];
	$c = $_GET['c'];
	logWithPrefix('getlist', $a.'-'.$b.'-'.$c);
	if(preg_match('/[0-9a-f]{32}/', $b, $matches) != 1 || preg_match('/[0-9a-f]{32}/', $c, $matches) != 1) {
		die('error1');
	}
	
	$r = md5("The SID $a with $b Request!!");
	//echo 'c:'.$c.'<BR>';
	//echo 'r:'.$r;
	if($c != $r) {
		die('erroe2');
	}

	$sql = "SELECT CASE WHEN f.IsLeaf = 1 THEN MD5(CONCAT(fl.ParentID,fl.ID)) ELSE fl.ID END AS ID,
				fl.ParentID, f.IsLeaf, f.Title, f.Title AS `Name`, f.Description,
				f.CreateDate  AS `TimeStamp`, f.Size, f.MIME, f.Ext, 
				CASE WHEN f.Size < 1024 THEN CONCAT(f.Size,' B')
					WHEN f.Size < 1024 * 1024 THEN CONCAT(ROUND(f.Size / 1024, 2), ' KB')
					ELSE CONCAT(ROUND(f.Size / 1024 / 1024, 2), ' MB') END AS `AddOn`,
				CASE WHEN f.MIME IN ('audio/x-aiff','audio/mpeg','audio/mp3') THEN 'Music-File' 
					WHEN f.MIME IN ('video/mp4') THEN 'Movie-File' 
					WHEN f.IsLeaf = 0 THEN 'Folder' ELSE 'Library-File' END AS `Icon`,
				CASE WHEN f.MIME IN ('audio/x-aiff','audio/mpeg','audio/mp3') THEN 'Music' 
					WHEN f.MIME IN ('video/mp4') THEN 'Movie' 
					WHEN f.IsLeaf = 0 THEN 'Folder' ELSE 'File' END AS `FileType`,
				fr.CreateDate, fr.BeginDate, fr.EndDate, fr.EndDate AS ExpireDate,
				DATEDIFF(fr.EndDate, CURDATE()) AS DaysLeft, f.Path
			FROM FileRelations fr
			INNER JOIN 
				(SELECT fg.FileID AS ID, f.ID AS ParentID
				FROM Files f
				INNER JOIN FileGroups fg
				ON f.ID = fg.GroupID
				UNION
				SELECT ID, '' AS ParentID
				FROM Files
				WHERE IsLeaf = 0
				ORDER BY ParentID, ID) /*FileList*/ fl
			ON fl.ParentID = fr.FileID OR fl.ID = fr.FileID
			INNER JOIN Files f
			ON f.ID = fl.ID
			INNER JOIN Students s
			ON s.ID = '$a' AND s.Removed = 0
			WHERE (fr.Type = 'All'
				OR (fr.Type = 'Group' AND fr.TypeID IN (SELECT GroupID FROM StudentGroups WHERE StudentID = '$a'))
				OR (fr.Type = 'Student' AND fr.TypeID = '$a')) AND DATEDIFF(fr.EndDate, CURDATE()) >= 0
			Group BY fr.EndDate DESC, fr.FileID, fl.ID
			ORDER BY fr.EndDate DESC, f.CreateDate DESC, f.IsLeaf ASC";
	
	$table = mysql_GetIndexedArrayRows($sql);
	foreach($table as &$row) {
		if($row['IsLeaf'] == 1) {
			$host = $_SERVER['HTTP_HOST'];
			$path = $row["Path"];
			$row['url'] = "http://$host/file_storage/$path";
			$row['done'] = "http://$host/ipod/b.php?a=$a".urlencode("\t".$row['url']);
		}
		unset($row["Path"]);
	}
	$plist = new CFPropertyList();
	$td = new CFTypeDetector();  
	$guessedStructure = $td->toCFType($table);
	
	$guessedStructure->rewind();
	while($guessedStructure->valid()) {
		if($guessedStructure instanceOf CFArray || $guessedStructure instanceOf CFDictionary) {
			$item = $guessedStructure->current();
			$item->rewind();
			while($item->valid()) {
				$key = $item->key();
				switch($key) {
					case "Name":
					case "Description":
					case "TimeStamp":
					case "AddOn":
						$value = $item->current()->getValue();
						$item->add($key, new CFString($value));
						break;
				}
				$item->next();
			}
		}
		$guessedStructure->next();
	}
	
	$plist->add($guessedStructure);
	print($plist->toXML());
?>
