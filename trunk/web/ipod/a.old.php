<?php
	require_once('../includes/mysql_tools.php');
	require_once('../includes/logger.php');
	require_once('../includes/ipod_algorithm.php');
	require_once('../includes/cfpropertylist-1.0.5/CFPropertyList.php');
	
	$a = $_GET['a'];
	$b = $_GET['b'];
	$c = $_GET['c'];
	logWithPrefix('getlist', $a.'-'.$b.'-'.$c);
	if(preg_match('/[0-9a-f]{32}/', $b, $matches) != 1 || preg_match('/[0-9a-f]{32}/', $c, $matches) != 1) {
		die();
	}
	
	$r = md5("The SID $a with $b Request!!");
	if($c != $r) {
		die();
	}

	$sql = "SELECT * FROM 
			(
			SELECT f.ID, f.ParentID, f.IsLeaf, f.Title, f.Title AS `Name`, f.Description, 
						f.CreateDate  AS `TimeStamp`, f.Size, f.MIME, f.Ext, 
						CASE WHEN f.Size < 1024 THEN CONCAT(f.Size,' B')
							WHEN f.Size < 1024 * 1024 THEN CONCAT(ROUND(f.Size / 1024, 2), ' KB')
							ELSE CONCAT(ROUND(f.Size / 1024 / 1024, 2), ' MB') END AS `AddOn`,
						CASE WHEN f.MIME IN ('audio/x-aiff','audio/mpeg','audio/mp3') THEN 'Music-File' 
							WHEN f.MIME IN ('video/mp4') THEN 'Movie-File' 
							WHEN f.IsLeaf = 0 THEN 'Folder' ELSE 'Library-File' END AS `Icon`,
						CASE WHEN f.MIME IN ('audio/x-aiff','audio/mpeg','audio/mp3') THEN 'Music' 
							WHEN f.MIME IN ('video/mp4') THEN 'Movie' 
							WHEN f.IsLeaf = 0 THEN 'Folder' ELSE 'File' END AS `FileType`,
						fr.CreateDate, fr.BeginDate, fr.EndDate, fr.EndDate AS ExpireDate,
						DATEDIFF(fr.EndDate, CURDATE()) AS DaysLeft
			FROM FileRelations fr
			INNER JOIN Files f
			ON (fr.FileID = f.ID OR f.ParentID = fr.FileID) AND fr.Type = 0 AND DATEDIFF(fr.EndDate, CURDATE()) >= 0
			INNER JOIN Students s
			ON s.ID = '$a' AND s.Removed = 0
			UNION ALL
			SELECT f.ID, f.ParentID, f.IsLeaf, f.Title, f.Title AS `Name`, f.Description, 
						f.CreateDate  AS `TimeStamp`, f.Size, f.MIME, f.Ext, 
						CASE WHEN f.Size < 1024 THEN CONCAT(f.Size,' B')
							WHEN f.Size < 1024 * 1024 THEN CONCAT(ROUND(f.Size / 1024, 2), ' KB')
							ELSE CONCAT(ROUND(f.Size / 1024 / 1024, 2), ' MB') END AS `AddOn`,
						CASE WHEN f.MIME IN ('audio/x-aiff','audio/mpeg','audio/mp3') THEN 'Music-File' 
							WHEN f.MIME IN ('video/mp4') THEN 'Movie-File' 
							WHEN f.IsLeaf = 0 THEN 'Folder' ELSE 'Library-File' END AS `Icon`,
						CASE WHEN f.MIME IN ('audio/x-aiff','audio/mpeg','audio/mp3') THEN 'Music' 
							WHEN f.MIME IN ('video/mp4') THEN 'Movie' 
							WHEN f.IsLeaf = 0 THEN 'Folder' ELSE 'File' END AS `FileType`,
						fr.CreateDate, fr.BeginDate, fr.EndDate, fr.EndDate AS ExpireDate,
						DATEDIFF(fr.EndDate, CURDATE()) AS DaysLeft
			FROM FileRelations fr
			INNER JOIN Files f
			ON (fr.FileID = f.ID OR f.ParentID = fr.FileID) AND DATEDIFF(fr.EndDate, CURDATE()) >= 0
			INNER JOIN Groups g
			ON fr.Type = 1 AND fr.TypeID = g.ID
			INNER JOIN StudentGroups sg
			ON g.ID = sg.GroupID
			INNER JOIN Students s
			ON s.ID = sg.StudentID AND s.ID = '$a' AND s.Removed = 0
			) tbl
			GROUP BY ID
			ORDER BY CreateDate DESC, IsLeaf ASC";
	
	$table = mysql_GetIndexedArrayRows($sql);
	foreach($table as &$row) {
		if($row['IsLeaf'] == 1) {
			$host = $_SERVER['HTTP_HOST'];
			$path = $row['ParentID'].'/'.$row['ID'];
			$row['url'] = "http://$host/file_storage/$path";
			$row['done'] = "http://$host/ipod/b.php?a=$a".urlencode("\t".$row['url']);
		}
	}
	$plist = new CFPropertyList();
	$td = new CFTypeDetector();  
	$guessedStructure = $td->toCFType($table);
	$plist->add($guessedStructure);
	print($plist->toXML());
?>